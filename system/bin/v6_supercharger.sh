#!/system/bin/sh
#
# V6 SuperCharger, OOM Grouping & Priority Fixes created by zeppelinrox.
# SuperMinFree Calculator & MFK Calculator (for min_free_kbytes) created by zeppelinrox also :)
#
# See http://goo.gl/krtf9 - Linux Memory Consumption - Nice article!
# See http://goo.gl/hFdNO - Memory and SuperCharging Overview or... "Why `Free RAM` Is NOT Wasted RAM!"
# See http://goo.gl/4w0ba - MFK Calculator Info - explanation for vm.min_free_kbytes.
#
# For Debugging: Delete the # at the beginning of the next line and watch the output on the next run!
# set -x -o errexit
#
echo ""
echo "================================================"
echo " NOTE: BUSYBOX v1.16.2 OR HIGHER IS RECOMMENDED!"
echo "================================================"
sleep 2
busybox mount -o remount,rw / 2>/dev/null
busybox mount -o remount,rw rootfs 2>/dev/null
if [ ! -d "/sqlite_stmt_journals" ]; then
	mkdir /sqlite_stmt_journals
	madesqlitefolder=1
else madesqlitefolder=0
fi
if [ ! -d "/data/V6_SuperCharger" ]; then mkdir /data/V6_SuperCharger; fi
if [ ! -d "/sdcard/V6_SuperCharger" ]; then	mkdir /sdcard/V6_SuperCharger; fi
cat > /sdcard/!SuperCharger.html <<EOF
<br>
<br>
<i><u><b>-=The V6 SuperCharger=-</b></u> created by zeppelinrox.</i><br>
<br>
Hi! I hope that the V6 SuperCharger script is working well for you!<br>
<br>
Here is some <b><u>Background Info</u></b> in case you're curious...<br>
<br>
<a href="http://goo.gl/krtf9">Linux Memory Consumption</a> - Nice article!<br>
<a href="http://goo.gl/hFdNO">Memory and SuperCharging Overview</a> or... "Why \`Free RAM\` Is NOT Wasted RAM!"<br>
<a href="http://goo.gl/4w0ba">MFK Calculator Info</a> - explanation for <b>vm.min_free_kbytes</b><br>
<br>
Ok... now be sure to have <a href="http://market.android.com/details?id=com.jrummy.busybox.installer">BusyBox</a> installed or else the scripts won't work!<br>
Most custom roms should already have a version installed and that usually works OK.<br>
So if you need to, only install <b>BusyBox v1.16.2 or higher!</b><br>
Note that some versions above v1.18.2 and below v1.19.1 sometimes give errors so <u>PAY ATTENTION</u> to the script output!<br>
Versions above v1.19.0 should be fine though :-)<br>
<br>
A nice app for running the script is <a href="http://market.android.com/details?id=os.tools.scriptmanager">Script Manager</a><br>
It can even load scripts on boot - on ANY ROM!<br>
Plus, it even has WIDGETS!<br>
So you can actually put a V6 SuperCharger shortcut on your desktop, launch it, and have a quick peek at your current status!<br>
<br>
But first, you need to set up Script Manager properly!<br>
In the "Config" settings, enable "Browse as Root."<br>
Then browse to where you saved the V6 SuperCharger script, select it, and in the script's properties box, be sure to select "Run as Root" ie. SuperUser!<br>
<b>Do NOT run this file at boot!</b> (You don't want to run the install on every boot, do you?)<br>
Run the V6 SuperCharger script, touch the screen to access the soft keyboard, and enter your choice :)<br>
<br>
<b>Stock ROMs</b>: After running the script, have Script Manager load the newly created <b>/data/99SuperCharger.sh</b> on boot<br>
In the "Config" settings, be sure that "Browse as Root" is enabled.<br>
Press the menu key and then Browser. Navigate up to the root, then click on the "data" folder.<br>
Click on 99SuperCharger.sh and select "Script" from the "Open As" menu.<br>
In the properties dialogue box, check "Run as root" (SuperUser) and "Run at boot" and "Save".<br>
And that's it!<br>
Script Manager will load your most recent settings on boot!<br>
If you run the script later and with different settings, you don't have to reconfigure anything.<br>
Script Manager will just load the new /data/99SuperCharger.sh on boot automagically :)<br>
<br>
<b>Custom ROMs</b>: If you have a custom rom that loads /system/etc/init.d boot scripts,<br>
You DON'T need to use Script Manager to load a boot script. It will all be automatic!<br>
Also, if you can run boot scripts from the /system/etc/init.d folder, there are other options.<br>
For example you can use an app like Terminal Emulator to run the script.<br>
If your ROM has the option, <b>DISABLE "Lock Home In Memory.</b> This takes effect immediately.<br>
Alternately, <u>if you need to free up extra ram</u>, you can use "Lock Home in Memory" as a "Saftey Lock".<br>
ie. Use it to toggle your launcher from "Bulletproof" (0) or Hard To Kill (1) to "Weak" (2) in the event that you want to make the launcher an easy kill and free up extra RAM ;)<br>
<br>
<b>If Settings Don't Stick:</b> If you have Auto Memory Manager, DISABLE SuperUser permissions and if you have AutoKiller Memory Optimizer, DISABLE the apply settings at boot option!<br>
Also, if you have a <b>Custom ROM</b>, there might be something in the init.d folder that interferes with priorities and minfrees.<br>
If you can't find the problem, a quick fix is to have Script Manager run <b>/system/etc/init.d/*99SuperCharger</b> "at boot" and "as root."<br>
<br>
Another option is to make a Script Manager widget for <b>/system/etc/init.d/*99SuperCharger</b> or <b>/data/99SuperCharger.sh</b> on your homescreen and simply launch it after each reboot.<br>
<br>
For those with a <b>Milestone</b>, I made a version for <b>Androidiani Open Recovery</b> too :D<br>
Just extract the zip to the root of the sdcard (it contains the directory structure), load AOR, and there will be a SuperCharger Menu on the main screen! <br>
<br>
For more SuperCharging help and info,<br>
See the <a href="http://goo.gl/qM6yR">-=V6 SuperCharger Thread=-</a><br>
Feedback is Welcome!<br>
<br>
-=zeppelinrox=- @ <a href="http://goo.gl/qM6yR">XDA</a> & <a href="http://www.droidforums.net/forum/droid-hacks/148268-script-v6-supercharger-htk-bulletproof-launchers-fix-memory-all-androids.html">Droid</a> Forums<br>
<br>
EOF
if [ "$madesqlitefolder" -eq 1 ]; then rm -r /sqlite_stmt_journals; fi
busybox mount -o remount,ro / 2>/dev/null
busybox mount -o remount,ro rootfs 2>/dev/null
echo "        Test Driving Your Android Device..."
echo "================================================"
echo ""
sleep 1
if [ ! -f "/system/xbin/busybox" ] && [ ! -f "/xbin/busybox" ] && [ ! -f "/system/bin/busybox" ] && [ ! -f "/bin/busybox" ] && [ ! -f "/system/sbin/busybox" ] && [ ! -f "/sbin/busybox" ]; then
	echo " BusyBox NOT FOUND..."
	echo ""
	sleep 4
	echo "                           ...No Supe for you!!"
	echo ""
	sleep 4
	echo " If you continue, problems can occur..."
	echo ""
	sleep 4
	echo "                         ...and even bootloops!"
	echo ""
	sleep 4
	echo "  ...Please install BusyBox and try again..."
	echo ""
	sleep 4
	echo " Loading Market..."
	echo ""
	sleep 4
	su -c "LD_LIBRARY_PATH=/vendor/lib:/system/lib am start http://market.android.com/details?id=com.jrummy.busybox.installer"
	echo ""
	echo " I hope that helped! :)"
	echo ""
	sleep 4
	echo " Continue anyway...?"
	echo ""
	sleep 2
	echo -n " Enter Y for Yes, any key for No: "
	read bbnotfound
	echo ""
	case $bbnotfound in
	  y|Y)echo " uh... right... okay..."
		  echo ""
		  sleep 2;;
		*)echo " Buh Bye..."
		  exit 69;;
	esac
else
	test1=`busybox | head -n 1 | awk '{print $1}'`
	test2=`busybox | head -n 1 | awk '{print $2}'`
	if [ "$test2" ]; then
		echo -n " BusyBox "
		echo "$test2 Found!"
	else echo " ERROR! Can't determine BusyBox version!"
	fi
	echo ""
	sleep 1
	if [ "$test1" = "BusyBox" ]; then
		echo "   AWKing Awesome... AWK test passed!"
		echo ""
		sleep 1
		test3=`pgrep ini`
		if [ "$test3" ]; then echo "          Groovy... pgrep test passed too!"
		else
			echo " BusyBox has holes in it..."
			echo ""
			sleep 4
			echo " ...BulletProof Apps ain't gonna work..."
			echo ""
			sleep 4
			echo "                          ...pgrep test FAILED!"
		fi
	else
		echo " There was an AWK error..."
		echo ""
		sleep 4
		echo "                              ...what the AWK!?"
		echo ""
		sleep 4
		echo " If you continue, problems can occur..."
		echo ""
		sleep 4
		echo "                         ...and even bootloops!"
		echo ""
		sleep 4
		echo " ...Please reinstall BusyBox and try again..."
		echo ""
		sleep 4
		echo " Loading Market..."
		echo ""
		sleep 4
		su -c "LD_LIBRARY_PATH=/vendor/lib:/system/lib am start http://market.android.com/details?id=com.jrummy.busybox.installer"
		echo ""
		echo " I hope that helped! :)"
		echo ""
		sleep 4
		echo " Continue anyway...?"
		echo ""
		sleep 2
		echo -n " Enter Y for Yes, any key for No: "
		read awkerror
		echo ""
		case $awkerror in
		  y|Y)echo " uh... right... okay...";;
		  *)echo " Buh Bye..."
			exit 69;;
		esac
	fi
	echo ""
	sleep 1
fi
if [ "`id | grep =0`" ]; then echo "        Nice! You're Running as Root/SuperUser!"
else
	echo " You are NOT running this script as root..."
	echo ""
	sleep 4
	echo "                      ...No SuperUser for you!!"
	echo ""
	sleep 4
	echo "     ...Please Run as Root and try again..."
	echo ""
	sleep 4
	echo " Loading Help File..."
	echo ""
	sleep 4
	su -c "LD_LIBRARY_PATH=/vendor/lib:/system/lib am start -a android.intent.action.VIEW -n com.android.browser/.BrowserActivity -d file:///sdcard/!SuperCharger.html"
	echo ""
	echo " I hope that helped! :)"
	echo ""
	sleep 4
	exit 69
fi
echo ""
sleep 1
line=================================================
echo $line
echo "              Test Drive Complete!"
echo $line
echo ""
sleep 2
speed=1
sleep="sleep $speed"
smrun=`pgrep scriptmanager`
bbversion=`busybox | head -n 1 | awk '{print $1,$2}'`
ram=$((`free | awk '{ print $2 }' | sed -n 2p`/1024))
opt=0;ran=0;gb=0;ics=0;memory=c;oomstick=0;ram1=5144;lupdate=0
initrcpath1="/init.rc"
initrcpath="/data$initrcpath1"
initrcbackup=$initrcpath.unsuper
allrcpaths=`grep import $initrcpath1 | sed 's/.*import*.//'`
busybox mount -o remount,rw / 2>/dev/null
busybox mount -o remount,rw rootfs 2>/dev/null
busybox mount -o remount,rw /system 2>/dev/null
busybox mount -o remount,rw `busybox mount | grep system | awk '{print $1,$3}' | sed -n 1p` 2>/dev/null
if [ "$allrcpaths" ] && [ ! -f "$allrcpaths" ]; then touch $allrcpaths; chown 0.0 $allrcpaths; chmod 644 $allrcpaths; fi
for rc in `busybox find /system -iname "*.rc"`; do
	if [ ! "`echo $rc | grep goldfish`" ] && [ ! "`echo $rc | grep ueventd`" ] && [ ! "`echo $allrcpaths | grep $rc`" ] ; then
		chown 0.0 $rc; chmod 644 $rc
		allrcpaths="$allrcpaths $rc"
		if [ "`grep -ls "on boot" $rc`" ]; then rcpaths="$rcpaths $rc"; fi
	fi
done
busybox mount -o remount,ro / 2>/dev/null
busybox mount -o remount,ro rootfs 2>/dev/null
busybox mount -o remount,ro /system 2>/dev/null
busybox mount -o remount,ro `busybox mount | grep system | awk '{print $1,$3}' | sed -n 1p` 2>/dev/null
LIMIT1=0;LIMIT2=3;LIMIT3=6;LIMIT4=10;LIMIT5=12;LIMIT6=15
newscadj="$LIMIT1,$LIMIT2,$LIMIT3,$LIMIT4,$LIMIT5,$LIMIT6";oldscadj1="0,3,5,7,14,15";oldscadj2="0,2,4,7,14,15";scamadj1="0,2,5,7,14,15";scamadj2="0,1,2,4,6,15"
if [ -f "/data/V6_SuperCharger/SuperChargerMinfree" ]; then
	cp -fr /data/V6_SuperCharger/SuperChargerMinfree /data/V6_SuperCharger/SuperChargerMinfreeOld
	scminfreeold=`cat /data/V6_SuperCharger/SuperChargerMinfreeOld`
fi
if [ -f "/data/V6_SuperCharger/SuperChargerOptions" ]; then animation=`awk -F , '{print $3}' /data/V6_SuperCharger/SuperChargerOptions`; fi
if [ ! "$animation" ] || [ "$animation" -eq 1 ]; then
	clear;$sleep;echo $line;echo "";echo "";echo "";echo "";echo "";echo ""; echo "";echo "";echo "";echo "";echo "";$sleep
	clear;echo "";echo $line;echo "";echo "";echo "";echo "";echo "";echo ""; echo "";echo "";echo "";echo "";$sleep
	clear;echo "";echo $line;echo $line;echo "";echo "";echo "";echo "";echo ""; echo "";echo "";echo "";echo "";$sleep
	clear;echo "";echo $line;echo "";echo $line;echo "";echo "";echo "";echo ""; echo "";echo "";echo "";echo "";$sleep
	clear;echo "";echo $line;echo "    |/   #####";echo "";echo $line;echo "";echo "";echo ""; echo "";echo "";echo "";echo "";$sleep
	clear;echo "";echo $line;echo "    | /  #    #";echo "    |/   #####";echo "";echo $line;echo "";echo "";echo "";echo "";echo "";echo "";$sleep
	clear;echo "";echo $line;echo "    ||//  #    #";echo "    | /  #    #";echo "    |/   #####";echo "";echo $line;echo "";echo "";echo "";echo "";echo "";$sleep
	clear;echo "";echo $line;echo "    || //  #####";echo "    ||//  #    #";echo "    | /  #    #";echo "    |/   #####";echo "";echo $line;echo "";echo "";echo "";echo "";$sleep
	clear;echo "";echo $line;echo "    ||  //  #";echo "    || //  #####";echo "    ||//  #    #";echo "    | /  #    #";echo "    |/   #####";echo "";echo $line;echo "";echo "";echo "";$sleep
	clear;echo "";echo $line;echo "    ||   //  #    #" ;echo "    ||  //  #";echo "    || //  #####";echo "    ||//  #    #";echo "    | /  #    #";echo "    |/   #####";echo "";echo $line;echo "";echo "";$sleep
	clear;echo "";echo $line;echo "    ||    //  #####";echo "    ||   //  #    #";echo "    ||  //  #";echo "    || //  #####";echo "    ||//  #    #";echo "    | /  #    #";echo "    |/   #####";echo "";echo $line;echo "";$sleep
	clear;echo "";echo $line;echo "";echo "    ||    //  #####";echo "    ||   //  #    #";echo "    ||  //  #";echo "    || //  #####";echo "    ||//  #    #";echo "    | /  #    #";echo "    |/   #####";echo "";echo $line;sleep 2
	clear;echo "";echo $line;echo "";echo "    ||    //  #####";echo "    ||   //  #    #";echo "    ||  //  #           -=SUPERCHARGER=-";echo "    || //  #####";echo "    ||//  #    #";echo "    | /  #    #";echo "    |/   #####";echo "";echo $line;sleep 2
	clear;echo " zoom...";echo $line;echo "";echo "    ||    //  #####";echo "    ||   //  #    #";echo "    ||  //  #           -=SUPERCHARGER=-";echo "    || //  #####";echo "    ||//  #    #";echo "    | /  #    #  Presented";echo "    |/   #####";echo "";echo $line;sleep 2
	clear;echo " zoOM... zoOM...";echo $line;echo "";echo "    ||    //  #####";echo "    ||   //  #    #";echo "    ||  //  #           -=SUPERCHARGER=-";echo "    || //  #####";echo "    ||//  #    #";echo "    | /  #    #  Presented by:";echo "    |/   #####";echo "";echo $line;sleep 2
	clear;echo " zOOM... zOOM...";echo $line;echo "";echo "    ||    //  #####";echo "    ||   //  #    #";echo "    ||  //  #           -=SUPERCHARGER=-";echo "    || //  #####";echo "    ||//  #    #";echo "    | /  #    #  Presented by:";echo "    |/   #####                 -=zeppelinrox=-";echo "";echo $line;sleep 2
else
	clear
	echo ""
	echo $line
fi
echo " NOTE: BUSYBOX v1.16.2 OR HIGHER IS RECOMMENDED!"
echo $line
echo ""
$sleep
if [ -f "/data/V6_SuperCharger/SuperChargerOptions" ]; then
	speed=`awk -F , '{print $1}' /data/V6_SuperCharger/SuperChargerOptions`
	sleep="sleep $speed"
	buildprop=`awk -F , '{print $2}' /data/V6_SuperCharger/SuperChargerOptions`
	initrc=`awk -F , '{print $4}' /data/V6_SuperCharger/SuperChargerOptions`
else firstgear=yes; sleep="sleep 2"
fi
if [ "$smrun" ]; then echo "   Touch the screen to bring up soft keyboard"
else echo " Try Script Manager... it's easier!"
fi
echo ""
echo $line
echo ""
$sleep
echo " Additional BusyBox Info:"
echo ""
$sleep
echo " `busybox | head -n 1 | awk '{print $1,$2,$3}'`"
echo " `busybox | head -n 1 | awk '{print $4,$5,$6,$7,$8,$9,$10}'`"
echo ""
$sleep
echo "        BE SURE IT'S A COMPLETE VERSION!"
echo ""
$sleep
echo " You are currently using $bbversion..."
echo ""
$sleep
if [ "$bbversion" \> "BusyBox v1.16.1" ] && [ "$bbversion" \< "BusyBox v1.18.3" ] || [ "$bbversion" \> "BusyBox v1.18.9" ]; then echo "           ...which is fine - if it's COMPLETE!"
else
	echo $line
	echo -n " WARNING!  Your BusyBox is greater than "
	if [ "$bbversion" \> "BusyBox v1.18.2" ] && [ "$bbversion" \< "BusyBox v1.19.0" ]; then
		echo "v1.18.2"
		echo ""
		sleep 2
		echo "                      ...and less that v1.19.0!"
		echo $line
		echo ""
		sleep 2
		echo " These versions can cause problems..."
	else
		echo "v1.16.2"
		echo $line
		echo ""
		sleep 2
		echo " Some commands may not even work..."
	fi
	echo ""
	sleep 2
	echo " ..and you may have issues such as bootloops..."
	echo ""
	sleep 2
	echo "                  ...proceed at your own risk!!"
	echo ""
	sleep 2
	echo " Oh yeah... Loading Market..."
	echo ""
	sleep 2
	su -c "LD_LIBRARY_PATH=/vendor/lib:/system/lib am start http://market.android.com/details?id=com.jrummy.busybox.installer"
fi
echo ""
$sleep
homeadj=`getprop ro.HOME_APP_ADJ`;FA=`getprop ro.FOREGROUND_APP_ADJ`;PA=`getprop ro.PERCEPTIBLE_APP_ADJ`;VA=`getprop ro.VISIBLE_APP_ADJ`
if [ ! "$FA" ] || [ "`grep build.version.release /system/build.prop | sed 's/.*=//'`" \> 3.69 ]; then ics=1;FA=0;PA=2
elif [ "$PA" ]; then gb=1
fi
if [ "$homeadj" -lt "$VA" ] && [ "$homeadj" -eq "$(($FA+1))" ] && [ "$ics" -eq 0 ]; then
	for l in `ls /proc`; do
		if [ "$l" -ne 0 ] 2>/dev/null && [ -f "/proc/$l/oom_adj" ] && [ "`cat /proc/$l/cmdline`" ] && [ "`cat /proc/$l/oom_adj`" -eq "$homeadj" ]; then
			HL=`cat /proc/$l/oom_adj`
			lname=`cat /proc/$l/cmdline | sed 's/.*\///'`
			status="diehard"; break
		fi
	done
fi
if [ ! "$HL" ]; then
	for l in `echo $(grep set.*/*.Launcher /d*/system/packages.xml | sed 's/\/*.L.*//' | sed 's/.*="//')`; do
		if [ -f "/proc/$(pgrep $l)/oom_adj" ]; then
			if [ ! "$HL" ] || [ "$HL" -gt "`cat /proc/$(pgrep $l)/oom_adj`" ]; then
				HL=`cat /proc/$(pgrep $l)/oom_adj`
				lfound=$l
				lname=$l
			fi
		fi
	done
	for l in `pgrep -l htc.launcher` `pgrep -l sonyericsson.home` `pgrep -l lghome` `pgrep -l adwfreak` `pgrep -l zeam` `pgrep -l trebuchet` `pgrep -l home` `pgrep -l twlauncher` `pgrep -l tw3` `pgrep -l tw4` `pgrep -l teslacoilsw.launcher` `pgrep -l $lfound` `pgrep -l android.launcher` `pgrep -l acore`; do
		if [ -f "/proc/$l/oom_adj" ]; then
			if [ "`cat /proc/$l/oom_adj`" -eq "$homeadj" ] || [ "`cat /proc/$l/oom_adj`" -eq "$VA" ] && [ "$ics" -eq 0 ] || [ "$ics" -eq 1 ]; then
				if [ ! "$HL" ] || [ "$HL" -gt "`cat /proc/$l/oom_adj`" ]; then
					HL=`cat /proc/$l/oom_adj`
					lupdate=1
				fi
			fi
		elif [ "$l" \> "a" ] && [ "$lupdate" -eq 1 ]; then
			lname=$l
			lupdate=0
		fi
	done 2>/dev/null
fi
if [ "$ics" -eq 1 ] && [ "$HL" -lt 3 ]; then icssced=hellzyeah; VA=3
elif [ "$ics" -eq 1 ]; then HL=6; VA=1
fi
echo $line
if [ -f "/sdcard/V6_SuperCharger/SuperChargerAdj" ] && [ ! "`grep "SuperCharger" /system/build.prop`" ]; then
	resuperable=indeed; newsupercharger=woohoo
	echo ""
	echo " Oh wait... Did you flash a new ROM or kernel?"
	echo ""
	$sleep
	echo " I Will Automagically..."
	echo ""
	$sleep
	echo $line
	echo "           Run The Re-SuperCharger!!"
	echo $line
	echo ""
	$sleep
	echo " This lets you restore settings and scripts..."
	echo ""
	$sleep
	echo "  ...without overwriting your new system files!"
	echo ""
	$sleep
	echo " You may need to install separately..."
	echo ""
	$sleep
	echo " ...a Launcher, Nitro Lag Nullifier & 3G Turbo."
elif [ "$firstgear" ]; then
	echo "                  Hey Rookie!!"
	echo $line
	echo ""
	$sleep
	echo " First Time SuperChargers..."
	echo ""
	$sleep
	echo "         ...will make a Pit Stop in..."
	echo ""
	$sleep
	echo "                             ...Driver Options!"
else
	echo "   Prior scrolling speed of $speed had been set..."
	echo $line
	$sleep
	if [ "$ics" -eq 0 ]; then
		if [ "$buildprop" -eq 0 ]; then	echo "   LOCAL.PROP to be used for SuperCharging!"
		else echo "   BUILD.PROP to be used for SuperCharging!"
		fi
		echo $line
		$sleep
	fi
	if [ "$initrc" -eq 1 ]; then
		echo "   System Integration of $initrcpath1 is ON..."
		echo $line
		echo ""
		$sleep
		echo "   Go to Driver Options and DISABLE..."
		echo ""
		$sleep
		echo "         ...if you have root/permission issues!"
		echo ""
		echo $line
		$sleep
	elif [ -d "/system/etc/init.d" ]; then
		echo "   System Integration of $initrcpath1 is OFF..."
		echo $line
		echo ""
		$sleep
		echo "   Go to Driver Options and ENABLE..."
		echo ""
		$sleep
		echo "             ...for greater System Integration!"
		echo ""
		echo $line
		$sleep
	fi
	echo " Note: You can bake $initrcpath into your ROM!"
	echo $line
	echo ""
	$sleep
	echo " All settings can be changed in Driver Options!"
fi 2>/dev/null
echo ""
echo $line
echo ""
$sleep
if [ "$HL" ]; then
	echo " $lname is the home launcher!"
	echo ""
	$sleep
	echo -n " But verify groupings with the bOOM Stick"
	if [ "$lname" = "android.process.acore" ] && [ ! "$status" ]; then
		echo "..."
		echo ""
		$sleep
		echo "              ...since I'm not 100% sure... lol"
	else echo "!"
	fi
	echo ""
	echo $line
	echo ""
	$sleep
fi
if [ "$lname" = "android.process.acore" ] && [ ! "$status" ] && [ "$ics" -eq 0 ] || [ ! "$HL" ] && [ "$ics" -eq 0 ]; then
	echo " Is Home is Locked in Memory?"
	echo ""
	$sleep
	echo " If it is, Enter Y for Yes, any key for No..."
	echo ""
	$sleep
	echo -n " Just say \"No\" if you don't know: "
	read homelocked
	echo ""
	case $homelocked in
	  y|Y)HL=$VA
		  echo " WHAT? You need to disable that feature!";;
		*)HL=$homeadj
		  echo " Good Stuff!";;
	esac
	echo ""
	echo $line
	echo ""
	$sleep
fi
chmod 777 /sys/module/lowmemorykiller/parameters/minfree
chmod 777 /sys/module/lowmemorykiller/parameters/adj
echo -n " Press the Enter Key... and Come Get Some!!"
read getsome
while :; do
 MB0=4;MB1=0;MB2=0;MB3=0;MB4=0;MB5=0;MB6=0
 SL0=0;SL1=0;SL2=0;SL3=0;SL4=0;SL5=0;SL6=0
 scpercent=0;newlauncher=0;showbuildpropopt=0;break=0;restore=0;quickcharge=0;calculatorcharge=0;revert=0;ReSuperCharge=0;UnSuperCharged=0;UnSuperChargerError=0;SuperChargerScriptManagerHelp=0;SuperChargerHelp=0;vroomverifier=;low=w;ldone=
 currentminfree=`cat /sys/module/lowmemorykiller/parameters/minfree`
 currentadj=`cat /sys/module/lowmemorykiller/parameters/adj`
 ram2=`$low$memory "$0" | awk '{print $1}'`
 if [ -f "/data/V6_SuperCharger/SuperChargerAdj" ]; then scadj=`cat /data/V6_SuperCharger/SuperChargerAdj`; fi
 if [ -f "/data/V6_SuperCharger/BulletProof_Apps_HitList" ]; then bpapplist=`cat /data/V6_SuperCharger/BulletProof_Apps_HitList`; fi
 if [ -f "/data/V6_SuperCharger/SuperChargerMinfree" ]; then scminfree=`cat /data/V6_SuperCharger/SuperChargerMinfree`
 else scminfree=
 fi
 if [ -f "/data/V6_SuperCharger/SuperChargerCustomMinfree" ]; then sccminfree=`cat /data/V6_SuperCharger/SuperChargerCustomMinfree`
 else sccminfree=
 fi
 if [ ! -f "/data/V6_SuperCharger/SuperChargerMinfreeOld" ]; then scminfreeold=; fi
 for rc in $allrcpaths; do
	if [ -f "$rc.unsuper" ] ; then allrcbackups="$allrcbackups $rc.unsuper"; fi
	if [ "`grep "super_service" $rc`" ]; then scservice="scsinstalled"; else scservice=; fi
	if [ "`grep "bullet_service" $rc`" ]; then bpservice="bpsinstalled"; else bpservice=; fi
 done
 if [ "$opt" -ne 69 ]; then echo ""; else $sleep; fi
 echo $line
 echo " For Help & Info, see /sdcard/!SuperCharger.html"
 echo $line
 $sleep
 busybox echo " \\\\\\\\ T H E  V 6   S U P E R C H A R G E R ////"
 echo "  ============================================"
 busybox echo "        \\\\\\\\    Driver's Console    ////"
 echo "         =============================="
 echo ""
 echo "1. SuperCharger & Launcher Status{Update9 RC6.9}"
 echo "                                               }"
 echo "==================== 256 HP ===================="
 echo "2. UnLedded  (Multitasking){8,12,22,24,26,28 MB}"
 echo "3. Ledded        (Balanced){8,12,26,28,30,32 MB}"
 echo "4. Super UL    (Aggressive){8,12,28,30,35,50 MB}"
 echo "                                               }"
 echo "==================== 512 HP ===================="
 echo "5. UnLedded (Multitasking){8,14,40,50,60, 75 MB}"
 echo "6. Ledded       (Balanced){8,14,55,70,85,100 MB}"
 echo "7. Super UL   (Aggressive){8,14,75,90,95,125 MB}"
 echo "                                               }"
 echo "=================== 768+ HP ===================="
 echo "8. Super 768HP(Aggressive){8,16,150,165,180,200}"
 echo "9. Super 1000HP(Agressive){8,16,200,220,240,275}"
 echo "                                               }"
 echo $line
 echo -n "10. Quick V6 Cust-OOMizer "
 if [ "$sccminfree" ]; then echo $sccminfree | awk -F , '{print "{"$1/256","$2/256","$3/256","$4/256","$5/256","$6/256"}"}'
 else echo "  {Create Or Restore!}"
 fi
 echo $line
 echo "11. OOM Grouping Fixes + Hard To Kill Launcher }"
 echo "12. OOM Grouping Fixes + Die-Hard Launcher     }"
 echo "13. OOM Grouping Fixes + BulletProof Launcher  }"
 echo $line
 echo "14. UnKernelizer         {UnDo Kernel/VM Tweaks}"
 echo "15. UnSuperCharger      {Revert Memory Settings}"
 echo $line
 echo "16. The bOOM STICK        {Verify OOM Groupings}"
 echo "17. BulletProof Apps               {Hit or Miss}"
 echo "18. Engine Flush        {ReCoupe RAM & Kill Lag}"
 echo "19. Detailing         {Vacuum & Reindex SQL DBs}"
 echo "20. Nitro Lag Nullifier           {Experimental}"
 echo $line
 echo "21. System Installer            {Terminal Usage}"
 echo "22. Re-SuperCharger        {Restore V6 Settings}"
 echo "23. PowerShifting         {Switch Presets FAST!}"
 echo $line
 echo "24. Owner's Guide      {Open !SuperCharger.html}"
 echo "25. Help Centre             {Open XDA SC Thread}"
 echo "26. Driver Options                    {Settings}"
 echo "27. SuperCharge You                    {Really!}"
 echo "28. ReStart Your Engine       {Reboot Instantly}"
 echo "29. SuperClean & ReStart  {Wipe Dalvik & Reboot}"
 echo "30. Eject                                 {Exit}"
 echo $line
 if [ "$ran" -eq 0 ] && [ ! "$resuperable" ]; then
	echo "     So is it working? READ MESSAGES BELOW ;]"
	echo $line
	echo "     The Next 4 Sections Are Worth 25% Each!!"
	echo $line
 fi
 echo ""
 echo -n "   Launcher is"
 if [ "$ics" -eq 1 ] && [ ! "$icssced" ]; then
	echo " on ICeS .... so.... weak.... :("
	echo "   (Assumes that stock ICS ADJs are in effect!)"; status=4
 elif [ "$HL" -gt "$VA" ]; then echo ".... so.... weak.... :("; status=4
 elif [ "$HL" -eq "$VA" ]; then echo " Locked In Memory ie. Very Weak!"; status=3
 else
	if [ "$HL" -eq "$FA" ]; then echo -n " BULLETPROOF!"; status=0
	elif [ "$HL" -eq "$(($FA+1))" ]; then echo -n " DIE-HARD!"; status=1
	else echo -n " HARD TO KILL!"; status=2
	fi
	echo " ie. SUPERCHARGED!"
	oomstick=1
	scpercent=$(($scpercent+25))
 fi
 echo ""
 echo $line
 sleep 2
 echo -n "   SuperCharger ADJ Entries "
 if [ "$icssced" ]; then echo "NOT Needed..."; scpercent=$(($scpercent+25))
 elif [ "$ics" -eq 1 ]; then echo "NOT Possible..."
	echo $line
	echo "   Protect the Launcher via \"BulletProof Apps\"!"
 elif [ "`grep "V6 SuperCharger" /system/build.prop`" ]; then echo "Found in build.prop!"; scpercent=$(($scpercent+25))
 elif [ -f "/data/local.prop" ] && [ "`grep "V6 SuperCharger" /data/local.prop`" ]; then echo "Found in local.prop!"; scpercent=$(($scpercent+25))
 else echo "NOT Found!"
 fi
 echo $line
 if [ "$currentadj" = "$newscadj" ] || [ "$currentadj" = "$oldscadj1" ] || [ "$currentadj" = "$oldscadj2" ] || [ "$currentadj" = "$scamadj1" ]; then
	scpercent=$(($scpercent+25))
	if [ "$currentadj" != "$newscadj" ]; then
		echo ""
		echo "   NON-SuperCharger OOM Grouping Is In Effect!"
		echo ""
		if [ "$currentadj" = "$scamadj1" ]; then
			echo "   ...and the Launcher Is NOT \"Non-Killable\"!"
			echo "   If you thought it was, you were mislead..."
			echo ""
			echo "   Apps simply got moved up a slot..."
			echo "     ...so the minfrees become less aggressive!"
			echo ""
			echo $line
			echo "  So there's NOTHING \"Non-Killable\" About It!"
		fi
		echo $line
	elif [ ! "$scadj" ] && [ "$ran" -eq 0 ]; then echo "   OOM Grouping Fixes ARE In Effect!"; echo $line
	fi
 elif [ "$currentadj" = "$scamadj2" ]; then
	echo "   Found \"Stockish\" OOM Grouping..."
	echo "             ...which basically does nothing :P"
	echo $line
 fi
 if [ "$ran" -eq 1 ]; then scpercent=$(($scpercent+25))
 else
	if [ "$scadj" ] && [ "$status" -eq 3 ]; then
		echo ""
		echo "   Home Launcher is Locked in Memory!"
		echo "   You need to DISABLE this shi... feature..."
		echo "          ...In both ROM and Launcher settings!"
	elif [ "$ics" -eq 1 ] && [ ! "$icssced" ]; then
		echo ""
		echo "   SuperCharged Launcher is not in effect..."
		echo "   ICS WON'T load values from a prop file :("
		echo "   But the other fixes STILL work!"
		echo "   But now you can do it via SERVICES.JAR..."
		echo "           ...See the XDA Thread for more info!"
	elif [ "$scadj" ] && [ "$oomstick" -eq 0 ] && [ "$buildprop" -eq 0 ] && [ "$ics" -eq 0 ]; then
		echo ""
		echo "   SuperCharged Launcher Is NOT In Effect!"
		echo "   You may need to use the build.prop option..."
		echo "   Select 2 - 12 from the menu NOW to access it!"
		showbuildpropopt=1
	fi
	if [ "$scadj" ] && [ "$currentadj" != "$scadj" ]; then
		echo ""
		echo "   OOM Grouping Fixes ARE NOT In Effect!"
		echo "   That means the boot script did NOT run..."
		echo "   Select Help File! from the menu for Repairs!"
	elif [ "$scadj" ] && [ "$currentadj" = "$scadj" ]; then
		echo ""
		echo "   OOM Grouping Fixes ARE In Effect!"
		echo "   That means the boot script ran!"
	else newsupercharger=woohoo
	fi
	if [ "$ics" -eq 1 ] && [ ! "$icssced" ] || [ "$scadj" ]; then echo ""; echo $line; fi
	if [ "$scminfree" ]; then echo ""; fi
	if [ "$scminfree" ] && [ "$currentminfree" != "$scminfree" ]; then
		echo "   Current Values DON'T MATCH Prior SuperCharge!"
		if [ -f "/data/Ran_SuperCharger" ]; then echo "   Check /data/Ran_SuperCharger for boot info."
		else echo "   The boot script *99SuperCharger did NOT run!"
		fi
		echo "   Select Help File! from the menu for Repairs!"
	elif [ "$scminfree" ] && [ "$currentminfree" = "$scminfree" ]; then
		echo "   Current Values MATCH Prior SuperCharge!"
		echo "   That means that it's working! ;)"
		scpercent=$(($scpercent+25))
	fi
 fi
 if [ ! "$scminfree" ]; then
	echo "   SuperCharger Minfrees NOT FOUND! Run 2 - 10!"
	echo $line
 else echo ""; echo $line
 fi
 echo "   Current Minfrees = $((`echo $currentminfree | awk -F , '{print $1}'`/256)),$((`echo $currentminfree | awk -F , '{print $2}'`/256)),$((`echo $currentminfree | awk -F , '{print $3}'`/256)),$((`echo $currentminfree | awk -F , '{print $4}'`/256)),$((`echo $currentminfree | awk -F , '{print $5}'`/256)),$((`echo $currentminfree | awk -F , '{print $6}'`/256)) MB"
 if [ "$scminfreeold" ]; then echo $scminfreeold | awk -F , '{print "  Prior V6 Minfrees = "$1/256","$2/256","$3/256","$4/256","$5/256","$6/256" MB"}'; fi
 echo $line
 echo ""
 echo "   You have $ram MB of RAM on your device..."
 if [ "$ram" -lt 256 ]; then echo -n "         ...256 HP"
	calculatedmb="8, 12, $(($ram*11/100)), $(($ram*12/100)), $(($ram*13/100)), $(($ram*14/100))"
 elif [ "$ram" -lt 512 ]; then echo -n "         ...512 HP"
	calculatedmb="8, 12, $(($ram*11/100)), $(($ram*13/100)), $(($ram*15/100)), $(($ram*17/100))"
 elif [ "$ram" -lt 768 ]; then echo -n "         ...768 HP"
	calculatedmb="8, 14, $(($ram*15/100)), $(($ram*17/100)), $(($ram*19/100)), $(($ram*21/100))"
 else echo -n "         ...1000 HP"
	calculatedmb="8, 16, $(($ram*20/100)), $(($ram*22/100)), $(($ram*24/100)), $(($ram*26/100))"
 fi
 calculatedminfree=`echo $calculatedmb | awk -F , '{print $1*256","$2*256","$3*256","$4*256","$5*256","$6*256}'`
 calculatedmbnosp=`echo $calculatedminfree | awk -F , '{print $1/256","$2/256","$3/256","$4/256","$5/256","$6/256}'`
 echo " settings are recommended!"
 echo ""
 if [ "$currentminfree" != "$calculatedminfree" ]; then
	echo "          -=SuperMinFree Calculator=- says:"
	echo ""
	echo "   Cust-OOMize with $calculatedmb MB!"
	echo ""
	echo "   Slot 3 Sets Free RAM & Is Your Task Killer!"
	echo ""
 else
	echo " BUT SuperMinFree Calculator Values are set..."
	echo ""
	echo $line
	echo "               SO THAT'S COOL TOO!"
 fi
 echo $line
 if [ "$currentminfree" != "`cat /sdcard/V6_SuperCharger/SuperChargerMinfree`" ] && [ "$ran" -eq 0 ]; then
	echo "   Re-SuperCharger Settings on SD Card..."
	echo ""
	awk -F , '{print "                    "$1/256", "$2/256", "$3/256", "$4/256", "$5/256", "$6/256" MB"}' /sdcard/V6_SuperCharger/SuperChargerMinfree
	echo $line
 fi 2>/dev/null
 if [ "$allrcpaths" ]; then
	echo ""
	echo " STOCK ROMS will benefit most from Services..."
	echo "   ...since those builds have no init.d support!"
	echo ""
	if [ "$scservice" ] || [ "$bpservice" ] || [ ! "$scminfree" ]; then
		echo $line
		echo -n "       SuperCharger Service is "
		if [ "$scservice" ]; then echo "Installed!"
		else echo "NOT Installed!"
		fi
		echo $line
		echo -n "   BulletProof Apps Service is "
		if [ "$bpservice" ]; then echo "Installed!"
		else echo "NOT Installed!"
		fi
	else
		if [ "$scpercent" -lt 100 ]; then echo $line; fi
		echo " Sorry, your ROM wipes service entries on boot."
		if [ "$scpercent" -eq 100 ]; then
			echo ""
			echo $line
			echo " You're 100% SuperCharged so it doesn't matter!"
		fi
	fi
	echo $line
 fi
 echo -n "        Nitro Lag Nullifier is "
 if [ "`grep Nullifier /system/build.prop`" ]; then echo "Installed!"
 else echo "NOT Installed!"
 fi
 echo $line
 if [ ! -f "`ls /system/etc/init.d/*BulletProof_Apps*`" ] && [ ! -f "/data/97BulletProof_Apps.sh" ]; then echo "    BulletProof Apps Script is NOT Installed!"
 elif [ "`cat /proc/*/cmdline | grep Bullet`" != "Bullet" ]; then
	for i in `cat /proc/*/cmdline | grep Bullet`; do
		if [ "`echo $i | grep Proof`" ] && [ "`echo $i | grep Effect`" ]; then echo "           $i"
		elif [ "`echo $i | grep Proof`" ] && [ "`echo $i | grep init.d`" ]; then echo " `echo $i | sed 's/.*etc//'` is Running!"
		elif [ "`echo $i | grep Proof`" ] && [ "`echo $i | grep data`" ]; then echo "$i is Running!"
		fi
		if [ "$i" != "Bullet" ]; then echo $line; fi
	done
	ldone=yes
 elif [ "`ls /system/etc/init.d/*BulletProof_Apps*`" ]; then echo " `ls /system/etc/init.d/*BulletProof_Apps* | sed 's/.*etc//'` is NOT Running!"
 elif [ -f "/data/97BulletProof_Apps.sh" ]; then echo "/data/97BulletProof_Apps.sh is NOT Running!"
 fi 2>/dev/null
 if [ ! "$ldone" ]; then echo $line; fi
 echo ""
 echo " Lag? Disable Lock Home in Memory & Compcache!"
 echo " Also Run Engine Flush Every Few Hours!"
 echo ""
 if [ "$smrun" ]; then
	echo " In Config, select Run as Root & Browse as Root!"
	echo " But DO NOT run this script at boot!"
	echo ""
	echo " For a quick status check..."
	echo " ...put a V6 SuperCharger WIDGET on the desktop!"
 else
	echo " Optimized for display with Script Manager."
	echo ""
	echo " SM can give you a quick status check..."
	echo " ...Put a V6 SuperCharger WIDGET on the desktop!"
	echo "                                   ...Try it! :)"
 fi
 echo ""
 echo $line
 ramkbytesfree=`free | awk '{ print $4 }' | sed -n 2p`
 ramkbytescached=`cat /proc/meminfo | grep Cached | awk '{print $2}' | sed -n 1p`
 ramfree=$(($ramkbytesfree/1024));ramcached=$(($ramkbytescached/1024));ramreportedfree=$(($ramfree + $ramcached))
 echo " True Free $ramfree MB = \"Free\" $ramreportedfree - Cached Apps $ramcached"
 echo $line
 echo "     SuperCharger Level: $scpercent% SuperCharged!"
 echo $line
 if [ "$ran" -eq 1 ] && [ "$scpercent" -lt 100 ]; then
	echo " Did it work? READ ABOVE MESSAGES AFTER REBOOT!"
	echo $line
 fi
 echo -n " Please Enter Option [1 - 30]: "
 if [ "$resuperable" ]; then opt=22; echo $opt
 elif [ "$firstgear" ]; then opt=26; echo $opt
 else read opt
 fi
 $sleep
 echo $line
 busybox echo "            \\\\\\\\ V6 SUPERCHARGER ////"
 echo "             ======================="
 echo ""
 $sleep
 if [ "$opt" -gt 1 ] && [ "$opt" -le 26 ] || [ "$opt" -eq 29 ]; then
	busybox mount -o remount,rw / 2>/dev/null
	busybox mount -o remount,rw rootfs 2>/dev/null
	busybox mount -o remount,rw /system 2>/dev/null
	busybox mount -o remount,rw `busybox mount | grep system | awk '{print $1,$3}' | sed -n 1p` 2>/dev/null
	if [ ! -d "/sqlite_stmt_journals" ]; then
		mkdir /sqlite_stmt_journals
		madesqlitefolder=1
	else madesqlitefolder=0
	fi
	if [ ! "`grep "SuperCharger Installation" /system/build.prop`" ]; then echo "# SuperCharger Installation Marker." >> /system/build.prop; fi
	if [ "$opt" -ne 15 ]; then
		if [ ! -d "/data/V6_SuperCharger" ]; then mkdir /data/V6_SuperCharger; fi
		if [ ! -d "/data/V6_SuperCharger/PowerShift_Scripts" ]; then mkdir /data/V6_SuperCharger/PowerShift_Scripts; fi
		if [ ! -d "/data/V6_SuperCharger/BulletProof_One_Shots" ]; then mkdir /data/V6_SuperCharger/BulletProof_One_Shots; fi
		if [ ! -d "/sdcard/V6_SuperCharger" ]; then	mkdir /sdcard/V6_SuperCharger; fi
		if [ ! -d "/sdcard/V6_SuperCharger/PowerShift_Scripts" ]; then mkdir /sdcard/V6_SuperCharger/PowerShift_Scripts; fi
		if [ ! -d "/sdcard/V6_SuperCharger/BulletProof_One_Shots" ]; then mkdir /sdcard/V6_SuperCharger/BulletProof_One_Shots; fi
	fi
 fi 2>/dev/null
 case $opt in
  1) echo "      V6 SUPERCHARGER AND LAUNCHER STATUS!";;
  2) echo "      256HP UNLEDDED + DIE-HARD LAUNCHER!"
	 CONFIG="256HP UnLedd"
	 MB1=8;MB2=12;MB3=22;MB4=24;MB5=26;MB6=28;;
  3) echo "       256HP LEDDED + DIE-HARD LAUNCHER!"
	 CONFIG="256HP Ledded"
	 MB1=8;MB2=12;MB3=26;MB4=28;MB5=30;MB6=32;;
  4) echo "   256HP SUPER UNLEDDED + DIE-HARD LAUNCHER!"
	 CONFIG="256HP Super"
	 MB1=8;MB2=12;MB3=28;MB4=30;MB5=35;MB6=50;;
  5) echo "      512HP UNLEDDED + DIE-HARD LAUNCHER!"
	 CONFIG="512HP UnLedd"
	 MB1=8;MB2=14;MB3=40;MB4=50;MB5=60;MB6=75;;
  6) echo "       512HP LEDDED + DIE-HARD LAUNCHER!"
	 CONFIG="512HP Ledded"
	 MB1=8;MB2=14;MB3=55;MB4=70;MB5=85;MB6=100;;
  7) echo "   512HP SUPER UNLEDDED + DIE-HARD LAUNCHER!"
	 CONFIG="512HP Super"
	 MB1=8;MB2=14;MB3=75;MB4=90;MB5=95;MB6=125;;
  8) echo "   768HP SUPER UNLEDDED + DIE-HARD LAUNCHER!"
	 if [ "$ram" -le 256 ]; then
		echo $line
		$sleep
		echo "      WHOA! There's NOT Enough RAM! lulz!"
		opt=23
	 else
		CONFIG="768HP Super"
		MB1=8;MB2=16;MB3=150;MB4=165;MB5=180;MB6=200
	 fi;;
  9) echo "   1000HP SUPER UNLEDDED + DIE-HARD LAUNCHER!"
	 if [ "$ram" -le 256 ]; then
		echo $line
		$sleep
		echo "      WHOA! There's NOT Enough RAM! lulz!"
		opt=23
	 else
		CONFIG="1000HP Super"
		MB1=8;MB2=16;MB3=200;MB4=220;MB5=240;MB6=275
	 fi;;
  10)echo "       CUST-OOMIZER + DIE-HARD LAUNCHER!"
	 CONFIG="Cust-OOMized"
	 echo $line
	 echo ""
	 $sleep
	 echo " Your Current Minfree values are..."
	 echo ""
	 $sleep
	 echo $currentminfree | awk -F , '{print "               "$1/256", "$2/256", "$3/256", "$4/256", "$5/256", "$6/256" MB"}'
	 echo ""
	 $sleep
	 echo " Your SuperMinFree Calculator values are..."
	 echo ""
	 $sleep
	 echo "               $calculatedmb MB"
	 echo ""
	 if [ "$sccminfree" ]; then
		$sleep
		echo " Your Prior Cust-OOMizer values are..."
		echo ""
		$sleep
		echo $sccminfree | awk -F , '{print "               "$1/256", "$2/256", "$3/256", "$4/256", "$5/256", "$6/256" MB"}'
		echo ""
	 fi
	 if [ "$scminfreeold" ]; then
		$sleep
		echo " Your Prior V6 Minfrees are..."
		echo ""
		$sleep
		echo $scminfreeold | awk -F , '{print "               "$1/256", "$2/256", "$3/256", "$4/256", "$5/256", "$6/256" MB"}'
		echo ""
	 fi
	 echo $line
	 if [ "$currentminfree" != "$calculatedminfree" ] && [ "$calculatedminfree" != "$scminfree" ] && [ "$calculatedminfree" != "$sccminfree" ]; then
		echo ""
		$sleep
		echo " Apply SuperMinFree Calculator Settings?"
		echo ""
		$sleep
		echo -n " Enter Y for Yes, any key for No: "
		read calculated
		echo ""
		echo $line
		case $calculated in
		  y|Y)calculatorcharge=1
			  CONFIG="Calculator"
			  MB1=`echo $calculatedmbnosp | awk -F , '{print $1}'`;MB2=`echo $calculatedmbnosp | awk -F , '{print $2}'`;MB3=`echo $calculatedmbnosp | awk -F , '{print $3}'`;MB4=`echo $calculatedmbnosp | awk -F , '{print $4}'`;MB5=`echo $calculatedmbnosp | awk -F , '{print $5}'`;MB6=`echo $calculatedmbnosp | awk -F , '{print $6}'`
			  echo "  Cust-OOMizing with SuperMinFree Calculator!";;
			*);;
		esac
	 fi 2>/dev/null
	 if [ "$currentminfree" != "$sccminfree" ] && [ "$currentminfree" != "$scminfree" ] && [ "$calculatorcharge" -eq 0 ]; then
		echo ""
		$sleep
		echo " Apply Quick SuperCharge of Current Minfrees?"
		echo ""
		$sleep
		echo " If Yes, these become your Cust-OOMizer values."
		echo ""
		$sleep
		echo -n " Enter Y for Yes, any key for No: "
		read quick
		echo ""
		echo $line
		case $quick in
		  y|Y)quickcharge=1
			  MB1=`echo $currentminfree | awk -F , '{print $1/256}'`;MB2=`echo $currentminfree | awk -F , '{print $2/256}'`;MB3=`echo $currentminfree | awk -F , '{print $3/256}'`;MB4=`echo $currentminfree | awk -F , '{print $4/256}'`;MB5=`echo $currentminfree | awk -F , '{print $5/256}'`;MB6=`echo $currentminfree | awk -F , '{print $6/256}'`
			  echo "   Quick Cust-OOMizer Settings will be Saved!";;
			*);;
		esac
	 fi 2>/dev/null
	 if [ "$sccminfree" ] && [ "$currentminfree" != "$sccminfree" ] && [ "$quickcharge" -eq 0 ] && [ "$calculatorcharge" -eq 0 ]; then
		echo ""
		$sleep
		echo " Restore Previous Cust-OOMizer Settings?"
		echo ""
		$sleep
		echo -n " Enter Y for Yes, any key for No: "
		read crestore
		echo ""
		echo $line
		case $crestore in
		  y|Y)restore=1
			  MB1=`echo $sccminfree | awk -F , '{print $1/256}'`;MB2=`echo $sccminfree | awk -F , '{print $2/256}'`;MB3=`echo $sccminfree | awk -F , '{print $3/256}'`;MB4=`echo $sccminfree | awk -F , '{print $4/256}'`;MB5=`echo $sccminfree | awk -F , '{print $5/256}'`;MB6=`echo $sccminfree | awk -F , '{print $6/256}'`
			  echo "    Cust-OOMizer Settings will be Restored!";;
			*);;
		esac
	 fi
	 if [ "$scminfreeold" ] && [ "$currentminfree" != "$scminfreeold" ] && [ "$sccminfree" != "$scminfreeold" ] && [ "$calculatedminfree" != "$scminfreeold" ] && [ "$quickcharge" -eq 0 ] && [ "$calculatorcharge" -eq 0 ] && [ "$restore" -eq 0 ]; then
		echo ""
		$sleep
		echo " Revert to Prior V6 Minfrees?"
		echo ""
		$sleep
		echo -n " Enter Y for Yes, any key for No: "
		read prior
		echo ""
		echo $line
		case $prior in
		  y|Y)revert=1
			  MB1=`echo $scminfreeold | awk -F , '{print $1/256}'`;MB2=`echo $scminfreeold | awk -F , '{print $2/256}'`;MB3=`echo $scminfreeold | awk -F , '{print $3/256}'`;MB4=`echo $scminfreeold | awk -F , '{print $4/256}'`;MB5=`echo $scminfreeold | awk -F , '{print $5/256}'`;MB6=`echo $scminfreeold | awk -F , '{print $6/256}'`
			  echo "      Prior V6 Minfrees will be Restored!";;
			*);;
		esac
	 fi 2>/dev/null
	 if [ "$restore" -eq 0 ] && [ "$quickcharge" -eq 0 ] && [ "$calculatorcharge" -eq 0 ] && [ "$revert" -eq 0 ]; then
		echo " Running Cust-OOMizer..."
		echo $line
		$sleep
		busybox echo "            \\\\\\\\ V6 CUST-OOMIZER ////"
		echo "             ======================="
		echo ""
		$sleep
		echo " Enter your desired lowmemorykiller OOM levels!"
		echo ""
		$sleep
		echo " Slot 3 determines your fee RAM the most!!"
		echo ""
		$sleep
		echo " Note: Enter \"X\" to Exit at any time :)"
		echo ""
		echo $line
		echo ""
		$sleep
		while [ "$break" -eq 0 ]; do
			echo -n "               Slot 1: ";read MB1
			if [ "$MB1" = "X" ] || [ "$MB1" = "x" ]; then break=1
			elif [ "$MB1" -gt 0 ] 2>/dev/null; then break
			else echo " Input Error!                      Try again :)";sleep 2; fi
		done
		while [ "$break" -eq 0 ]; do
			echo -n "                 Slot 2: ";read MB2
			if [ "$MB2" = "X" ] || [ "$MB2" = "x" ]; then break=1
			elif [ "$MB2" -gt 0 ] 2>/dev/null; then break
			else echo " Input Error!                      Try again :)";sleep 2; fi
		done
		while [ "$break" -eq 0 ]; do
			echo -n "                   Slot 3: ";read MB3
			if [ "$MB3" = "X" ] || [ "$MB3" = "x" ]; then break=1
			elif [ "$MB3" -gt 0 ] 2>/dev/null; then break
			else echo " Input Error!                      Try again :)";sleep 2; fi
		done
		while [ "$break" -eq 0 ]; do
			echo -n "                     Slot 4: ";read MB4
			if [ "$MB4" = "X" ] || [ "$MB4" = "x" ]; then break=1
			elif [ "$MB4" -gt 0 ] 2>/dev/null; then break
			else echo " Input Error!                      Try again :)";sleep 2; fi
		done
		while [ "$break" -eq 0 ]; do
			echo -n "                       Slot 5: ";read MB5
			if [ "$MB5" = "X" ] || [ "$MB5" = "x" ]; then break=1
			elif [ "$MB5" -gt 0 ] 2>/dev/null; then break
			else echo " Input Error!                      Try again :)";sleep 2; fi
		done
		while [ "$break" -eq 0 ]; do
			echo -n "                         Slot 6: ";read MB6
			if [ "$MB6" = "X" ] || [ "$MB6" = "x" ]; then break=1
			elif [ "$MB6" -gt 0 ] 2>/dev/null; then break
			else echo " Input Error!                      Try again :)";sleep 2; fi
		done
		echo ""
		if [ "$break" -eq 0 ]; then
			$sleep
			echo $line
			echo " CONFIRM! $MB1, $MB2, $MB3, $MB4, $MB5, $MB6 MB?"
			echo $line
			echo ""
			$sleep
			echo -n " Enter N for No, any key for Yes: "
			read custOOM
			echo ""
			case $custOOM in
			  n|N)opt=23;;
				*)echo $line
				  echo "        Cust-OOMizer Settings Accepted!";;
			esac
		else opt=23
		fi
	 fi;;
  11)echo " OOM GROUPING FIXES PLUS..."
	 echo ""
	 echo "                      ...HARD TO KILL LAUNCHER!"
	 if [ "$gb" -eq 0 ] || [ "$ics" -eq 1 ]; then
		echo $line
		echo ""
		$sleep
		if [ "$ics" -eq 1 ]; then
			echo " Unable to SuperCharge launchers on ICS..."
			echo ""
			$sleep
			echo "          ...can only apply adj/Grouping Fix..."
			echo ""
			$sleep
			echo "                           ...and other tweaks!"
			echo ""
			$sleep
			echo $line
			if [ "$icssced" ]; then	echo " Hey your services.jar is patched so who cares!"; fi
		else
			echo " Sorry, Hard To Kill Launcher..."
			echo ""
			$sleep
			echo "             ...is not available on this ROM..."
			echo ""
			opt=23
		fi
	 fi;;
  12)echo " OOM GROUPING FIXES PLUS..."
	 echo ""
	 echo "                          ...DIE-HARD LAUNCHER!"
	 if [ "$ics" -eq 1 ]; then
		echo $line
		echo ""
		$sleep
		echo " Unable to SuperCharge launchers on ICS..."
		echo ""
		$sleep
		echo "          ...can only apply adj/Grouping Fix..."
		echo ""
		$sleep
		echo "                           ...and other tweaks!"
		echo ""
		$sleep
		echo $line
		if [ "$icssced" ]; then	echo " Hey your services.jar is patched so who cares!"; fi
	 fi;;
  13)echo " OOM GROUPING FIXES PLUS..."
	 echo ""
	 echo "                       ...BULLETPROOF LAUNCHER!"
	 echo $line
	 echo ""
	 $sleep
	 if [ "$ics" -eq 1 ]; then
		echo " Unable to SuperCharge launchers on ICS..."
		echo ""
		$sleep
		echo "          ...can only apply adj/Grouping Fix..."
		echo ""
		$sleep
		echo "                           ...and other tweaks!"
		echo ""
		$sleep
		echo $line
		if [ "$icssced" ]; then	echo " Hey your services.jar is patched so who cares!"; fi
	 else
		echo "               =================="
		busybox echo "                \\\\\\ WARNING! ///"
		echo "                 =============="
		echo ""
		$sleep
		echo $line
		echo "    This Breaks \"Long-Press Back To Kill\"!"
		echo $line
		echo ""
		$sleep
		echo " If you don't have or don't use this feature..."
		echo ""
		$sleep
		echo " ...or don't know what it is - just say \"Yes\" ;)"
		echo ""
		echo $line
		echo ""
		$sleep
		echo -n " BulletProof? Enter Y for Yes, any key for No:"
		read breakback
		echo ""
		echo $line
		case $breakback in
		  y|Y)echo "  Break Back Button... erm... BulletProofing!";;
			*)echo "    OK... Die-Hard Launcher Is Great Anyway!"
			  opt=23;;
		esac
	 fi;;
  14)echo "              ===================="
	 busybox echo "             //// UNKERNELIZER \\\\\\\\"
	 if [ ! -f "`ls /system/etc/init.d/*SuperCharger*`" ] && [ ! -f "`ls /data/*SuperCharger*`" ] && [ ! -f "$initrcpath" ] && [ ! -f "$initrcbackup" ] && [ ! "$allrcbackups" ]; then
		echo $line
		$sleep
		echo "        There's Nothing to UnKernelize!"
		opt=23
	 fi 2>/dev/null;;
  15)echo "             ======================="
	 busybox echo "            //// UN-SUPERCHARGER \\\\\\\\"
	 echo $line
	 echo ""
	 $sleep
	 echo -n "         :|"
	 sleep 3
	 echo -n "    !@#?&%(*)(*)&(!)?!"
	 sleep 3
	 echo "    :/"
	 sleep 3
	 echo ""
	 echo $line
	 echo ""
	 echo " WHAT? UnSuperCharge? Are you sure?"
	 echo ""
	 $sleep
	 echo -n " Enter Y for Yes, any key for No: "
	 read unsuper
	 echo ""
	 echo $line
	 case $unsuper in
		y|Y)echo " Well... okay then... be like that! :p";;
		  *)echo " False alarm... *whew*"
			opt=23;;
	 esac;;
  16)echo "            ======================="
	 busybox echo "           //// THE bOOM STICK! \\\\\\\\";;
  17)echo "            ========================"
	 busybox echo "           //// BULLETPROOF APPS \\\\\\\\";;
  18)echo "              ===================="
	 busybox echo "             //// ENGINE FLUSH \\\\\\\\";;
  19)echo "                ================="
	 busybox echo "               //// DETAILING \\\\\\\\";;
  20)echo "           ==========================="
	 busybox echo "          //// NITRO LAG NULLIFIER \\\\\\\\";;
  21)echo "             ======================"
	 busybox echo "            //// SYSTEM INSTALL \\\\\\\\";;
  22)echo "             ======================="
	 busybox echo "            //// RE-SUPERCHARGER \\\\\\\\";;
  23)echo "              ====================="
	 busybox echo "             //// POWERSHIFTING \\\\\\\\"
	 echo $line
	 echo ""
	 $sleep
	 echo " Now you can quickly switch minfree settings!"
	 echo ""
	 $sleep
	 echo " When you run a preset or the Cust-OOMizer..."
	 echo ""
	 $sleep
	 echo "                      ...a PowerShift Script..."
	 echo ""
	 $sleep
	 echo "                   ...is saved to your SD Card!"
	 echo ""
	 echo $line
	 $sleep
	 echo " You can find them in the folder..."
	 echo ""
	 $sleep
	 echo "  .../data/V6_SuperCharger/PowerShift_Scripts :D"
	 echo $line
	 echo ""
	 $sleep
	 echo " Create \"Quick Widgets\" for them..."
	 echo ""
	 $sleep
	 echo "            ...and PowerShift between settings!"
	 echo ""
	 $sleep
	 echo $line
	 echo " They will also be your new SuperCharger values!"
	 echo $line
	 echo ""
	 $sleep
	 echo " They will have descriptive names..."
	 echo ""
	 $sleep
	 echo "                  ...indicating minfree values."
	 echo ""
	 $sleep
	 echo " Different Cust-OOMizer settings will be saved!"
	 echo "";;
  24)echo "              ====================="
	 busybox echo "             //// Owner's Guide \\\\\\\\"
	 echo $line
	 echo ""
	 $sleep
	 echo " Loading Help File..."
	 echo ""
	 $sleep
	 su -c "LD_LIBRARY_PATH=/vendor/lib:/system/lib am start -a android.intent.action.VIEW -n com.android.browser/.BrowserActivity -d file:///sdcard/!SuperCharger.html"
	 echo ""
	 echo " I hope that helped! :)"
	 echo "";;
  25)echo "               ==================="
	 busybox echo "              //// HELP CENTRE \\\\\\\\"
	 echo $line
	 echo ""
	 $sleep
	 echo " Loading web site for more help & updates..."
	 echo ""
	 $sleep
	 su -c "LD_LIBRARY_PATH=/vendor/lib:/system/lib am start http://goo.gl/qM6yR"
	 echo ""
	 echo " I hope that helped! :)"
	 echo "";;
  26)echo "             ======================"
	 busybox echo "            //// DRIVER OPTIONS \\\\\\\\"
	 echo $line
	 echo ""
	 $sleep
	 echo " Scrolling Speed Options..."
	 echo " =========================="
	 echo ""
	 $sleep
	 echo " 1 (fast), 2 (normal), 3 (slow)"
	 while :; do
		echo ""
		$sleep
		echo -n " Please select scrolling speed (1 - 3): "
		read speed
		case $speed in
		  0)sleep="sleep $speed";break;;
		  1)sleep="sleep $speed";break;;
		  2)sleep="sleep $speed";break;;
		  3)sleep="sleep $speed";break;;
		  *)echo ""
			echo "      Invalid entry... Please try again :)";;
		esac
	 done
	 echo ""
	 echo $line
	 echo "       Scrolling Speed is now set to $speed..."
	 echo $line
	 echo ""
	 $sleep
	 if [ "$ics" -eq 0 ]; then
		echo " Build.prop vs Local.prop..."
		echo " ==========================="
		echo ""
		$sleep
		echo $line
		if [ ! "$buildprop" ] && [ ! -f "/data/local.prop" ]; then
			echo $line
			echo "  WARNING: You don't have a /data/local.prop!"
			echo $line
			echo ""
			$sleep
			echo " You can try creating /data/local.prop..."
			echo ""
			$sleep
			echo "  ...and see if the launcher gets SuperCharged!"
			echo ""
			$sleep
			echo $line
		fi
		echo "    Using local.prop is STRONGLY recommended!"
		echo $line
		echo ""
		$sleep
		echo " But if the launcher DOES'T get SuperCharged..."
		echo ""
		$sleep
		echo " You can apply MEM and ADJ values..."
		echo ""
		$sleep
		echo "        ...to build.prop instead of local.prop!"
		echo ""
		$sleep
		echo " This is more likely to stick but is RISKIER..."
		echo ""
		$sleep
		echo $line
		echo " WARNING: There is a small chance of bootloops!"
		echo ""
		$sleep
		echo "                 ...so have a backup available!"
		echo $line
		echo ""
		$sleep
		echo -n " Do you want to use Build.prop"
		if [ ! "$buildprop" ] && [ ! -f "/data/local.prop" ]; then
			echo "..."
			echo ""
			$sleep
			echo "                 ...or create /data/local.prop?"
		else echo "?"
		fi
		echo ""
		$sleep
		buildpropold=$buildprop
		echo -n " Enter (B)uild.prop or any key for local.prop:"
		read buildpropopt
		echo ""
		echo $line
		case $buildpropopt in
		  b|B)echo "        Okay! build.prop Mode Activated!"
			  buildprop=1;;
			*)echo "        Okay! local.prop Mode Activated!"
			  buildprop=0;;
		esac
		echo $line
		echo ""
		$sleep
		showbuildpropopt=0
	 else buildprop=1
	 fi
	 if [ "$buildpropold" ] && [ "$buildprop" != "$buildpropold" ]; then
		if [ "$buildpropold" -eq 1 ]; then
			sed -n '/.*V6 SuperCharger/,/.*V6 SuperCharged/p' /system/build.prop >> /data/local.prop
			echo " Copied SuperCharger entries to local.prop..."
			echo ""
			$sleep
			sed -i '/.*V6 SuperCharger/,/.*V6 SuperCharged/d' /system/build.prop
			echo "   ...and cleaned SuperCharger from build.prop!"
		elif [ -f "/data/local.prop" ]; then
			sed -n '/.*V6 SuperCharger/,/.*V6 SuperCharged/p' /data/local.prop >> /system/build.prop
			echo " Copied SuperCharger entries to build.prop..."
			echo ""
			$sleep
			sed -i '/.*V6 SuperCharger/,/.*V6 SuperCharged/d' /data/local.prop
			echo "   ...and cleaned SuperCharger from local.prop!"
		fi
		echo ""
		echo $line
		echo ""
		$sleep
		echo -n " Press The Enter Key..."
		read enter
		echo ""
		echo $line
		echo ""
		$sleep
	 fi
	 if [ -d "/system/etc/init.d" ]; then
		echo " System Integration - $initrcpath1 Options..."
		echo " =================="
		echo ""
		$sleep
		echo " SuperCharger can attempt greater integration..."
		echo ""
		$sleep
		echo "  ...with system files on boot using init.rc..."
		echo ""
		$sleep
		echo " If it sticks, it makes for a much cleaner mod!"
		echo ""
		$sleep
		echo " Root access issues are rare, but can occur..."
		echo ""
		echo $line
		$sleep
		echo " Regardless, a SuperCharged init.rc file..."
		echo ""
		$sleep
		echo "               ...can always be found in /data!"
		echo $line
		$sleep
		echo " Note: You can bake $initrcpath into your ROM!"
		echo $line
		echo ""
		$sleep
		echo " Custom ROMs: Settings should stick either way!"
		echo ""
		echo $line
		echo ""
		$sleep
		echo -n " Integrate? Enter Y for Yes, any key for No: "
		read bake
		echo ""
		echo $line
		case $bake in
		  y|Y)initrc=1
			  echo "    System Integration of $initrcpath1 is now ON!";;
			*)initrc=0
			  echo "    System Integration of $initrcpath1 is now OFF!";;
		esac
		echo $line
		echo ""
		$sleep
	 else initrc=0
	 fi
	 echo " Disable AWESOME V6 Animation? (Say \"NO\"!) :P"
	 echo "         ======="
	 echo ""
	 $sleep
	 echo -n " Enter Y for Yes, any key for No: "
	 read animate
	 echo ""
	 echo $line
	 case $animate in
	  y|Y)animation=0
		  echo "          Boo... Animation is OFF... :/";;
		*)animation=1
		  echo "          Yay... Animation is ON... :D";;
	 esac
	 echo $line
	 echo ""
	 $sleep
	 echo "$speed,$buildprop,$animation,$initrc" > /data/V6_SuperCharger/SuperChargerOptions
	 cp -fr /data/V6_SuperCharger/SuperChargerOptions /sdcard/V6_SuperCharger/SuperChargerOptions
	 echo "        Driver Options Have Been Saved!"
	 echo ""
	 $sleep
	 echo $line
	 echo "             Now Off To The Races!"
	 firstgear=;;
  27)echo "                ================="
	 busybox echo "               //// OFF TOPIC \\\\\\\\"
	 echo $line
	 echo ""
	 $sleep
	 echo " Ok this is \"Off Topic\"..."
	 echo ""
	 $sleep
	 echo " ...but maybe there's a good reason..."
	 echo ""
	 $sleep
	 echo "            ...why this script is so popular :p"
	 echo ""
	 $sleep
	 echo " It's profound... so just read the link..."
	 echo ""
	 $sleep
	 echo "  ...and decide for yourself if it makes sense!"
	 echo ""
	 $sleep
	 echo " If you don't like it... remember..."
	 echo ""
	 $sleep
	 echo "                  ...don't shoot the messenger!"
	 echo ""
	 $sleep
	 echo " Either way... I'm not crazy..."
	 echo ""
	 $sleep
	 echo "      ...and you may learn something..."
	 echo ""
	 $sleep
	 echo "                    ...even if you're a doctor!"
	 echo ""
	 $sleep;$sleep
	 su -c "LD_LIBRARY_PATH=/vendor/lib:/system/lib am start http://www.cancertutor.com/"
	 echo "";;
  28)echo $line
	 echo "                    !!POOF!!"
	 echo $line
	 echo ""
	 sleep 2
	 busybox sync
	 echo 1 > /proc/sys/kernel/sysrq 2>/dev/null
	 echo b > /proc/sysrq-trigger 2>/dev/null
	 echo "  If it don't go poofie, just reboot manually!"
	 echo ""
	 busybox reboot;;
  29)echo "              ==================="
	 busybox echo "             //// SUPERCLEAN! \\\\\\\\"
	 if [ "$ram1" -ne "$ram2" ]; then opt=23; fi;;
  30)echo " Did you find this useful? Feedback is welcome!";;
  *) echo "   #!*@%$*?%@&)&*#!*?(*)(*)&(!)%#!&?@#$*%&?&$%$*#?!"
	 echo ""
	 sleep 2
	 echo "     oops.. typo?!  $opt is an Invalid Option!"
	 echo ""
	 sleep 2
	 echo "            1 <= Valid Option => 30 !!"
	 echo ""
	 sleep 2
	 echo -n "  hehe... Press the Enter Key to continue... ;)"
	 read enter
	 opt=0;;
 esac
 if [ "$opt" -ne 0 ] && [ "$opt" -ne 1 ] && [ "$opt" -ne 14 ] && [ "$opt" -ne 15 ] && [ "$opt" -ne 16 ] && [ ! "$opt" -gt 23 ] && [ "$ram1" -ne "$ram2" ]; then opt=69; resuperable=; firstgear=; fi
 if [ "$opt" -eq 1 ]; then
	echo $line
	echo ""
	$sleep
	echo " Out Of Memory (OOM) / lowmemorykiller values:"
	echo ""
	$sleep
	echo "        "$currentminfree pages
	echo ""
	$sleep
	echo $currentminfree | awk -F , '{print "  Which means: "$1/256", "$2/256", "$3/256", "$4/256", "$5/256", "$6/256" MB"}'
	echo ""
	echo $line
	echo ""
	$sleep
	echo "           Home Launcher Priority is: $HL"
	echo ""
	$sleep
	echo "          Foreground App Priority is: $FA"
	echo ""
	$sleep
	if [ "$gb" -eq 1 ] || [ "$icssced" ]; then
		echo "         Perceptible App Priority is: $PA"
		echo ""
		$sleep
	fi
	echo "             Visible App Priority is: $VA"
	echo ""
	echo $line
	echo ""
	$sleep
	if [ "$status" -eq 4 ]; then
		echo " Launcher is greater than Visible App..."
		echo ""
		$sleep
		echo $line
		echo "     Wow, that's one weak ass launcher! :("
	elif [ "$status" -eq 3 ]; then
		echo " Launcher is equal to Visible App..."
		echo ""
		$sleep
		echo "          ...Home Launcher is Locked In Memory!"
		echo ""
		$sleep
		echo $line
		echo "      meh... that's still pretty weak! :P"
	elif [ "$status" -eq 0 ]; then
		echo " Launcher is equal to Foreground App..."
		echo ""
		$sleep
		if [ "$gb" -eq 1 ] || [ "$icssced" ]; then
			echo "           ...is less than Perceptible App..."
			echo ""
			$sleep
		fi
		echo "             ...and is less than Visible App..."
		echo ""
		$sleep
		echo $line
		echo "         Home Launcher is BULLETPROOF!"
	elif [ "$status" -eq 1 ]; then
		echo " Launcher is greater than Foreground App..."
		echo ""
		$sleep
		if [ "$gb" -eq 1 ] || [ "$icssced" ]; then
			echo "           ...is less than Perceptible App..."
			echo ""
			$sleep
		fi
		echo "             ...and is less than Visible App..."
		echo ""
		$sleep
		echo $line
		echo "          Home Launcher is DIE-HARD!"
	else
		echo " Launcher is greater than Foreground App..."
		echo ""
		$sleep
		if [ "$gb" -eq 1 ] || [ "$icssced" ]; then
			echo "            ...is equal to Perceptible App..."
			echo ""
			$sleep
		fi
		echo "             ...and is less than Visible App..."
		echo ""
		$sleep
		echo $line
		echo "      Home Launcher is very HARD TO KILL!"
	fi
	echo $line
	echo ""
	$sleep
	echo -n " Press The Enter Key..."
	read enter
	echo ""
	echo $line
	echo ""
	echo " SuperCharger and Launcher Status..."
	echo ""
	$sleep
	echo "     ...by -=zeppelinrox=- @ XDA & Droid Forums"
	sleep 2
 elif [ "$opt" -ge 2 ] && [ "$opt" -le 30 ]; then
	if [ "$opt" -le 22 ] || [ "$opt" -eq 29 ]; then
		echo $line
		echo ""
		$sleep
	fi
	if [ "$opt" -le 13 ] || [ "$opt" -eq 22 ]; then
		if [ "`busybox find / -iname *artprojects.RAM*.apk -maxdepth 3`" ]; then
			echo $line
			echo "         WARNING: Incompatibilty Error!"
			echo $line
			echo ""
			$sleep
			echo " A memory app of questionable integrity found!"
			echo ""
			$sleep
			echo " To ensure REAL SUPERCHARGING..."
			echo "           =================="
			echo ""
			$sleep
			echo "    ...with A REAL SUPERCHARGED LAUNCHER..."
			echo "            ============================"
			echo ""
			$sleep
			echo "      You need to uninstall that \"app\" :/"
			opt=30
		fi 2>/dev/null
	fi
	if [ "$opt" -eq 22 ]; then
		echo " Your previous V6 SuperCharger Settings are..."
		echo ""
		$sleep
		awk -F , '{print "               "$1/256",",$2/256",",$3/256",",$4/256",",$5/256",",$6/256 " MB"}' /sdcard/V6_SuperCharger/SuperChargerMinfree
		echo ""
		$sleep
		echo " Re-SuperCharge from your SD Card?"
		echo ""
		$sleep
		echo -n " Enter Y for Yes, any key for No: "
		read resuper
		echo ""
		echo $line
		case $resuper in
			y|Y)CONFIG=Re-Super
				echo " Re-SuperCharging from your SD Card..."
				echo $line
				echo ""
				$sleep
				if [ "`ls /sdcard/V6_SuperCharger/SuperCharger*`" ]; then echo "      ...your Driver Options..."; $sleep; cp -fr /sdcard/V6_SuperCharger/SuperCharger* /data/V6_SuperCharger; fi 2>/dev/null
				if [ "`ls /sdcard/V6_SuperCharger/*SuperCharger`" ]; then cp -fr /sdcard/V6_SuperCharger/*SuperCharger /system/etc/init.d; fi 2>/dev/null
				if [ "`ls /sdcard/V6_SuperCharger/*BulletProof_Apps`" ]; then cp -fr /sdcard/V6_SuperCharger/*BulletProof_Apps /system/etc/init.d; fi 2>/dev/null
				if [ "`ls /sdcard/V6_SuperCharger/BulletProof_Apps*`" ]; then cp -fr /sdcard/V6_SuperCharger/BulletProof_Apps* /data/V6_SuperCharger; fi 2>/dev/null
				if [ -f "/sdcard/V6_SuperCharger/99SuperCharger.sh" ]; then echo "      ...your SuperCharger Settings..."; $sleep; cp -fr /sdcard/V6_SuperCharger/99SuperCharger.sh /data; fi
				if [ -f "/sdcard/V6_SuperCharger/97BulletProof_Apps.sh" ]; then echo "      ...your BulletProof Apps Settings..."; $sleep; cp -fr /sdcard/V6_SuperCharger/97BulletProof_Apps.sh /data; fi
				if [ -d "/sdcard/V6_SuperCharger/BulletProof_One_Shots" ]; then cp -fr /sdcard/V6_SuperCharger/BulletProof_One_Shots /data/V6_SuperCharger; fi
				if [ -d "/sdcard/V6_SuperCharger/PowerShift_Scripts" ]; then echo "      ...your PowerShift Scripts..."; $sleep; cp -fr /sdcard/V6_SuperCharger/PowerShift_Scripts /data/V6_SuperCharger; fi
				if [ -f "/sdcard/V6_SuperCharger/!Fast_Engine_Flush.sh" ]; then echo "      ...your \"Quick Widget\" Scripts..."; $sleep; cp -fr /sdcard/V6_SuperCharger/!Fast_Engine_Flush.sh /data/V6_SuperCharger; fi
				if [ -f "/sdcard/V6_SuperCharger/!Detailing.sh" ]; then cp -fr /sdcard/V6_SuperCharger/!Detailing.sh /data/V6_SuperCharger; fi
				if [ -f "/sdcard/V6_SuperCharger/!SuperClean.sh" ]; then cp -fr /sdcard/V6_SuperCharger/!SuperClean.sh /data/V6_SuperCharger; fi
				echo "      ...all \"Terminal Emulator\" Scripts... "; dd if=$0 of=/system/xbin/v6 2>/dev/null
				if [ -f "/data/V6_SuperCharger/SuperChargerAdj" ]; then scadj=`cat /data/V6_SuperCharger/SuperChargerAdj`; fi
				if [ -f "/data/V6_SuperCharger/SuperChargerMinfree" ]; then scminfree=`cat /data/V6_SuperCharger/SuperChargerMinfree`; ReSuperCharge=1
					MB1=`echo $scminfree | awk -F , '{print $1/256}'`;MB2=`echo $scminfree | awk -F , '{print $2/256}'`;MB3=`echo $scminfree | awk -F , '{print $3/256}'`;MB4=`echo $scminfree | awk -F , '{print $4/256}'`;MB5=`echo $scminfree | awk -F , '{print $5/256}'`;MB6=`echo $scminfree | awk -F , '{print $6/256}'`
				fi
				chown 0.0 /system/etc/init.d/*; chmod 777 /system/etc/init.d/*
				chown 0.0 /data/V6_SuperCharger/*; chmod 777 /data/V6_SuperCharger/*
				chown 0.0 /data/V6_SuperCharger/BulletProof_One_Shots/*; chmod 777 /data/V6_SuperCharger/BulletProof_One_Shots/*
				chown 0.0 /data/V6_SuperCharger/PowerShift_Scripts/*; chmod 777 /data/V6_SuperCharger/PowerShift_Scripts/*
				chown 0.0 /system/xbin/v6; chmod 777 /system/xbin/v6
				if [ "`ls /data/*.sh`" ]; then chown 0.0 /data/*.sh; chmod 777 /data/*.sh; fi
				if [ -f "/data/V6_SuperCharger/!Fast_Engine_Flush.sh" ]; then cp -fr /data/V6_SuperCharger/!Fast_Engine_Flush.sh /system/xbin/flush; fi
				if [ -f "/data/V6_SuperCharger/!Detailing.sh" ]; then cp -fr /data/V6_SuperCharger/!Detailing.sh /system/xbin/vac; fi
				if [ -f "/data/V6_SuperCharger/!SuperClean.sh" ]; then cp -fr /data/V6_SuperCharger/!SuperClean.sh /system/xbin/sclean; fi
				echo ""
				echo $line
				$sleep
				echo " ...finished copying files to /system and /data!"
				echo $line
				$sleep
				if [ -f "/data/V6_SuperCharger/SuperChargerOptions" ]; then
					speed=`awk -F , '{print $1}' /data/V6_SuperCharger/SuperChargerOptions`
					sleep="sleep $speed"
					buildprop=`awk -F , '{print $2}' /data/V6_SuperCharger/SuperChargerOptions`
					animation=`awk -F , '{print $3}' /data/V6_SuperCharger/SuperChargerOptions`
					initrc=`awk -F , '{print $4}' /data/V6_SuperCharger/SuperChargerOptions`
				fi
				echo "   Prior scrolling speed of $speed had been set..."
				echo $line
				$sleep
				if [ "$ics" -eq 0 ]; then
					if [ "$buildprop" -eq 0 ]; then	echo "   LOCAL.PROP to be used for SuperCharging!"
					else echo "   BUILD.PROP to be used for SuperCharging!"
					fi
					echo $line
					$sleep
				fi
				if [ "$initrc" -eq 1 ]; then
					echo "   System Integration of $initrcpath1 is ON..."
					echo $line
					$sleep
				elif [ -d "/system/etc/init.d" ]; then
					echo "   System Integration of $initrcpath1 is OFF..."
					echo $line
					$sleep
				fi
				echo ""
				echo " If you previously used Terminal to execute..."
				echo ""
				$sleep
				echo "   ...\"v6\" or \"flush\" or \"vac\" or \"sclean\"..."
				echo ""
				$sleep
				echo $line
				echo "    Those commands will work automagically!"
				echo $line
				echo ""
				$sleep
				echo " But to avoid conflicts (different rom, etc)..."
				echo ""
				$sleep
				echo "        ...some system files were not restored!"
				echo ""
				$sleep
				echo " Nitro Lag Nullifier and 3G TurboCharger..."
				echo ""
				$sleep
				echo "                ...may need to be re-installed!"
				echo ""
				echo $line
				echo ""
				$sleep
				echo " But this comes first... heh..."
				echo ""
				if [ "$ics" -eq 0 ]; then
					$sleep
					echo " Select a launcher to complete Re-SuperCharge!"
					while :; do
						echo ""
						$sleep
						echo -n " Enter (H)TK, (D)ie-Hard, (B)ulletProof: "
						read launcher
						echo ""
						case $launcher in
						  h|H)opt=11;break;;
						  d|D)opt=12;break;;
						  b|B)opt=13;break;;
							*)echo "      Invalid entry... Please try again :)";;
						esac
					done
				else opt=12
				fi
				echo $line
				echo ""
				$sleep;;
			  *)echo " Re-SuperCharging declined... meh..."
				if [ "$firstgear" ]; then
					echo $line
					echo ""
					$sleep
					echo " Going to load Driver Options instead!"
					echo ""
				fi
				resuperable=;opt=23;;
		esac
	fi
	if [ "$opt" -le 15 ]; then
		if [ "$opt" -ne 14 ]; then
			if [ -f "/sdcard/UnSuperCharged.html" ]; then rm /sdcard/UnSuperCharged.html; fi
			if [ -f "/sdcard/UnSuperChargerError.html" ]; then rm /sdcard/UnSuperChargerError.html; fi
			if [ -f "/sdcard/SuperChargerScriptManagerHelp.html" ]; then rm /sdcard/SuperChargerScriptManagerHelp.html; fi
			if [ -f "/sdcard/SuperChargerHelp.html" ]; then rm /sdcard/SuperChargerHelp.html; fi
		fi
		if [ "$opt" -le 10 ] || [ "$ReSuperCharge" -eq 1 ]; then
			SL0=$(($MB0*256));SL1=$(($MB1*256));SL2=$(($MB2*256));SL3=$(($MB3*256));SL4=$(($MB4*256));SL5=$(($MB5*256));SL6=$(($MB6*256))
			echo " zOOM... zOOM..."
			echo ""
			$sleep
		elif [ "$scminfree" ]; then SL0=$(($MB0*256));SL1=`echo $scminfree | awk -F , '{print $1}'`;SL2=`echo $scminfree | awk -F , '{print $2}'`;SL3=`echo $scminfree | awk -F , '{print $3}'`;SL4=`echo $scminfree | awk -F , '{print $4}'`;SL5=`echo $scminfree | awk -F , '{print $5}'`;SL6=`echo $scminfree | awk -F , '{print $6}'`
		fi
		if [ "$opt" -le 13 ]; then
			echo "=============  Information Section  ============"
			echo "             ======================="
			echo ""
			$sleep
			if [ "$showbuildpropopt" -eq 1 ]; then
				echo " Even though you SuperCharged before..."
				echo ""
				$sleep
				echo "        ...Your launcher is NOT SuperCharged..."
				echo ""
				$sleep
				echo " To apply MEM and ADJ values..."
				echo ""
				$sleep
				echo "   ...You can try and use build.prop..."
				echo ""
				$sleep
				echo "                      ...instead of local.prop!"
				echo ""
				$sleep
				echo " This is more likely to work but riskier..."
				echo ""
				$sleep
				echo $line
				echo " WARNING: There is a small chance of bootloops!"
				echo ""
				$sleep
				echo "                 ...so have a backup available!"
				echo $line
				echo ""
				$sleep
				echo " Do you want to use Build.prop?"
				echo ""
				$sleep
				echo -n " Enter Y for Yes, any key for No: "
				read buildpropopt
				echo ""
				case $buildpropopt in
				  y|Y)echo " Okay... will use the build.prop method!"
					  buildprop=1;;
					*)echo " Okay... will try local.prop method again..."
					  buildprop=0;;
				esac
				echo ""
				$sleep
				echo "$speed,$buildprop,$animation,$initrc" > /data/V6_SuperCharger/SuperChargerOptions
				cp -fr /data/V6_SuperCharger/SuperChargerOptions /sdcard/V6_SuperCharger/SuperChargerOptions
				echo " Settings have been saved!"
				echo ""
				$sleep
				echo " Note: This can be changed later under Options!"
				echo ""
				echo $line
				echo ""
				$sleep
			fi
			for p in /system/build.prop /data/local.prop /system/bin/build.prop /system/etc/ram.conf; do
				if [ -f "$p" ]; then
					if [ -f "$p.unsuper" ]; then echo " Leaving ORIGINAL ${p##*/} backup intact..."
					else
						echo " Backing up ORIGINAL ${p##*/}..."
						echo ""
						$sleep
						cp -r $p $p.unsuper
						echo "          ...as $p.unsuper!"
					fi
					echo ""
					$sleep
				fi
			done
			echo $line
			if [ "$ics" -eq 0 ]; then
				echo -n " MEM and ADJ values to be applied to "
				if [ "$buildprop" -eq 0 ]; then echo "LOCAL.PROP!"
				else echo "BUILD.PROP!"
				fi
				echo $line
			fi
			echo ""
			$sleep
		fi
		if [ "$opt" -le 14 ]; then
			if [ -f "$initrcpath1" ] && [ ! -f "$initrcpath" ]; then cp -r $initrcpath1 $initrcpath; fi
			for rc in $initrcpath1 $allrcpaths /system/etc/hw_config.sh; do
				if [ -f "$rc" ]; then
					echo " Found $rc!"
					echo ""
					$sleep
					if [ "$rc" = "$initrcpath1" ]; then
						if [ -f "$initrcbackup" ]; then echo " Backup already exists... leaving backup intact"
						else
							cp -r $initrcpath1 $initrcbackup
							echo " Backing up ORIGINAL settings..."
						fi
					elif [ -f "$rc.unsuper" ]; then echo " Backup already exists... leaving backup intact"
					else
							cp -r $rc $rc.unsuper
							echo " Backing up ORIGINAL settings..."
					fi
					echo ""
					$sleep
					if [ "$opt" -ne 14 ]; then
						if [ "`grep -ls "on boot" $rc`" ] && [ "$rc" != "/system/etc/hw_config.sh" ]; then
							echo $line
							if [ "$rc" = "$initrcpath1" ] && [ "$initrc" -eq 0 ]; then
								echo ""
								echo " System Integration of $initrcpath1 is OFF..."
								echo ""
								$sleep
								echo " But for cooking/baking into ROMs..."
								echo ""
								$sleep
								echo $line
							fi
							if [ "$rc" = "$initrcpath1" ] && [ "$initrc" -eq 0 ]; then echo -n " /data/"
							elif [ "$rc" = "$initrcpath1" ]; then echo -n " /"
							else echo -n " "
							fi
							if [ "$opt" -ge 11 ] && [ "$opt" -le 13 ] && [ "$ReSuperCharge" -eq 0 ] && [ "$ics" -eq 0 ]; then echo "${rc##*/} will be OOM Fixed!"
							else
								echo -n "${rc##*/} will be SuperCharged"
								if [ "$rc" = "$initrcpath1" ]; then echo "!"
								else echo "..."; echo $line; $sleep; echo "  ...it will also run The SuperCharger Service!"
								fi
							fi
						elif [ "$opt" -le 10 ] || [ "$ReSuperCharge" -eq 1 ] && [ "$rc" != "$initrcpath1" ] && [ "$rc" != "/system/etc/hw_config.sh" ]; then echo $line; echo "     It will run The SuperCharger Service!"
						fi
					fi
					echo $line
					echo ""
					$sleep
				fi
			done
		fi
		if [ "$opt" -eq 15 ]; then
			echo " UNSUPERCHARGE..."
			echo ""
			sleep 1
			echo "       ...UNFIX OOM GROUPINGS..."
			echo ""
			sleep 1
			echo "                   ...RESTORE WEAK ASS LAUNCHER"
			echo ""
			echo $line
			$sleep
			echo " Boo... UnSuperCharging Performance...."
			echo $line
			echo ""
			$sleep
			if [ ! -f "`ls /system/etc/init.d/*SuperCharger*`" ] && [ ! -f "`ls /data/*SuperCharger*`" ] && [ ! -f "`ls /system/etc/init.d/*BulletProof_Apps*`" ] && [ ! -f "$initrcpath" ] && [ ! -f "$initrcbackup" ] && [ ! "$allrcbackups" ]; then
				echo " I Got Nothing To Do! Try SuperCharging first!"
				echo ""
				$sleep
				UnSuperCharged=1
				cat > /sdcard/UnSuperCharged.html <<EOF
There was nothing to uninstall!<br>
<br>
For more SuperCharging help and info,<br>
See the <a href="http://goo.gl/qM6yR">V6 SuperCharger Thread</a><br>
Feedback is Welcome!<br>
<br>
-=zeppelinrox=- @ <a href="http://goo.gl/qM6yR">XDA</a> & <a href="http://www.droidforums.net/forum/droid-hacks/148268-script-v6-supercharger-htk-bulletproof-launchers-fix-memory-all-androids.html">Droid</a> Forums<br>
EOF
				echo $line
				echo " See /sdcard/UnSuperCharged.html for assistance!"
				echo $line
				echo ""
				$sleep
			else
				if [ -f "$initrcpath" ]; then rm $initrcpath; fi
				if [ -f "$initrcbackup" ]; then
					echo "                 BACKUP FOUND!"
					echo ""
					$sleep
					echo " Restoring $initrcpath1..."
					echo ""
					echo $line
					echo ""
					$sleep
					cp -fr $initrcbackup $initrcpath1
					rm $initrcbackup
				fi
				for rc in $allrcpaths; do
					if [ ! -f "$rc.unsuper" ]; then
						echo "      ERROR... ERROR... ERROR... ERROR..."
						echo ""
						$sleep
						echo "          AN RC BACKUP WAS NOT FOUND!"
						echo ""
						$sleep
						echo "       CAN'T restore some default values!"
						echo ""
						sleep 3
						UnSuperChargerError=1
						cat > /sdcard/UnSuperChargerError.html <<EOF
The backup file, $rc.unsuper, WAS NOT found!<br>
Please do a manual restore of $rc from your ROM's update file!<br>
<br>
For more SuperCharging help and info,<br>
See the <a href="http://goo.gl/qM6yR">V6 SuperCharger Thread</a><br>
Feedback is Welcome!<br>
<br>
-=zeppelinrox=- @ <a href="http://goo.gl/qM6yR">XDA</a> & <a href="http://www.droidforums.net/forum/droid-hacks/148268-script-v6-supercharger-htk-bulletproof-launchers-fix-memory-all-androids.html">Droid</a> Forums<br>
EOF
						echo $line
						echo " See /sdcard/UnSuperChargerError.html for help!"
						echo $line
						if  [ "${rc##*/}" != "init.rc" ]; then
							echo ""
							sleep 4
							sed -i '/.*V6 SuperCharger/,/.*V6 SuperCharged/d' $rc
							sed -i '/SuperCharger_Service/,/SuperCharged_Service/d' $rc
							sed -i '/BulletProof_Apps_Service/,/BulletProofed_Apps_Service/d' $allrcpaths
							sed -i '/vm\/.*min_free_kbytes*/d' $rc
							sed -i '/vm\/.*oom.*/d' $rc
							sed -i '/vm\/.*overcommit_memory.*/d' $rc
							sed -i '/vm\/.*swappiness*/d' $rc
							sed -i '/kernel\/panic.*/d' $rc
							echo " Cleaned "$rc
							echo ""
							echo $line
						fi
					else
						echo "                 BACKUP FOUND!"
						echo ""
						$sleep
						echo " Restoring ${rc##*/}..."
						mv $rc.unsuper $rc
						echo ""
						echo $line
					fi
					echo ""
					$sleep
				done
				allrcbackups=;ran=0
			fi 2>/dev/null
		fi
		if [ "$opt" -ne 14 ]; then
			for p in /data/local.prop /system/build.prop /system/bin/build.prop /system/etc/ram.conf; do
				if [ -f "$p" ]; then
					chmod 644 $p
					if [ "$opt" -eq 15 ] && [ ! -f "$p.unsuper" ] || [ "$opt" -le 13 ] || [ "$ReSuperCharge" -eq 1 ]; then
						if [ "$UnSuperCharged" -ne 1 ]; then
							echo " Cleaning ADJ values from ${p##*/}..."
							echo ""
							$sleep
						fi
						sed -i '/BEGIN OOM_ADJ_Settings/,/.*V6 SuperCharged/d' $p
						sed -i '/.*_ADJ/d' $p
						if [ "$UnSuperCharged" -ne 1 ]; then
							echo " Cleaning MEM values from ${p##*/}..."
							echo ""
							$sleep
						fi
						sed -i '/.*V6 SuperCharger/,/END OOM_MEM_Settings/d' $p
						if [ "$scminfree" ]; then sed -i '/.*_MEM/d' $p; fi
					fi
				fi
			done
			if [ "$opt" -eq 15 ] || [ "$opt" -le 10 ] || [ "$ReSuperCharge" -eq 1 ] && [ ! "$newsupercharger" ]; then
				if [ "`ls /system/etc/init.d/*SuperCharger*`" ]; then
					echo " Cleaning Up SuperCharge from /init.d folder"
					echo ""
					$sleep
					echo " Cleaning Up Grouping Fixes from /init.d folder"
					echo ""
					$sleep
					rm /system/etc/init.d/*SuperCharger*
				fi 2>/dev/null
				if [ -f "/data/99SuperCharger.sh" ]; then
					echo " Cleaning Up SuperCharge from /data folder"
					echo ""
					$sleep
					echo " Cleaning Up Grouping Fixes from /data folder"
					echo ""
					$sleep
					rm /data/99SuperCharger.sh
				fi
			fi
			if [ "`ls /data/SuperCharger*`" ]; then rm /data/SuperCharger*; fi 2>/dev/null
			if [ "`ls /data/*-\(*\).sh`" ]; then rm /data/*-\(*\).sh; fi 2>/dev/null
			if [ -f "/data/local/userinit.sh" ]; then sed -i '/.*SuperCharger/d' /data/local/userinit.sh; fi
			if [ -f "/system/etc/hw_config.sh" ]; then sed -i '/.*V6 SuperCharger/,/.*V6 SuperCharged/d' /system/etc/hw_config.sh
				if [ "$UnSuperCharged" -ne 1 ] && [ ! "$newsupercharger" ]; then
					echo " Cleaning Up SuperCharge from hw_config.sh..."
					echo ""
					$sleep
				fi
			fi
		fi
		if [ "$opt" -eq 15 ]; then
			if [ -d "/data/V6_SuperCharger" ]; then rm -r /data/V6_SuperCharger; fi
			if [ "`ls /data/*BulletProof_Apps*`" ]; then rm /data/*BulletProof_Apps*; fi 2>/dev/null
			if [ "`ls /data/*SuperCharger*`" ]; then rm /data/*SuperCharger*; fi 2>/dev/null
			if [ "`ls /system/etc/init.d/*BulletProof_Apps*`" ]; then rm /system/etc/init.d/*BulletProof_Apps*; fi 2>/dev/null
			if [ -f "/system/xbin/v6" ]; then rm /system/xbin/v6; fi
			if [ -f "/system/xbin/flush" ]; then rm /system/xbin/flush; fi
			if [ -f "/system/xbin/vac" ]; then rm /system/xbin/vac; fi
			if [ -f "/system/xbin/sclean" ]; then rm /system/xbin/sclean; fi
			if [ "$UnSuperCharged" -ne 1 ]; then
				for p in /data/local.prop /system/build.prop /system/bin/build.prop /system/etc/hw_config.sh /system/etc/ram.conf; do
					if [ -f "$p.unsuper" ]; then
						echo " Restoring ORIGINAL ${p##*/}..."
						echo ""
						$sleep
						mv $p.unsuper $p
					fi
				done
				echo " Removed Kernel/Memory Tweaks..."
				echo ""
				$sleep
				if [ "$UnSuperChargerError" -ne 1 ]; then
					echo " Your ROM's default minfree values are restored!"
					echo ""
					echo $line
					echo ""
					$sleep
				fi
				echo " Out Of Memory (OOM) Groupings UnFixed..."
				echo ""
				$sleep
				echo "                ...OOM Priorities UnFixed..."
				echo ""
				$sleep
				echo "                  Weak Ass Launcher Restored :("
				echo ""
				$sleep
				echo $line
				echo "           UnSuperCharging Complete!"
				echo $line
				echo ""
				$sleep
				echo " REBOOT NOW..."
				echo ""
				$sleep
				echo "           ...FOR UNSUPERCHARGE TO TAKE EFFECT!"
				echo ""
				echo $line
				echo ""
				$sleep
			fi
			echo " UnSuperCharging..."
			echo ""
			$sleep
			echo "     ...by -=zeppelinrox=- @ XDA & Droid Forums"
			sleep 2
		fi
		if [ "$opt" -le 14 ]; then
			if [ "$opt" -eq 14 ]; then
				echo " Removing Kernel/Virtual Memory Tweaks..."
				echo ""
				$sleep
			fi
			for rc in $initrcpath $allrcpaths; do
				if [ "$opt" -ne 14 ]; then
					sed -i '/BEGIN OOM_ADJ_Settings/,/.*V6 SuperCharged/d' $rc
					sed -i '/.*_ADJ/d' $rc
					sed -i '/.*V6 SuperCharger/,/END OOM_MEM_Settings/d' $rc
					if [ "$scminfree" ]; then sed -i '/.*_MEM/d' $rc
						sed -i '/write \/sys\/module\/lowmemorykiller/d' $rc
					else sed -i '/write \/sys\/module\/lowmemorykiller\/parameters\/adj/d' $rc
					fi
				fi
				if [ "$opt" -le 10 ] || [ "$ReSuperCharge" -eq 1 ]; then sed -i '/SuperCharger_Service/,/SuperCharged_Service/d' $rc; fi
				sed -i '/vm\/.*min_free_kbytes*/d' $rc
				sed -i '/vm\/.*oom.*/d' $rc
				sed -i '/vm\/.*overcommit_memory.*/d' $rc
				sed -i '/vm\/.*swappiness*/d' $rc
				sed -i '/kernel\/panic.*/d' $rc
			done
			for sc in /system/etc/init.d/*SuperCharger* /data/99SuperCharger.sh /system/etc/hw_config.sh; do
			  if [ -f "$sc" ]; then
				if [ "$opt" -eq 14 ]; then
					sed -i '/.*-w vm.min_free_kbytes*/d' $sc
					sed -i '/.*oom.*/d' $sc
					sed -i '/.*overcommit_memory.*/d' $sc
					sed -i '/.*swappiness*/d' $sc
					sed -i '/.*panic.*/d' $sc
				elif [ "$sc" != "/system/etc/hw_config.sh" ]; then rm $sc
				fi
			  fi
			done
			if [ "$opt" -eq 14 ]; then
				echo "       ...Kernel/Virtual Memory Tweaks Removed!"
				echo ""
			fi
		fi
		if [ "$opt" -le 13 ]; then
			echo $newscadj > /data/V6_SuperCharger/SuperChargerAdj
			scadj=`cat /data/V6_SuperCharger/SuperChargerAdj`
			adj1=`echo $scadj | awk -F , '{print $1}'`;adj2=`echo $scadj | awk -F , '{print $2}'`;adj3=`echo $scadj | awk -F , '{print $3}'`;adj4=`echo $scadj | awk -F , '{print $4}'`;adj5=`echo $scadj | awk -F , '{print $5}'`;adj6=`echo $scadj | awk -F , '{print $6}'`
			if [ "$opt" -le 10 ] || [ "$ReSuperCharge" -eq 1 ]; then
				if [ "$scminfree" ] && [ ! "$scminfreeold" ]; then
					cp -fr /data/V6_SuperCharger/SuperChargerMinfree /data/V6_SuperCharger/SuperChargerMinfreeOld
					scminfreeold=`cat /data/V6_SuperCharger/SuperChargerMinfreeOld`
				fi
				echo "$SL1,$SL2,$SL3,$SL4,$SL5,$SL6" > /data/V6_SuperCharger/SuperChargerMinfree
				scminfree=`cat /data/V6_SuperCharger/SuperChargerMinfree`
				if [ "$opt" -eq 10 ]; then
					echo $line
					echo ""
					if [ "$restore" -eq 1 ]; then echo "     Restoring Prior Cust-OOMizer Settings!"
					else
						if [ "$sccminfree" ] && [ "$calculatorcharge" -eq 0 ]; then
							echo " Removing Prior Cust-OOMizer Settings..."
							echo ""
							$sleep
						fi
						if [ "$quickcharge" -eq 1 ]; then echo "      Saving Quick Cust-OOMizer Settings!"
						elif [ "$calculatorcharge" -eq 1 ]; then echo "   Applying SuperMinFree Calculator Settings!"
						elif [ "$revert" -eq 1 ]; then echo "        Reverting to Prior V6 Minfrees!"
						else echo "     Saving Your New Cust-OOMizer Settings!"
						fi
					fi
					if [ "$calculatorcharge" -eq 0 ]; then cp -fr /data/V6_SuperCharger/SuperChargerMinfree /data/V6_SuperCharger/SuperChargerCustomMinfree; fi
					echo ""
					$sleep
				fi
				echo $line
				if [ "$ReSuperCharge" -eq 1 ]; then	echo "         Re-SuperCharging Performance!"
				else echo " SuperCharging Performance: $CONFIG!"
				fi
				echo $line
				echo ""
				$sleep
				echo " Out Of Memory (OOM) / lowmemorykiller values:"
				echo ""
				$sleep
				echo $currentminfree | awk -F , '{print "    Old MB = "$1/256", "$2/256", "$3/256", "$4/256", "$5/256", "$6/256" MB"}'
				echo "    New MB = $MB1, $MB2, $MB3, $MB4, $MB5, $MB6 MB"
				echo ""
				$sleep
				echo " Old Pages = "$currentminfree
				echo " New Pages = $scminfree"
				echo ""
				$sleep
			fi
			#
			# MFK Calculator (for min_free_kbytes) created by zeppelinrox.
			#
			if [ "$SL3" -gt 0 ]; then SSMF=$SL3
			else SSMF=`awk -F , '{print $3}' /sys/module/lowmemorykiller/parameters/minfree`
			fi
			MFK=$(($SSMF*4/5))
			echo $line
			echo " Make Foreground AND Background Apps STAY FAST!"
			echo $line
			echo ""
			$sleep
			echo " With The MFK Calculator!"
			echo " ========================"
			echo ""
			$sleep
			echo " Key Settings That Impact Typical Free RAM..."
			echo ""
			$sleep
			if [ "$ics" -eq 1 ] && [ ! "$icssced" ]; then echo -n "         5"
			else echo -n "         $(($adj2+3))"
			fi
			echo " = Secondary Server ADJ"
			echo "         3 = Secondary Server Slot"
			echo "        $((SSMF/256)) = Secondary Server LMK MB"
			echo ""
			$sleep
			echo $line
			echo " Balanced MFK Calculation Complete..."
			echo $line
			echo ""
			$sleep
			echo "      $MFK = Customized MFK (min_free_kbytes)"
			echo ""
			if [ "$buildprop" -eq 1 ]; then prop="/system/build.prop"
			else prop="/data/local.prop"; touch $prop
			fi
			proplist="/system/etc/ram.conf $prop /system/build.prop"
			for p in $proplist ; do
				if [ -f "$p" ]; then
					echo "# V6 SuperCharger, OOM Grouping & Priority Fixes created by zeppelinrox." >> $p
					sed -i '/OOM Grouping/ a\
# SuperMinFree Calculator & MFK Calculator (for min_free_kbytes) created by zeppelinrox also :)\
#\
# See http://goo.gl/krtf9 - Linux Memory Consumption - Nice article!\
# See http://goo.gl/hFdNO - Memory and SuperCharging Overview ...or... "Why \`Free RAM\` Is NOT Wasted RAM!"\
# See http://goo.gl/4w0ba - MFK Calculator Info - explanation for vm.min_free_kbytes.\
#\
# DO NOT DELETE COMMENTS. DELETING COMMENTS WILL BREAK UNINSTALL ROUTINE!\
#\
# BEGIN OOM_MEM_Settings\
# END OOM_MEM_Settings\
# BEGIN OOM_ADJ_Settings\
# END OOM_ADJ_Settings\
# End of V6 SuperCharged Entries.' $p
					if [ "$p" = "/system/etc/ram.conf" ]; then sed -i '/END OOM_ADJ_Settings/ a\
LMK_ADJ="'$scadj'"\
LMK_MINFREE="'$scminfree'"' $p
					else sed -i '/END OOM_ADJ_Settings/ a\
# Miscellaneous Tweaks!\
persist.sys.purgeable_assets=1\
wifi.supplicant_scan_interval=180\
windowsmgr.max_events_per_sec=200\
pm.sleep_mode=1\
# 3G TurboCharger Enhancment!\
net.tcp.buffersize.default=6144,87380,1048576,6144,87380,524288\
net.tcp.buffersize.wifi=524288,1048576,2097152,524288,1048576,2097152\
net.tcp.buffersize.lte=524288,1048576,2097152,524288,1048576,2097152\
net.tcp.buffersize.hsdpa=6144,87380,1048576,6144,87380,1048576\
net.tcp.buffersize.evdo_b=6144,87380,1048576,6144,87380,1048576\
net.tcp.buffersize.umts=6144,87380,1048576,6144,87380,524288\
net.tcp.buffersize.gprs=6144,87380,1048576,6144,87380,524288\
net.tcp.buffersize.edge=6144,87380,524288,6144,16384,262144\
net.tcp.buffersize.hspa=6144,87380,524288,6144,16384,262144' $p
						break
					fi
				fi
			done
			if [ "$ics" -eq 0 ]; then
				for rc in $initrcpath $rcpaths; do
					echo $line
					$sleep
					if [ "`grep -s "on early-boot" $rc`" ]; then sed -i '/on early-boot/ a\
    # V6 SuperCharger, OOM Grouping & Priority Fixes created by zeppelinrox.' $rc
					else sed -i '/on boot/ a\
    # V6 SuperCharger, OOM Grouping & Priority Fixes created by zeppelinrox.' $rc
					fi
					sed -i '/V6 SuperCharger/ a\
    # SuperMinFree Calculator & MFK Calculator (for min_free_kbytes) created by zeppelinrox also :)\
    #\
    # See http://goo.gl/krtf9 - Linux Memory Consumption - Nice article!\
    # See http://goo.gl/hFdNO - Memory and SuperCharging Overview ...or... "Why \`Free RAM\` Is NOT Wasted RAM!"\
    # See http://goo.gl/4w0ba - MFK Calculator Info - explanation for vm.min_free_kbytes.\
    #\
    # DO NOT DELETE COMMENTS. DELETING COMMENTS WILL BREAK UNINSTALL ROUTINE!\
    #\
    # BEGIN OOM_MEM_Settings\
    # END OOM_MEM_Settings\
    # BEGIN OOM_ADJ_Settings\
    # END OOM_ADJ_Settings\
    # End of V6 SuperCharged Entries.' $rc
					if [ "$scminfree" ]; then sed -i '/on boot/ a\
    write /sys/module/lowmemorykiller/parameters/minfree '$scminfree $rc
						sed -i '/BEGIN OOM_MEM_Settings/ a\
    setprop ro.FOREGROUND_APP_MEM '$SL1'\
    setprop ro.VISIBLE_APP_MEM '$SL2'\
    setprop ro.SECONDARY_SERVER_MEM '$SL3'\
    setprop ro.BACKUP_APP_MEM '$SL4'\
    setprop ro.HOME_APP_MEM '$SL0'\
    setprop ro.HIDDEN_APP_MEM '$SL4'\
    setprop ro.EMPTY_APP_MEM '$SL6 $rc
						if [ "$gb" -eq 1 ]; then sed -i '/ro.VISIBLE_APP_MEM/ a\
    setprop ro.PERCEPTIBLE_APP_MEM '$SL0'\
    setprop ro.HEAVY_WEIGHT_APP_MEM '$SL3 $rc
						else sed -i '/ro.HIDDEN_APP_MEM/ a\
    setprop ro.CONTENT_PROVIDER_MEM '$SL5 $rc
						fi
					fi
					sed -i '/on boot/ a\
    write /sys/module/lowmemorykiller/parameters/adj '$scadj $rc
					sed -i '/BEGIN OOM_ADJ_Settings/ a\
    setprop ro.FOREGROUND_APP_ADJ '$adj1'\
    setprop ro.VISIBLE_APP_ADJ '$adj2'\
    setprop ro.SECONDARY_SERVER_ADJ '$adj3'\
    setprop ro.BACKUP_APP_ADJ '$(($adj3+1))'\
    setprop ro.HOME_APP_ADJ '$adj2'\
    setprop ro.HIDDEN_APP_MIN_ADJ '$(($adj4-1))'\
    setprop ro.EMPTY_APP_ADJ '$adj6 $rc
					if [ "$gb" -eq 1 ]; then sed -i '/ro.VISIBLE_APP_ADJ/ a\
    setprop ro.PERCEPTIBLE_APP_ADJ '$(($adj1+2))'\
    setprop ro.HEAVY_WEIGHT_APP_ADJ '$(($adj2+1)) $rc
					else sed -i '/ro.HIDDEN_APP_MIN_ADJ/ a\
    setprop ro.CONTENT_PROVIDER_ADJ '$adj4 $rc
					fi
					if [ "$opt" -eq 13 ]; then sed -i 's/.* ro.HOME_APP_ADJ .*/    setprop ro.HOME_APP_ADJ '$adj1/ $rc
					elif [ "$opt" -eq 11 ]; then sed -i 's/.* ro.HOME_APP_ADJ .*/    setprop ro.HOME_APP_ADJ '$(($adj1+2))/ $rc
					else sed -i 's/.* ro.HOME_APP_ADJ .*/    setprop ro.HOME_APP_ADJ '$(($adj1+1))/ $rc
					fi
					if [ "`grep -s "on early-boot" $rc`" ]; then sed -i '/minfree/ a\
    write /proc/sys/vm/min_free_kbytes '$MFK $rc
					else sed -i '/END OOM_ADJ_Settings/ a\
    write /proc/sys/vm/min_free_kbytes '$MFK $rc
					fi
					sed -i '/vm\/min_free_kbytes/ a\
    write /proc/sys/vm/oom_kill_allocating_task 0\
    write /proc/sys/vm/panic_on_oom 0\
    write /proc/sys/vm/overcommit_memory 1\
    write /proc/sys/vm/swappiness 0\
    write /proc/sys/kernel/panic_on_oops 1\
    write /proc/sys/kernel/panic 30' $rc
					if [ "$rc" = "$initrcpath" ] && [ "$initrc" -eq 0 ]; then echo -n " /data/"
					elif [ "$rc" = "$initrcpath" ]; then echo -n " /"
					else echo -n " "
					fi
					if [ "$opt" -ge 11 ] && [ "$opt" -le 13 ] && [ "$ReSuperCharge" -eq 0 ]; then echo "${rc##*/} has been OOM Fixed!"
					else echo "${rc##*/} has been SuperCharged!"
					fi
				done
				echo $line
				echo ""
				$sleep
				echo " Fixing Out Of Memory (OOM) Groupings..."
				echo ""
				$sleep
				echo "                    ...Fixing OOM Priorities..."
				echo ""
				$sleep
				for p in $prop /system/etc/ram.conf; do
					if [ -f "$p" ]; then
						if [ "$scminfree" ]; then sed -i '/BEGIN OOM_MEM_Settings/ a\
ro.FOREGROUND_APP_MEM='$SL1'\
ro.VISIBLE_APP_MEM='$SL2'\
ro.SECONDARY_SERVER_MEM='$SL3'\
ro.BACKUP_APP_MEM='$SL4'\
ro.HOME_APP_MEM='$SL0'\
ro.HIDDEN_APP_MEM='$SL4'\
ro.EMPTY_APP_MEM='$SL6 $p
							if [ "$gb" -eq 1 ]; then sed -i '/ro.VISIBLE_APP_MEM/ a\
ro.PERCEPTIBLE_APP_MEM='$SL0'\
ro.HEAVY_WEIGHT_APP_MEM='$SL3 $p
							else sed -i '/ro.HIDDEN_APP_MEM/ a\
ro.CONTENT_PROVIDER_MEM='$SL5 $p
							fi
						fi
						sed -i '/BEGIN OOM_ADJ_Settings/ a\
ro.FOREGROUND_APP_ADJ='$adj1'\
ro.VISIBLE_APP_ADJ='$adj2'\
ro.SECONDARY_SERVER_ADJ='$adj3'\
ro.BACKUP_APP_ADJ='$(($adj3+1))'\
ro.HOME_APP_ADJ='$adj2'\
ro.HIDDEN_APP_MIN_ADJ='$(($adj4-1))'\
ro.EMPTY_APP_ADJ='$adj6 $p
						if [ "$gb" -eq 1 ]; then sed -i '/ro.VISIBLE_APP_ADJ/ a\
ro.PERCEPTIBLE_APP_ADJ='$(($adj1+2))'\
ro.HEAVY_WEIGHT_APP_ADJ='$(($adj2+1)) $p
						else sed -i '/ro.HIDDEN_APP_MIN_ADJ/ a\
ro.CONTENT_PROVIDER_ADJ='$adj4 $p
						fi
					fi
				done
				echo " ...OOM Groupings and Priorities are now fixed!"
				echo ""
				echo $line
				echo ""
				$sleep
				if [ "$opt" -eq 13 ]; then
					echo " Applying BulletProof Launcher..."
					echo ""
					$sleep
					sed -i 's/ro.HOME_APP_ADJ=.*/ro.HOME_APP_ADJ='$adj1/ $prop
					if [ -f "/system/etc/ram.conf" ]; then sed -i 's/ro.HOME_APP_ADJ=.*/ro.HOME_APP_ADJ='$adj1/ /system/etc/ram.conf; fi
					echo " Launcher is not only SuperCharged..."
					echo ""
					$sleep
					echo $line
					echo "                           ...It's BULLETPROOF!"
					if [ "$homeadj" -ne "$adj1" ]; then newlauncher=1; fi
				elif [ "$opt" -eq 11 ]; then
					echo " Applying Hard To Kill Launcher..."
					echo ""
					$sleep
					sed -i 's/ro.HOME_APP_ADJ=.*/ro.HOME_APP_ADJ='$(($adj1+2))/ $prop
					if [ -f "/system/etc/ram.conf" ]; then sed -i 's/ro.HOME_APP_ADJ=.*/ro.HOME_APP_ADJ='$(($adj1+2))/ /system/etc/ram.conf; fi
					echo "              ...Hard To Kill Launcher APPLIED!"
					echo ""
					if [ "$homeadj" -ne "$(($adj1+2))" ]; then newlauncher=1; fi
				else
					echo " Applying Die-Hard Launcher..."
					echo ""
					$sleep
					sed -i 's/ro.HOME_APP_ADJ=.*/ro.HOME_APP_ADJ='$(($adj1+1))/ $prop
					if [ -f "/system/etc/ram.conf" ]; then sed -i 's/ro.HOME_APP_ADJ=.*/ro.HOME_APP_ADJ='$(($adj1+1))/ /system/etc/ram.conf; fi
					echo "                  ...Die-Hard Launcher APPLIED!"
					echo ""
					if [ "$homeadj" -ne "$(($adj1+1))" ]; then newlauncher=1; fi
				fi
			fi
			echo $line
			echo ""
			$sleep
			echo " Applying Kernel/Memory Tweaks..."
			echo ""
			$sleep
			echo -n "               ";busybox sysctl -w vm.min_free_kbytes=$MFK
			echo -n "      ";busybox sysctl -w vm.oom_kill_allocating_task=0
			echo -n "                  ";busybox sysctl -w vm.panic_on_oom=0
			echo -n "             ";busybox sysctl -w vm.overcommit_memory=1
			echo -n "                    ";busybox sysctl -w vm.swappiness=0
			echo -n "             ";busybox sysctl -w kernel.panic_on_oops=1
			echo -n "                     ";busybox sysctl -w kernel.panic=30
			echo ""
			echo $line
			echo ""
			$sleep
			echo " Applying Miscellaneous Tweaks..."
			echo ""
			$sleep
			setprop persist.sys.purgeable_assets 1
			echo "     persist.sys.purgeable_assets = 1"
			setprop wifi.supplicant_scan_interval 180
			echo "    wifi.supplicant_scan_interval = 180"
			setprop windowsmgr.max_events_per_sec 200
			echo "    windowsmgr.max_events_per_sec = 200"
			setprop pm.sleep_mode 1
			echo "                    pm.sleep_mode = 1"
			echo ""
			echo $line
			echo ""
			$sleep
			cat > /data/99SuperCharger.sh <<EOF
#!/system/bin/sh
#
# V6 SuperCharger, OOM Grouping & Priority Fixes created by zeppelinrox.
#
# SuperMinFree Calculator & MFK Calculator (for min_free_kbytes) created by zeppelinrox also :)
#
# See http://goo.gl/krtf9 - Linux Memory Consumption - Nice article!
# See http://goo.gl/hFdNO - Memory and SuperCharging Overview ...or... "Why \`Free RAM\` Is NOT Wasted RAM!"
# See http://goo.gl/4w0ba - MFK Calculator Info - explanation for vm.min_free_kbytes.
  ################################
 #  SuperCharger Service Notes  #
################################
# To leave the SuperCharger Service running, insert a # at the beginning of the "stop super_service" entry near the bottom of this script (3rd or 4th last line).
#
# To stop the SuperCharger Service, run Terminal Emulator and type...
# "su" and Enter.
# "stop super_service" and Enter.
#
# To restart the SuperCharger Service so it stays running, type...
# "su" and Enter.
# "start super_service" and Enter.
#
# If the service is running and you type: "cat /proc/*/cmdline | grep Super".
# The output would be 2 lines:
# /data/99SuperCharger.sh
# Super
#
# If it's not running, the output would be the last item ie. "Super" which was generated by the typed command.
#
EOF
			if [ ! "$allrcpaths" ]; then
				cat >> /data/99SuperCharger.sh <<EOF
# Ummm... this service won't work on your current ROM. Sorry :P
# Instead use Script Manager to run this script if you're on a stock ROM OR settings don't stick via the SuperCharger boot script found in /system/etc/init.d!
#
EOF
			fi
			cat >> /data/99SuperCharger.sh <<EOF
# echo ""; echo " See /data/Log_SuperCharger.log for the output!"; echo "";
# For debugging, delete the # at the beginning of the following 2 lines, and check the end of /data/Log_SuperCharger.log file to see what may have fubarred.
# set -x;
# exec > /data/Log_SuperCharger.log 2>&1;
#
line=================================================;
echo "";
echo \$line;
echo "   The -=V6 SuperCharger=- by -=zeppelinrox=-";
echo \$line;
echo "";
if [ ! "\`id | grep =0\`" ]; then
	echo " You are NOT running this script as root...";
	echo "";
	echo \$line;
	echo "                      ...No SuperUser for you!!";
	echo \$line;
	echo "";
	echo "     ...Please Run as Root and try again...";
	echo "";
	echo \$line;
	echo "";
	exit 69;
fi;
echo " To verify application of settings...";
echo "";
echo "           ...check out /data/Ran_SuperCharger!";
echo "";
echo \$line;
echo "";
if [ "\`pgrep -l android\`" ]; then terminate=yes;
	if [ ! "\`pgrep -l scriptmanager\`" ]; then
		echo " Waiting 90 seconds (avoid conflicts)... then...";echo "";
		echo " Gonna SuperCharge this Android... zoOM... zOOM!";
		echo "";
		echo \$line;
		echo "";
		sleep 90;
	fi;
fi;
EOF
			if [ -f "/system/etc/hw_config.sh" ]; then
				echo " SuperCharging /system/etc/hw_config.sh!"
				echo ""
				echo $line
				echo ""
				$sleep
				cat >> /system/etc/hw_config.sh <<EOF
# V6 SuperCharger, OOM Grouping & Priority Fixes created by zeppelinrox.
#
# SuperMinFree Calculator & MFK Calculator (for min_free_kbytes) created by zeppelinrox also :)
#
# See http://goo.gl/krtf9 - Linux Memory Consumption - Nice article!
# See http://goo.gl/hFdNO - Memory and SuperCharging Overview ...or... "Why \`Free RAM\` Is NOT Wasted RAM!"
# See http://goo.gl/4w0ba - MFK Calculator Info - explanation for vm.min_free_kbytes.
#
# DO NOT DELETE COMMENTS. DELETING COMMENTS WILL BREAK UNINSTALL ROUTINE!
#
line=================================================;
echo "";
echo \$line;
echo "";
EOF
			fi
			for sc in /data/99SuperCharger.sh /system/etc/hw_config.sh; do
				if [ -f "$sc" ]; then
					if [ "$initrc" -eq 1 ]; then
						cat >> $sc <<EOF
if [ -f "$initrcpath" ] && [ "\`grep "SuperCharger" /system/build.prop\`" ] && [ "\`diff /data/init.rc /init.rc\`" ]; then
	busybox mount -o remount,rw / 2>/dev/null;
	busybox mount -o remount,rw rootfs 2>/dev/null;
	cp -fr $initrcpath $initrcpath1;
	busybox mount -o remount,ro / 2>/dev/null;
	busybox mount -o remount,ro rootfs 2>/dev/null;
fi;
EOF
					fi
					cat >> $sc <<EOF
busybox mount -o remount,rw /system 2>/dev/null;
busybox mount -o remount,rw \`busybox mount | grep system | awk '{print \$1,\$3}' | sed -n 1p\` 2>/dev/null;
if [ "\`cat /proc/sys/vm/min_free_kbytes\`" -ne $MFK ] || [ "\`cat /proc/sys/net/core/rmem_max\`" -ne 1048576 ]; then
	  ####################################
	 #  Kernel & Virtual Memory Tweaks  #
	####################################
	busybox sysctl -w vm.min_free_kbytes=$MFK;
	busybox sysctl -w vm.oom_kill_allocating_task=0;
	busybox sysctl -w vm.panic_on_oom=0;
	busybox sysctl -w vm.overcommit_memory=1;
	busybox sysctl -w vm.swappiness=0;
	busybox sysctl -w kernel.panic_on_oops=1;
	busybox sysctl -w kernel.panic=30;
	  #################################
	 #  3G TurboCharger Enhancment!  #
	#################################
	busybox sysctl -w net.core.wmem_max=1048576;
	busybox sysctl -w net.core.rmem_max=1048576;
	busybox sysctl -w net.core.optmem_max=20480;
	busybox sysctl -w net.ipv4.tcp_moderate_rcvbuf=1;        # Be sure that autotuning is in effect
	busybox sysctl -w net.ipv4.route.flush=1;
	busybox sysctl -w net.ipv4.udp_rmem_min=6144;
	busybox sysctl -w net.ipv4.udp_wmem_min=6144;
	busybox sysctl -w net.ipv4.tcp_rmem='6144 87380 1048576';
	busybox sysctl -w net.ipv4.tcp_wmem='6144 87380 1048576';
	echo "";
	echo \$line;
	echo "";
fi;
currentadj=\`cat /sys/module/lowmemorykiller/parameters/adj\`;
currentminfree=\`cat /sys/module/lowmemorykiller/parameters/minfree\`;
scadj=\`cat /data/V6_SuperCharger/SuperChargerAdj\`;
scminfree=\`cat /data/V6_SuperCharger/SuperChargerMinfree\`;
if [ "\$scminfree" ] && [ "\$currentminfree" != "\$scminfree" ] || [ "\$currentadj" != "\$scadj" ]; then
	chmod 777 /sys/module/lowmemorykiller/parameters/adj;
	chmod 777 /sys/module/lowmemorykiller/parameters/minfree;
	echo \$scadj > /sys/module/lowmemorykiller/parameters/adj;
	if [ "\$scminfree" ]; then echo \$scminfree > /sys/module/lowmemorykiller/parameters/minfree; fi;
EOF
					if [ "$buildprop" -eq 0 ]; then
						echo "	sed -i '/.*_ADJ/d' /system/build.prop;" >> $sc
						echo "	sed -i '/.*_MEM/d' /system/build.prop;" >> $sc
						if [ -f "/system/bin/build.prop" ]; then
							echo "	sed -i '/.*_ADJ/d' /system/bin/build.prop;" >> $sc
							echo "	sed -i '/.*_MEM/d' /system/bin/build.prop;" >> $sc
						fi
					fi
					cat >> $sc <<EOF
	echo " \$( date +"%m-%d-%Y %H:%M:%S" ): Applied Settings from \$0!" >> /data/Ran_SuperCharger;
	echo "         SuperCharger Settings Applied!";
elif [ "\`pgrep -l android\`" ]; then
	echo " \$( date +"%m-%d-%Y %H:%M:%S" ): DIDN'T Apply Settings from \$0!" >> /data/Ran_SuperCharger;
	echo "  SuperCharger Settings Were ALREADY Applied!";
fi 2>/dev/null;
echo "";
echo " \$0 Executed...";
EOF
				fi
			done
			cat >> /data/99SuperCharger.sh <<EOF
echo "";
echo "          ===========================";
echo "           ) SuperCharge Complete! (";
echo "          ===========================";
if [ "\$terminate" ]; then
	busybox mount -o remount,ro / 2>/dev/null;
	busybox mount -o remount,ro rootfs 2>/dev/null;
	busybox mount -o remount,ro /system 2>/dev/null;
	busybox mount -o remount,ro \`busybox mount | grep system | awk '{print \$1,\$3}' | sed -n 1p\` 2>/dev/null;
	stop super_service;
else \$0 & exit 0;
fi;
# End of V6 SuperCharged Entries.
EOF
			if [ -f "/system/etc/hw_config.sh" ]; then
				cat >> /system/etc/hw_config.sh <<EOF
#busybox mount -o remount,ro /system 2>/dev/null;
#busybox mount -o remount,ro \`busybox mount | grep system | awk '{print \$1,\$3}' | sed -n 1p\` 2>/dev/null;
sh /data/99SuperCharger.sh;
# End of V6 SuperCharged Entries.
EOF
			fi
			chown 0.0 /data/99SuperCharger.sh; chmod 777 /data/99SuperCharger.sh
			if [ "$rcpaths" ] || [ "$initrc" -eq 1 ] && [ -d "/system/etc/init.d" ] && [ "$ics" -eq 0 ]; then
				echo " For Super Stickiness..."
				echo ""
				$sleep
			fi
			if [ -d "/system/etc/init.d" ]; then
				echo " SuperCharging /system/etc/init.d folder..."
				echo ""
				$sleep
				cp -fr /data/99SuperCharger.sh /system/etc/init.d/99SuperCharger
				if [ -f "/system/etc/init.d/99SuperCharger" ]; then
					sed -i 's/# echo/echo/' /system/etc/init.d/99SuperCharger
					sed -i 's/# set -x/set -x/' /system/etc/init.d/99SuperCharger
					sed -i 's/# exec/exec/' /system/etc/init.d/99SuperCharger
					sed -i '/busybox mount -o remount,rw `busybox mount/ a\
mv \$0 /system/etc/init.d/99SuperCharger;\
for i in \`busybox ls -r /system/etc/init.d\`; do\
	if [ "\$j" ]; then break; fi;\
	if [ ! -d "\$i" ]; then j=\$i; fi;\
done;\
echo " SuperCharger to run on boot as...";\
echo "";\
echo \$line;\
if [ "\$j" = "S98KickAssKernel" ] || [ "\$j" = "SS98KickAssKernel" ] && [ "\$i" \\\< "99SuperCharger1" ] || [ "\$j" \\\< "99SuperCharger1" ]; then\
	echo "          .../system/etc/init.d/99SuperCharger!";\
elif [ "\$j" = "SS98KickAssKernel" ] && [ "\$i" \\\< "S99SuperCharger" ] || [ "\$j" \\\< "S99SuperCharger" ]; then\
	mv /system/etc/init.d/99SuperCharger /system/etc/init.d/S99SuperCharger;\
	echo "         .../system/etc/init.d/S99SuperCharger!";\
else\
	mv /system/etc/init.d/99SuperCharger /system/etc/init.d/SS99SuperCharger;\
	echo "        .../system/etc/init.d/SS99SuperCharger!";\
fi;\
echo \$line;\
echo "";' /system/etc/init.d/99SuperCharger
					sed -i '/V6 SuperCharged/ i\
sh /system/etc/init.d/*KickAssKernel 2>/dev/null;\
sh /data/99SuperCharger.sh;' /system/etc/init.d/99SuperCharger
					sed -i '/super_service;/s/^/#/' /system/etc/init.d/99SuperCharger
					j=
					for i in `busybox ls -r /system/etc/init.d`; do
						if [ "$j" ]; then break; fi
						if [ ! -d "$i" ]; then j=$i; fi
					done
					if [ "$j" = "S98KickAssKernel" ] || [ "$j" = "SS98KickAssKernel" ] && [ "$i" \< "99SuperCharger1" ] || [ "$j" \< "99SuperCharger1" ]; then
						echo "     ...with /system/etc/init.d/99SuperCharger!"
					elif [ "$j" = "SS98KickAssKernel" ] && [ "$i" \< "S99SuperCharger" ] || [ "$j" \< "S99SuperCharger" ]; then
						mv /system/etc/init.d/99SuperCharger /system/etc/init.d/S99SuperCharger
						echo "    ...with /system/etc/init.d/S99SuperCharger!"
					else
						mv /system/etc/init.d/99SuperCharger /system/etc/init.d/SS99SuperCharger
						echo "   ...with /system/etc/init.d/SS99SuperCharger!"
					fi
				else
					echo " WARNING: ERROR copying file to /system/init.d!"
					echo ""
					$sleep
					echo " Got enough free space?"
					echo ""
					$sleep
					echo " System Partition has `df -h system | awk '{print $4,"Free ("$5}' | sed -n 2p` Used)"
				fi
				echo ""
			else
				echo " You have no /system/etc/init.d folder..."
				echo ""
				$sleep
				echo $line
				echo "    ...so instead, use /data/99SuperCharger.sh!"
				echo $line
				echo ""
				$sleep
				echo " Stock ROM? - Additional Configuration Required!"
				echo ""
				$sleep
				if [ "$opt" -le 10 ]; then
					echo -n " Some Changes are TEMPORARY & "
					if [ "$allrcpaths" ]; then echo "MAY NOT PERSIST!"
					else echo "WON'T PERSIST!"
					fi
					echo ""
					$sleep
					echo " To enable PERSISTENT SuperCharger settings...."
					echo ""
					$sleep
					echo "                   ...and OOM Grouping Fixes..."
				else echo " To enable PERSISTENT OOM Grouping Fixes..."
				fi
				echo ""
				$sleep
				if [ "$allrcpaths" ]; then
					echo "       ...The SuperCharger Service should work!"
					echo ""
					$sleep
					echo " BUT if it doesn't..."
					echo ""
					$sleep
				fi
				if [ "$smrun" ]; then
					SuperChargerScriptManagerHelp=1
					cat > /sdcard/SuperChargerScriptManagerHelp.html <<EOF
Yay! You already have <a href="http://market.android.com/details?id=os.tools.scriptmanager">Script Manager!</a><br>
After running the script, have Script Manager load the newly created <b>/data/99SuperCharger.sh</b> on boot<br>
In the "Config" settings, enable "Browse as Root."<br>
Press the menu key and then Browser.<br>
Navigate up to the root, then click on the "data" folder.<br>
Click on 99SuperCharger.sh and select "Script" from the "Open As" menu.<br>
In the properties dialogue box, check "Run as root" (ie. SuperUser) and "Run at boot" and "Save".<br>
And that's it!<br>
Script Manager will load your most recent settings on boot!<br>
<br>
Another option is to make a Script Manager widget for <b>/data/99SuperCharger.sh</b> on your homescreen and simply launch it after each reboot.<br>
<br>
If you run the script later and with different settings, you don't have to reconfigure anything.<br>
Script Manager will just load the new /data/99SuperCharger.sh on boot automagically :)<br>
<br>
For more SuperCharging help and info,<br>
See the <a href="http://goo.gl/qM6yR">V6 SuperCharger Thread</a><br>
Feedback is Welcome!<br>
<br>
-=zeppelinrox=- @ <a href="http://goo.gl/qM6yR">XDA</a> & <a href="http://www.droidforums.net/forum/droid-hacks/148268-script-v6-supercharger-htk-bulletproof-launchers-fix-memory-all-androids.html">Droid</a> Forums<br>
EOF
					echo "Use THIS app to load 99SuperCharger.sh on boot!"
					echo ""
					$sleep
					echo $line
					echo " See /sdcard/SuperChargerScriptManagerHelp.html"
				else
					echo " ..Please ENABLE boot scripts to be run from..."
					echo "                  .../system/etc/init.d folder!"
					echo " Easier: Script Manager can solve everything ;)"
					echo ""
					$sleep
					SuperChargerHelp=1
					cat > /sdcard/SuperChargerHelp.html <<EOF
To enable init.d boot scripts, go <a href="http://goo.gl/rZTyW">HERE</a><br>
This is for Motorolas! At least some of them anyway.<br>
If that page is incompatible with your phone, do some reasearch!<br>
<br>
A very nice and easy solution is to simply use<br>
Script Manager to load scripts on boot - on ANY ROM!<br>
Here is the <a href="http://market.android.com/details?id=os.tools.scriptmanager">Market Link</a><br>
So first, you use Script Manager to run the V6 SuperCharger script.<br>
Then use it again to load the newly created <b>/data/99SuperCharger.sh</b> on boot<br>
In the 99SuperCharger.sh properties dialogue box, check "Run as root" (ie. SuperUser) and "Run at boot" and "Save".<br>
And that's it!<br>
Script Manager will load your most recent settings on boot!<br>
<br>
Another option is to make a Script Manager widget for <b>/data/99SuperCharger.sh</b> on your homescreen and simply launch it after each reboot.<br>
<br>
If you run the script later and with different settings, you don't have to reconfigure anything.<br>
Script Manager will just load the new /data/99SuperCharger.sh on boot automagically :)<br>
<br>
For more SuperCharging help and info,<br>
See the <a href="http://goo.gl/qM6yR">V6 SuperCharger Thread</a><br>
Feedback is Welcome!<br>
<br>
-=zeppelinrox=- @ <a href="http://goo.gl/qM6yR">XDA</a> & <a href="http://www.droidforums.net/forum/droid-hacks/148268-script-v6-supercharger-htk-bulletproof-launchers-fix-memory-all-androids.html">Droid</a> Forums<br>
EOF
					echo $line
					echo "See /sdcard/SuperChargerHelp.html for more help!"
				fi
			fi
			echo $line
			if [ "$opt" -le 10 ] || [ "$ReSuperCharge" -eq 1 ]; then
				if [ "$allrcpaths" ] && [ ! "$scsinfo" ]; then
					if [ -d "/system/etc/init.d" ]; then
						echo ""
						$sleep
						echo " And for added, Auto Insurance..."
						echo ""
						$sleep
						echo $line
					fi
					if [ ! "$scservice" ]; then echo " Installing SuperCharger Service to..."
					else echo " SuperCharger Service had been installed to..."
					fi
					echo $line
					echo ""
					$sleep
				fi
				for scs in $initrcpath $allrcpaths; do
					cat >> $scs <<EOF
# SuperCharger_Service created by zeppelinrox.
#
# DO NOT DELETE COMMENTS. DELETING COMMENTS WILL BREAK UNINSTALL ROUTINE!
  ################################
 #  SuperCharger Service Notes  #
################################
# To restart the SuperCharger Service so it stays running, run Terminal Emulator and type...
# "su" and Enter.
# "start super_service" and Enter.
#
# To stop the SuperCharger Service, type...
# "su" and Enter.
# "stop super_service" and Enter.
#
# If the service is running and you type: "cat /proc/*/cmdline | grep Super".
# The output would be 2 lines:
# /data/99SuperCharger.sh
# Super
#
# If it's not running, the output would be the last item ie. "Super" which was generated by the typed command.
#
service super_service /system/bin/sh /data/99SuperCharger.sh
    class post-zygote_services
    user root
    group root
#
# End of SuperCharged_Service Entries.
EOF
					if [ "$scs" != "$initrcpath" ] && [ ! "$scsinfo" ]; then echo "   ...$scs!"; fi
					if [ ! "$scservice" ]; then $sleep; fi
				done
				if [ "$allrcpaths" ] && [ ! "$scsinfo" ]; then
					echo ""
					echo $line
					echo ""
					$sleep
					echo " You can leave the service on by either..."
					echo ""
					$sleep
					echo " ...reading comments in /data/99SuperCharger.sh"
					echo ""
					$sleep
					echo " Or... Run Terminal Emulator..."
					echo ""
					$sleep
					echo "              ...type \"su\" and Enter..."
					echo "                       =="
					echo ""
					$sleep
					echo "       ...type \"start super_service\" and Enter."
					echo "                ==================="
					echo ""
					$sleep
					echo " To stop the SuperCharger Service..."
					echo ""
					$sleep
					echo "        ...type \"stop super_service\" and Enter."
					echo "                 =================="
					echo ""
					echo $line
					echo ""
					$sleep
					echo "If the service is running and you type..."
					echo ""
					$sleep
					echo "          ...\"cat /proc/*/cmdline | grep Super\""
					echo ""
					$sleep
					echo " The output would look like this:"
					echo ""
					$sleep
					echo "      /data/99SuperCharger.sh"
					echo "      Super"
					echo ""
					echo $line
				elif [ ! "$allrcpaths" ] && [ ! "$scsinfo" ]; then
					echo $line
					echo "    SuperCharger Service Entries Installed!"
					echo $line
					echo ""
					$sleep
					echo " This won't work on this ROM but... "
					echo ""
					$sleep
					echo "       ...it makes for easy cooking into a ROM!"
					echo ""
					$sleep
					echo " Just read the comments in $initrcpath :-)"
					echo ""
					echo $line
				fi
				echo ""
				$sleep
				echo " Setting LowMemoryKiller to..."
				echo ""
				$sleep
				echo "         ...$MB1, $MB2, $MB3, $MB4, $MB5, $MB6 MB"
				echo ""
				$sleep
				echo "$scminfree" > /sys/module/lowmemorykiller/parameters/minfree
				currentminfree=`cat /sys/module/lowmemorykiller/parameters/minfree`
				echo " OOM Minfree levels are now set to..."
				echo ""
				$sleep
				echo "         ..."$currentminfree
				echo ""
				if [ "$newlauncher" -eq 0 ]; then
					$sleep
					echo $line
					echo "      SUPERCHARGE IN EFFECT IMMEDIATELY!!"
				fi
				echo $line
				echo ""
				$sleep
				cat > "/data/V6_SuperCharger/PowerShift_Scripts/$CONFIG-($MB1,$MB2,$MB3,$MB4,$MB5,$MB6)-PowerShift.sh" <<EOF
#!/system/bin/sh
#
# PowerShift Script for use with The V6 SuperCharger created by zeppelinrox.
# SuperMinFree Calculator & MFK Calculator (for min_free_kbytes) created by zeppelinrox also :)
#
# See http://goo.gl/krtf9 - Linux Memory Consumption - Nice article!
# See http://goo.gl/hFdNO - Memory and SuperCharging Overview ...or... "Why \`Free RAM\` Is NOT Wasted RAM!"
# See http://goo.gl/4w0ba - MFK Calculator Info - explanation for vm.min_free_kbytes.
#
line=================================================
clear
echo ""
echo \$line
echo "   The -=V6 SuperCharger=- by -=zeppelinrox=-"
echo \$line
echo ""
sleep 1
if [ ! "\`id | grep =0\`" ]; then
	sleep 2
	echo " You are NOT running this script as root..."
	echo ""
	sleep 3
	echo \$line
	echo "                      ...No SuperUser for you!!"
	echo \$line
	echo ""
	sleep 3
	echo "     ...Please Run as Root and try again..."
	echo ""
	echo \$line
	echo ""
	sleep 3
	exit 69
fi
busybox mount -o remount,rw /system 2>/dev/null
busybox mount -o remount,rw \`busybox mount | grep system | awk '{print \$1,\$3}' | sed -n 1p\` 2>/dev/null
echo " PowerShifting to a different gear!"
echo ""
sleep 1
echo \$line
awk -F , '{print "   Current Minfrees = "\$1/256","\$2/256","\$3/256","\$4/256","\$5/256","\$6/256" MB"}' /sys/module/lowmemorykiller/parameters/minfree
echo \$line
echo ""
sleep 1
echo " Setting LowMemoryKiller to..."
echo ""
sleep 1
echo \$line
echo "         ...$MB1, $MB2, $MB3, $MB4, $MB5, $MB6 MB"
echo \$line
echo ""
sleep 1
echo $scminfree > /sys/module/lowmemorykiller/parameters/minfree
echo $scminfree > /data/V6_SuperCharger/SuperChargerMinfree
echo " OOM Minfree levels are now set to..."
echo ""
sleep 1
echo "         ...\`cat /sys/module/lowmemorykiller/parameters/minfree\`"
echo ""
sleep 1
echo "  They are also your new SuperCharger values!"
echo ""
echo \$line
echo ""
sleep 1
echo " Applying Updated Kernel/Memory Tweaks..."
echo ""
sleep 1
echo -n "         ...";busybox sysctl -w vm.min_free_kbytes=$MFK
echo ""
sleep 1
echo \$line
echo " Updating MFK in *99SuperCharger Boot Scripts..."
echo \$line
echo ""
sleep 1
sed -i 's/vm.min_free_kbytes=.*/vm.min_free_kbytes=$MFK;/' /system/etc/init.d/*SuperCharger* 2>/dev/null
sed -i 's/free_kbytes\`" -ne .* ] /free_kbytes\`" -ne $MFK ] /' /system/etc/init.d/*SuperCharger* 2>/dev/null
sed -i 's/vm.min_free_kbytes=.*/vm.min_free_kbytes=$MFK;/' /data/99SuperCharger.sh 2>/dev/null
sed -i 's/free_kbytes\`" -ne .* ] /free_kbytes\`" -ne $MFK ] /' /data/99SuperCharger.sh 2>/dev/null
echo "          ==========================="
echo "           ) PowerShift Completed! ("
echo "          ==========================="
echo ""
busybox mount -o remount,ro /system 2>/dev/null
busybox mount -o remount,ro \`busybox mount | grep system | awk '{print \$1,\$3}' | sed -n 1p\` 2>/dev/null
exit 0
EOF
				chown 0.0 /data/V6_SuperCharger/PowerShift_Scripts/*; chmod 777 /data/V6_SuperCharger/PowerShift_Scripts/*
				echo " A PowerShift Script was saved as..."
				echo ""
				$sleep
				echo " $CONFIG-($MB1,$MB2,$MB3,$MB4,$MB5,$MB6)-PowerShift.sh!"
				echo ""
				$sleep
				echo " Goto data/V6_SuperCharger/PowerShift_Scripts.."
				echo ""
				$sleep
				echo "             ...make a \"Quick Widget\" for it..."
				echo ""
				$sleep
				echo "            ...and PowerShift between settings!"
				echo ""
				$sleep
				echo "  They'll also be your new SuperCharger values!"
				echo ""
				echo $line
				scsinfo=shown
			fi
			echo "$scadj" > /sys/module/lowmemorykiller/parameters/adj
			if [ "$newlauncher" -eq 1 ]; then
				$sleep
				echo "           LAUNCHER CHANGE DETECTED!"
				echo $line
				echo ""
				$sleep
				echo " REBOOT NOW TO ENABLE..."
				echo ""
				$sleep
				if [ "$opt" -eq 13 ]; then echo "          ...BULLETPROOF LAUNCHER..."
				elif [ "$opt" -eq 11 ]; then echo "         ...HARD TO KILL LAUNCHER..."
				else echo "             ...DIE-HARD LAUNCHER..."
				fi
				echo ""
				$sleep
				echo "                     ...AND OOM GROUPING FIXES!"
				echo ""
				$sleep
				echo $line
			fi
			if [ "$SuperChargerHelp" -eq 1 ]; then
				$sleep
				echo " RUN /data/99SuperCharger.sh AFTER EACH REBOOT!"
			elif [ "$SuperChargerScriptManagerHelp" -eq 1 ]; then
				$sleep
				echo " DON'T FORGET to have Script Manager load..."
				echo "            .../data/99SuperCharger.sh on boot!"
				echo "     ...or make a Script Manager WIDGET for it!"
			elif [ "$opt" -le 10 ] || [ "$ReSuperCharge" -eq 1 ]; then
				$sleep
				if [ "$ReSuperCharge" -eq 1 ]; then echo -n " Restored"
				else echo -n "$CONFIG"
				fi
				echo " Settings WILL PERSIST after reboot!"
				echo $line
				echo ""
				$sleep
				echo "   If they don't persist, check the help file!"
				echo ""
			else
				echo ""
				$sleep
				echo " If OOM Fixes are't in effect after a reboot..."
				echo ""
				$sleep
				echo "                 ...and the Launcher is weak..."
				echo ""
				$sleep
				echo "                        ...check the help file!"
				echo ""
			fi
			echo $line
			echo ""
			$sleep
			echo " SO... you know if it works or not by..."
			echo ""
			$sleep
			echo " ...READING THE INFO Under The Driver's Console!"
			echo ""
			$sleep
			echo $line
			echo " PLEASE: READ THE ABOVE MESSAGES BEFORE ASKING!"
			echo $line
			echo ""
			$sleep
			echo "   Because I may SNAP and call you names! ;-]"
			echo ""
			echo $line
			echo ""
			$sleep
			echo -n " Press The Enter Key..."
			read enter
			echo ""
		fi
		if [ -f "/system/bin/build.prop" ]; then cp -fr /system/build.prop /system/bin; fi
		if [ "$opt" -ne 15 ]; then
			if [ "$opt" -le 10 ] || [ "$ReSuperCharge" -eq 1 ]; then ran=1; fi
			if [ -f "$initrcpath" ] && [ "$initrc" -eq 1 ]; then cp -fr $initrcpath $initrcpath1; fi
			if [ "$ReSuperCharge" -eq 0 ]; then
				echo " Backing up Re-SuperCharger files to SD Card... "
				echo ""
				$sleep
				if [ -d "/data/V6_SuperCharger" ]; then cp -fr /data/V6_SuperCharger /sdcard; fi
				for rc in $initrcpath $allrcpaths; do cp -fr $rc* /sdcard/V6_SuperCharger; done
				if [ -f "/data/99SuperCharger.sh" ]; then cp -fr /data/99SuperCharger.sh /sdcard/V6_SuperCharger; fi
				if [ "`ls /system/etc/init.d/*SuperCharger*`" ]; then cp -fr /system/etc/init.d/*SuperCharger* /sdcard/V6_SuperCharger; fi 2>/dev/null
				if [ "`ls /system/build.prop*`" ]; then cp -fr /system/build.prop* /sdcard/V6_SuperCharger; fi 2>/dev/null
				if [ "`ls /data/local.prop*`" ]; then cp -fr /data/local.prop* /sdcard/V6_SuperCharger; fi 2>/dev/null
				if [ "`ls /system/etc/hw_config.sh*`" ]; then cp -fr /system/etc/hw_config.sh* /sdcard/V6_SuperCharger; fi 2>/dev/null
				if [ "`ls /system/etc/ram.conf*`" ]; then cp -fr /system/etc/ram.conf* /sdcard/V6_SuperCharger; fi 2>/dev/null
				echo "            ...Re-SuperCharger backup complete!"
				echo ""
			fi
			if [ "$allrcpaths" ] && [ "$ReSuperCharge" -eq 1 ]; then opt=17
			elif [ "$newsupercharger" ]; then
				echo $line
				echo ""
				$sleep
				if [ "$ReSuperCharge" -eq 1 ]; then echo " Re-SuperCharger is ALMOST done!"
				else echo " I think this is your FIRST SuperCharge..."
				fi
				echo ""
				$sleep
				echo " Select YES in the next step to..."
				echo ""
				$sleep
				echo $line
				echo "                       ...SuperClean & ReStart!"
				echo $line
				echo ""
				$sleep
				opt=29
				echo -n " Press The Enter Key..."
				read enter
				echo ""
			fi
		fi
	fi
	if [ "$opt" -eq 16 ] || [ "$opt" -eq 17 ]; then
		if [ "$opt" -eq 16 ]; then
			echo "             Does Your OOM Stick?!"
			echo "             ====================="
			echo ""
			$sleep
			echo " Find Out Here... AND Find Your Home Launcher!!"
			echo ""
			$sleep
			echo " Choose from 2 bitchin' OOM Sticks..."
			echo ""
			$sleep
			echo " zOOM Stick (Quick) Mode is faster..."
			echo ""
			$sleep
			echo " ...it shows App Name, Priority (ADJ), and PID!"
			echo ""
			$sleep
			echo " vrOOM Stick (Verbose) mode is slower..."
			echo ""
			$sleep
			echo "         ...but adds Path and/or Apk file info!"
			echo ""
			echo $line
			echo ""
			$sleep
			echo " Do you want vrOOM or zOOM Stick mode?"
			echo ""
			$sleep
			echo -n " Enter V for vrOOM, any key for zOOM: "
			read chooseverifier
			echo ""
			echo $line
			case $chooseverifier in
			  v|V)echo " vrOOM Stick Verifier (Verbose) selected..."
				  vroomverifier=1;;
				*)echo " zOOM Stick Verifier (Quick) selected..."
				  vroomverifier=0;;
			esac
			echo $line
			echo ""
			$sleep
		fi
		if [ "$opt" -eq 17 ] && [ ! "$resuperable" ] && [ "$ReSuperCharge" -eq 0 ] && [ "$bpapplist" ]; then
			echo " Un-BulletProof Previously BulletProofed Apps?"
			echo ""
			$sleep
			echo " \"One-Shot\" scripts will remain untouched..."
			echo ""
			$sleep
			echo -n " Enter Y for Yes, any key for No: "
			read unbp
			echo ""
			echo $line
			case $unbp in
			  y|Y)echo "             Un-BulletProofed Apps!"
				  rm /data/V6_SuperCharger/BulletProof_Apps_HitList
				  if [ -f "/data/V6_SuperCharger/BulletProof_Apps_Fine_Tuner.sh" ]; then rm /data/V6_SuperCharger/BulletProof_Apps_Fine_Tuner.sh; fi
				  if [ "`ls /data/*BulletProof_Apps*`" ]; then rm /data/*BulletProof_Apps*; fi 2>/dev/null
				  if [ "`ls /system/etc/init.d/*BulletProof_Apps*`" ]; then rm /system/etc/init.d/*BulletProof_Apps*; fi 2>/dev/null;;
				*)echo "                   Cool Beans!";;
			esac
			echo $line
			echo ""
			$sleep
		fi
		if [ "$opt" -eq 17 ] && [ ! "$resuperable" ] && [ "$ReSuperCharge" -eq 0 ]; then
			echo " This will attempt to lock an app in memory!"
			echo ""
			$sleep
			echo " Do you want to view the current process list?"
			echo ""
			$sleep
			echo -n " Enter N for No, any key for Yes: "
			read bp
			echo ""
			echo $line
			case $bp in
			  n|N)echo " Okay... maybe next time!";;
				*)echo " Loading zOOM Stick Verifier (Quick)..."
				  vroomverifier=0
				  echo $line
				  echo ""
				  $sleep;;
			esac
		fi
		if [ "$vroomverifier" ]; then
			echo "        ================================="
			echo "----=== zeppelinrox's bOOM Stick Verifier ===---"
			echo "        ================================="
			echo ""
			$sleep
			oomadj1=`awk -F , '{print $1}' /sys/module/lowmemorykiller/parameters/adj`;oomadj2=`awk -F , '{print $2}' /sys/module/lowmemorykiller/parameters/adj`;oomadj3=`awk -F , '{print $3}' /sys/module/lowmemorykiller/parameters/adj`;oomadj4=`awk -F , '{print $4}' /sys/module/lowmemorykiller/parameters/adj`;oomadj5=`awk -F , '{print $5}' /sys/module/lowmemorykiller/parameters/adj`;oomadj6=`awk -F , '{print $6}' /sys/module/lowmemorykiller/parameters/adj`
		fi
		if [ "$vroomverifier" ] && [ "$vroomverifier" -eq 0 ]; then
			echo " Using zOOM Stick Mode (Quick)..."
			echo ""
			$sleep
			echo " ADJ  PID    Process"
			echo ""
			echo " FOREGROUND_APP OOM GROUPING"
			echo " ==========================="
			if [ "$HL" -gt -18 ] && [ "$HL" -lt "$oomadj1" ]; then
				echo ""
				echo " HOME LAUNCHER IS IN HERE! (ADJ=$HL)"
				echo ""
			fi
			for i in `ls /proc`; do
			  if [ "$i" -ne 0 ] 2>/dev/null && [ -f "/proc/$i/oom_adj" ] && [ "`cat /proc/$i/cmdline`" ] && [ "`cat /proc/$i/oom_adj`" -gt -18 ] && [ "`cat /proc/$i/oom_adj`" -lt "$oomadj1" ]; then
				echo -n " `cat /proc/$i/oom_adj`  "
				echo -n $i
				echo "  `cat /proc/$i/cmdline | sed 's/.*\///'`"
			  fi
			done
			if [ "$HL" -eq "$oomadj1" ]; then
				echo ""
				echo " HOME LAUNCHER IS IN HERE! (ADJ=$HL)"
				echo ""
			fi
			for i in `ls /proc`; do
			  if [ "$i" -ne 0 ] 2>/dev/null && [ -f "/proc/$i/oom_adj" ] && [ "`cat /proc/$i/cmdline`" ] && [ "`cat /proc/$i/oom_adj`" -eq "$oomadj1" ]; then
				echo -n " `cat /proc/$i/oom_adj`  "
				echo -n $i
				echo "  `cat /proc/$i/cmdline | sed 's/.*\///'`"
			  fi
			done
			echo ""
			if [ "$scadj" ] && [ "$currentadj" = "$scadj" ] && [ "$oomstick" -eq 1 ] && [ "$HL" -ne "$oomadj1" ]; then
				echo " HOME_LAUNCHER OOM GROUPING!"
				echo " ==========================="
				for i in `ls /proc`; do
				  if [ "$i" -ne 0 ] 2>/dev/null && [ -f "/proc/$i/oom_adj" ] && [ "`cat /proc/$i/cmdline`" ] && [ "`cat /proc/$i/oom_adj`" -gt "$oomadj1" ] && [ "`cat /proc/$i/oom_adj`" -lt "$oomadj2" ]; then
					if [ "`cat /proc/$i/oom_adj`" -eq "$HL" ] && [ "$HL" -ne "$PA" ] && [ "$lname" = "`cat /proc/$i/cmdline`" ]; then
						echo ""
						echo " Home Launcher is on the NEXT line! (ADJ=$HL)"
					fi
					echo -n " `cat /proc/$i/oom_adj`  "
					echo -n $i
					echo "  `cat /proc/$i/cmdline | sed 's/.*\///'`"
					if [ "`cat /proc/$i/oom_adj`" -eq "$HL" ]; then echo ""; fi
				  fi
				done
				echo ""
				echo " VISIBLE_APP OOM GROUPING"
				echo " ========================"
				for i in `ls /proc`; do
				  if [ "$i" -ne 0 ] 2>/dev/null && [ -f "/proc/$i/oom_adj" ] && [ "`cat /proc/$i/cmdline`" ] && [ "`cat /proc/$i/oom_adj`" -eq "$oomadj2" ]; then
					echo -n " `cat /proc/$i/oom_adj`  "
					echo -n $i
					echo "  `cat /proc/$i/cmdline | sed 's/.*\///'`"
				  fi
				done
			else
				echo " VISIBLE_APP OOM GROUPING"
				echo " ========================"
				if [ "$HL" -gt "$oomadj1" ] && [ "$HL" -le "$oomadj2" ]; then
					echo ""
					echo " HOME LAUNCHER IS IN HERE! (ADJ=$HL)"
					echo ""
				fi
				for i in `ls /proc`; do
				  if [ "$i" -ne 0 ] 2>/dev/null && [ -f "/proc/$i/oom_adj" ] && [ "`cat /proc/$i/cmdline`" ] && [ "`cat /proc/$i/oom_adj`" -gt "$oomadj1" ] && [ "`cat /proc/$i/oom_adj`" -le "$oomadj2" ]; then
					echo -n " `cat /proc/$i/oom_adj`  "
					echo -n $i
					echo "  `cat /proc/$i/cmdline | sed 's/.*\///'`"
				  fi
				done
			fi
			echo ""
			echo " SECONDARY_SERVER OOM GROUPING"
			echo " ============================="
			if [ "$HL" -gt "$oomadj2" ] && [ "$HL" -le "$oomadj3" ]; then
				echo ""
				echo " HOME LAUNCHER IS IN HERE! (ADJ=$HL)"
				echo ""
			fi
			for i in `ls /proc`; do
			  if [ "$i" -ne 0 ] 2>/dev/null && [ -f "/proc/$i/oom_adj" ] && [ "`cat /proc/$i/cmdline`" ] && [ "`cat /proc/$i/oom_adj`" -gt "$oomadj2" ] && [ "`cat /proc/$i/oom_adj`" -le "$oomadj3" ]; then
				echo -n " `cat /proc/$i/oom_adj`  "
				echo -n $i
				echo "  `cat /proc/$i/cmdline | sed 's/.*\///'`"
			  fi
			done
			echo ""
			echo " HIDDEN_APP OOM GROUPING"
			echo " ======================="
			if [ "$HL" -gt "$oomadj3" ] && [ "$HL" -le "$oomadj4" ]; then
				echo ""
				echo " HOME LAUNCHER IS IN HERE! (ADJ=$HL)"
				echo ""
			fi
			for i in `ls /proc`; do
			  if [ "$i" -ne 0 ] 2>/dev/null && [ -f "/proc/$i/oom_adj" ] && [ "`cat /proc/$i/cmdline`" ] && [ "`cat /proc/$i/oom_adj`" -gt "$oomadj3" ] && [ "`cat /proc/$i/oom_adj`" -le "$oomadj4" ]; then
				echo -n " `cat /proc/$i/oom_adj`  "
				echo -n $i
				echo "  `cat /proc/$i/cmdline | sed 's/.*\///'`"
			  fi
			done
			echo ""
			echo " CONTENT_PROVIDER OOM GROUPING"
			echo " ============================="
			if [ "$HL" -gt "$oomadj4" ] && [ "$HL" -le "$oomadj5" ]; then
				echo ""
				echo " HOME LAUNCHER IS IN HERE! (ADJ=$HL)"
				echo ""
			fi
			for i in `ls /proc`; do
			  if [ "$i" -ne 0 ] 2>/dev/null && [ -f "/proc/$i/oom_adj" ] && [ "`cat /proc/$i/cmdline`" ] && [ "`cat /proc/$i/oom_adj`" -gt "$oomadj4" ] && [ "`cat /proc/$i/oom_adj`" -le "$oomadj5" ]; then
				echo -n " `cat /proc/$i/oom_adj`  "
				echo -n $i
				echo "  `cat /proc/$i/cmdline | sed 's/.*\///'`"
			  fi
			done
			echo ""
			echo " EMPTY_APP OOM GROUPING"
			echo " ======================"
			if [ "$HL" -gt "$oomadj5" ] && [ "$HL" -le "$oomadj6" ]; then
				echo ""
				echo " HOME LAUNCHER IS IN HERE! (ADJ=$HL)"
				echo ""
			fi
			for i in `ls /proc`; do
			  if [ "$i" -ne 0 ] 2>/dev/null && [ -f "/proc/$i/oom_adj" ] && [ "`cat /proc/$i/cmdline`" ] && [ "`cat /proc/$i/oom_adj`" -gt "$oomadj5" ] && [ "`cat /proc/$i/oom_adj`" -le "$oomadj6" ]; then
				echo -n " `cat /proc/$i/oom_adj`  "
				echo -n $i
				echo "  `cat /proc/$i/cmdline | sed 's/.*\///'`"
			  fi
			done
			echo ""
			echo $line
			if [ "$opt" -eq 16 ]; then echo "              zOOM Stick Complete!"; fi
		elif [ "$vroomverifier" ] && [ "$vroomverifier" -eq 1 ]; then
			echo " Using vrOOM Stick Mode (Verbose)..."
			echo ""
			$sleep
			echo " ADJ  PID    Process/Path        (APK)"
			echo ""
			echo " FOREGROUND_APP OOM GROUPING"
			echo " ==========================="
			if [ "$HL" -gt -18 ] && [ "$HL" -lt "$oomadj1" ]; then
				echo ""
				echo " HOME LAUNCHER IS IN HERE! (ADJ=$HL)"
				echo ""
			fi
			for i in `ls /proc`; do
			  if [ "$i" -ne 0 ] 2>/dev/null && [ -f "/proc/$i/oom_adj" ] && [ "`cat /proc/$i/cmdline`" ] && [ "`cat /proc/$i/oom_adj`" -gt -18 ] && [ "`cat /proc/$i/oom_adj`" -lt "$oomadj1" ]; then
				appname=`cat /proc/$i/cmdline | sed 's/.*\///'`
#				apkname=`su -c "LD_LIBRARY_PATH=/vendor/lib:/system/lib pm list packages -f $appname | sed 's/.*://' | sed 's/=.*//'"`
				apkname=`busybox find / -iname "*$appname*.apk" -maxdepth 3 | tail -n 1`
				echo -n " `cat /proc/$i/oom_adj`  "
				echo -n $i
				if [ "$apkname" ]; then
					echo -n "  `cat /proc/$i/cmdline`"
					echo "  ($apkname)"
				else echo "  `cat /proc/$i/cmdline`"
				fi
			  fi
			done
			if [ "$HL" -eq "$oomadj1" ]; then
				echo ""
				echo " HOME LAUNCHER IS IN HERE! (ADJ=$HL)"
				echo ""
			fi
			for i in `ls /proc`; do
			  if [ "$i" -ne 0 ] 2>/dev/null && [ -f "/proc/$i/oom_adj" ] && [ "`cat /proc/$i/cmdline`" ] && [ "`cat /proc/$i/oom_adj`" -eq "$oomadj1" ]; then
				appname=`cat /proc/$i/cmdline | sed 's/.*\///'`
#				apkname=`su -c "LD_LIBRARY_PATH=/vendor/lib:/system/lib pm list packages -f $appname | sed 's/.*://' | sed 's/=.*//'"`
				apkname=`busybox find / -iname "*$appname*.apk" -maxdepth 3 | tail -n 1`
				echo -n " `cat /proc/$i/oom_adj`  "
				echo -n $i
				if [ "$apkname" ]; then
					echo -n "  `cat /proc/$i/cmdline`"
					echo "  ($apkname)"
				else echo "  `cat /proc/$i/cmdline`"
				fi
			  fi
			done
			echo ""
			if [ "$scadj" ] && [ "$currentadj" = "$scadj" ] && [ "$oomstick" -eq 1 ] && [ "$HL" -ne "$oomadj1" ]; then
				echo " HOME_LAUNCHER OOM GROUPING!"
				echo " ==========================="
				for i in `ls /proc`; do
				  if [ "$i" -ne 0 ] 2>/dev/null && [ -f "/proc/$i/oom_adj" ] && [ "`cat /proc/$i/cmdline`" ] && [ "`cat /proc/$i/oom_adj`" -gt "$oomadj1" ] && [ "`cat /proc/$i/oom_adj`" -lt "$oomadj2" ]; then
					if [ "`cat /proc/$i/oom_adj`" -eq "$HL" ] && [ "$HL" -ne "$PA" ] && [ "$lname" = "`cat /proc/$i/cmdline`" ]; then
						echo ""
						echo " Home Launcher is on the NEXT line! (ADJ=$HL)"
					fi
					appname=`cat /proc/$i/cmdline | sed 's/.*\///'`
					apkname=`su -c "LD_LIBRARY_PATH=/vendor/lib:/system/lib pm list packages -f $appname | sed 's/.*://' | sed 's/=.*//'"`
#					apkname=`busybox find / -iname "*$appname*.apk" -maxdepth 3 | tail -n 1`
					echo -n " `cat /proc/$i/oom_adj`  "
					echo -n $i
					if [ "$apkname" ]; then
						echo -n "  `cat /proc/$i/cmdline`"
						echo "  ($apkname)"
					else echo "  `cat /proc/$i/cmdline`"
					fi
					if [ "`cat /proc/$i/oom_adj`" -eq "$HL" ]; then echo ""; fi
				  fi
				done
				echo ""
				echo " VISIBLE_APP OOM GROUPING"
				echo " ========================"
				for i in `ls /proc`; do
				  if [ "$i" -ne 0 ] 2>/dev/null && [ -f "/proc/$i/oom_adj" ] && [ "`cat /proc/$i/cmdline`" ] && [ "`cat /proc/$i/oom_adj`" -eq "$oomadj2" ]; then
					appname=`cat /proc/$i/cmdline | sed 's/.*\///'`
#					apkname=`su -c "LD_LIBRARY_PATH=/vendor/lib:/system/lib pm list packages -f $appname | sed 's/.*://' | sed 's/=.*//'"`
					apkname=`busybox find / -iname "*$appname*.apk" -maxdepth 3 | tail -n 1`
					echo -n " `cat /proc/$i/oom_adj`  "
					echo -n $i
					if [ "$apkname" ]; then
						echo -n "  `cat /proc/$i/cmdline`"
						echo "  ($apkname)"
					else echo "  `cat /proc/$i/cmdline`"
					fi
				  fi
				done
			else
				echo " VISIBLE_APP OOM GROUPING"
				echo " ========================"
				if [ "$HL" -gt "$oomadj1" ] && [ "$HL" -le "$oomadj2" ]; then
					echo ""
					echo " HOME LAUNCHER IS IN HERE! (ADJ=$HL)"
					echo ""
				fi
				for i in `ls /proc`; do
				  if [ "$i" -ne 0 ] 2>/dev/null && [ -f "/proc/$i/oom_adj" ] && [ "`cat /proc/$i/cmdline`" ] && [ "`cat /proc/$i/oom_adj`" -gt "$oomadj1" ] && [ "`cat /proc/$i/oom_adj`" -le "$oomadj2" ]; then
					appname=`cat /proc/$i/cmdline | sed 's/.*\///'`
#					apkname=`su -c "LD_LIBRARY_PATH=/vendor/lib:/system/lib pm list packages -f $appname | sed 's/.*://' | sed 's/=.*//'"`
					apkname=`busybox find / -iname "*$appname*.apk" -maxdepth 3 | tail -n 1`
					echo -n " `cat /proc/$i/oom_adj`  "
					echo -n $i
					if [ "$apkname" ]; then
						echo -n "  `cat /proc/$i/cmdline`"
						echo "  ($apkname)"
					else echo "  `cat /proc/$i/cmdline`"
					fi
				  fi
				done
			fi
			echo ""
			echo " SECONDARY_SERVER OOM GROUPING"
			echo " ============================="
			if [ "$HL" -gt "$oomadj2" ] && [ "$HL" -le "$oomadj3" ]; then
				echo ""
				echo " HOME LAUNCHER IS IN HERE! (ADJ=$HL)"
				echo ""
			fi
			for i in `ls /proc`; do
			  if [ "$i" -ne 0 ] 2>/dev/null && [ -f "/proc/$i/oom_adj" ] && [ "`cat /proc/$i/cmdline`" ] && [ "`cat /proc/$i/oom_adj`" -gt "$oomadj2" ] && [ "`cat /proc/$i/oom_adj`" -le "$oomadj3" ]; then
				appname=`cat /proc/$i/cmdline | sed 's/.*\///'`
#				apkname=`su -c "LD_LIBRARY_PATH=/vendor/lib:/system/lib pm list packages -f $appname | sed 's/.*://' | sed 's/=.*//'"`
				apkname=`busybox find / -iname "*$appname*.apk" -maxdepth 3 | tail -n 1`
				echo -n " `cat /proc/$i/oom_adj`  "
				echo -n $i
				if [ "$apkname" ]; then
					echo -n "  `cat /proc/$i/cmdline`"
					echo "  ($apkname)"
				else echo "  `cat /proc/$i/cmdline`"
				fi
			  fi
			done
			echo ""
			echo " HIDDEN_APP OOM GROUPING"
			echo " ======================="
			if [ "$HL" -gt "$oomadj3" ] && [ "$HL" -le "$oomadj4" ]; then
				echo ""
				echo " HOME LAUNCHER IS IN HERE! (ADJ=$HL)"
				echo ""
			fi
			for i in `ls /proc`; do
			  if [ "$i" -ne 0 ] 2>/dev/null && [ -f "/proc/$i/oom_adj" ] && [ "`cat /proc/$i/cmdline`" ] && [ "`cat /proc/$i/oom_adj`" -gt "$oomadj3" ] && [ "`cat /proc/$i/oom_adj`" -le "$oomadj4" ]; then
				appname=`cat /proc/$i/cmdline | sed 's/.*\///'`
#				apkname=`su -c "LD_LIBRARY_PATH=/vendor/lib:/system/lib pm list packages -f $appname | sed 's/.*://' | sed 's/=.*//'"`
				apkname=`busybox find / -iname "*$appname*.apk" -maxdepth 3 | tail -n 1`
				echo -n " `cat /proc/$i/oom_adj`  "
				echo -n $i
				if [ "$apkname" ]; then
					echo -n "  `cat /proc/$i/cmdline`"
					echo "  ($apkname)"
				else echo "  `cat /proc/$i/cmdline`"
				fi
			  fi
			done
			echo ""
			echo " CONTENT_PROVIDER OOM GROUPING"
			echo " ============================="
			if [ "$HL" -gt "$oomadj4" ] && [ "$HL" -le "$oomadj5" ]; then
				echo ""
				echo " HOME LAUNCHER IS IN HERE! (ADJ=$HL)"
				echo ""
			fi
			for i in `ls /proc`; do
			  if [ "$i" -ne 0 ] 2>/dev/null && [ -f "/proc/$i/oom_adj" ] && [ "`cat /proc/$i/cmdline`" ] && [ "`cat /proc/$i/oom_adj`" -gt "$oomadj4" ] && [ "`cat /proc/$i/oom_adj`" -le "$oomadj5" ]; then
				appname=`cat /proc/$i/cmdline | sed 's/.*\///'`
#				apkname=`su -c "LD_LIBRARY_PATH=/vendor/lib:/system/lib pm list packages -f $appname | sed 's/.*://' | sed 's/=.*//'"`
				apkname=`busybox find / -iname "*$appname*.apk" -maxdepth 3 | tail -n 1`
				echo -n " `cat /proc/$i/oom_adj`  "
				echo -n $i
				if [ "$apkname" ]; then
					echo -n "  `cat /proc/$i/cmdline`"
					echo "  ($apkname)"
				else echo "  `cat /proc/$i/cmdline`"
				fi
			  fi
			done
			echo ""
			echo " EMPTY_APP OOM GROUPING"
			echo " ======================"
			if [ "$HL" -gt "$oomadj5" ] && [ "$HL" -le "$oomadj6" ]; then
				echo ""
				echo " HOME LAUNCHER IS IN HERE! (ADJ=$HL)"
				echo ""
			fi
			for i in `ls /proc`; do
			  if [ "$i" -ne 0 ] 2>/dev/null && [ -f "/proc/$i/oom_adj" ] && [ "`cat /proc/$i/cmdline`" ] && [ "`cat /proc/$i/oom_adj`" -gt "$oomadj5" ] && [ "`cat /proc/$i/oom_adj`" -le "$oomadj6" ]; then
				appname=`cat /proc/$i/cmdline | sed 's/.*\///'`
#				apkname=`su -c "LD_LIBRARY_PATH=/vendor/lib:/system/lib pm list packages -f $appname | sed 's/.*://' | sed 's/=.*//'"`
				apkname=`busybox find / -iname "*$appname*.apk" -maxdepth 3 | tail -n 1`
				echo -n " `cat /proc/$i/oom_adj`  "
				echo -n $i
				if [ "$apkname" ]; then
					echo -n "  `cat /proc/$i/cmdline`"
					echo "  ($apkname)"
				else echo "  `cat /proc/$i/cmdline`"
				fi
			  fi
			done
			echo ""
			echo $line
			echo "             vrOOM Stick Complete!"
		fi
		if [ "$vroomverifier" ] && [ "$opt" -eq 17 ] || [ "$resuperable" ] || [ "$ReSuperCharge" -eq 1 ]; then
			if [ "$resuperable" ] || [ "$ReSuperCharge" -eq 1 ] && [ "$allrcpaths" ]; then
				echo $line
				echo " Installing BulletProof Apps Service to..."
				echo $line
				echo ""
				$sleep
			fi
			for bps in $initrcpath $allrcpaths; do
				sed -i '/BulletProof_Apps_Service/,/BulletProofed_Apps_Service/d' $bps
				cat >> $bps <<EOF
# BulletProof_Apps_Service created by zeppelinrox.
#
# DO NOT DELETE COMMENTS. DELETING COMMENTS WILL BREAK UNINSTALL ROUTINE!
  ####################################
 #  BulletProof Apps Service Notes  #
####################################
# To stop the BulletProof Apps Service, run Terminal Emulator and type...
# "su" and Enter.
# "stop bullet_service" and Enter.
#
# To restart the BulletProof Apps Service, type...
# "su" and Enter.
# "start bullet_service" and Enter.
#
# If the service is running and you type: "cat /proc/*/cmdline | grep Bullet".
# The output would be 3 lines:
# /data/97BulletProof_Apps.sh
# BulletProof_Apps_is_In_Effect!
# Bullet
#
# If it's not running, the output would be the last item ie. "Bullet" which was generated by the typed command.
#
service bullet_service /system/bin/sh /data/97BulletProof_Apps.sh
    class post-zygote_services
    user root
    group root
#
# End of BulletProofed_Apps_Service Entries.
EOF
				cp -fr $bps /sdcard/V6_SuperCharger
				if [ "$bps" != "$initrcpath" ] && [ "$allrcpaths" ]; then if [ "$resuperable" ] || [ "$ReSuperCharge" -eq 1 ]; then echo "   ...$bps!"; $sleep; fi; fi
			done
			if [ "$resuperable" ] || [ "$ReSuperCharge" -eq 1 ] && [ "$allrcpaths" ]; then
				echo ""
				if [ "$resuperable" ] && [ "$allrcpaths" ]; then
					echo $line
					echo ""
					$sleep
					echo " Re-SuperCharger is ALMOST done!"
					echo ""
					$sleep
					echo " Select YES in the next step to..."
					echo ""
					$sleep
					echo $line
					echo "                       ...SuperClean & ReStart!"
					echo $line
					echo ""
					$sleep
					opt=29
					echo -n " Press The Enter Key..."
					read enter
					echo ""
				else opt=22
				fi
			fi
		fi
		if [ "$vroomverifier" ] && [ "$opt" -eq 17 ]; then
		  while :; do
			bpps=;bppid=;bpappname=
			echo ""
			$sleep
			if [ ! "$bptips" ]; then
				echo " Ideas: BulletProof the Phone, Media or SMS App!"
				echo ""
				$sleep
				if [ "$ics" -eq 1 ] && [ ! "$icssced" ];then
					echo $line
					echo " ICS TIP! Try BulletProofing The Launcher Here!"
					echo $line
					echo ""
					$sleep
				fi
				bptips=shown
			fi
			echo " Enter a unique segment of the process name..."
			echo ""
			$sleep
			echo " Example: \"Opera\" for Opera Browser..."
			echo ""
			$sleep
			echo -n " Or press the Enter key to exit: "
			read bpps
			echo ""
			echo $line
			if [ ! "$bpps" ]; then
				echo " Okay... See Ya Later! ;-)"
				break
			elif [ ! "`pgrep $bpps`" ]; then
				echo " Can't find $bpps running..."
				echo ""
				$sleep
				echo "             ...so it can't be BulletProofed :("
			else
				bppid=`pgrep $bpps`
				if [ -f "/proc/$bppid/cmdline" ]; then bpappname=`cat /proc/$bppid/cmdline | sed 's/.*\///'`
				else
					echo ""
					echo " Grrrr... there's more than one match!"
					echo ""
					$sleep
					for b in `pgrep -l $bpps`; do
					  if [ "$b" \< "a" ] && [ -f "/proc/$b/cmdline" ]; then
						bpappnametemp=`cat /proc/$b/cmdline | sed 's/.*\///'`
						echo -n " Want $bpappnametemp? (Y)es or E(N)ter: "
						read pick
						case $pick in
						  y|Y)bpappname=$bpappnametemp;;
							*);;
						esac
						echo ""
						$sleep
					  fi
					  if [ "$bpappname" ]; then echo $line; break; fi
					done 2>/dev/null
				fi
				if [ "$bpappname" ]; then
					echo " BulletProofing $bpappname..."
					echo ""
					$sleep
					echo "-17" > /proc/`pgrep $bpps`/oom_adj
					renice -10 `pgrep $bpps`
					echo " $bpappname has been BulletProofed!"
					echo ""
					$sleep
					echo -n " $bpappname's oom score is "
					cat /proc/`pgrep $bpps`/oom_score
					echo ""
					$sleep
					echo -n " $bpappname's oom priority is "
					cat /proc/`pgrep $bpps`/oom_adj
					echo $line
					if [ "`ls /data/V6_SuperCharger/BulletProof_One_Shots | grep $bpappname`" ]; then
						echo " This BulletProof \"One-Shot\" Script Exists! :o)"
					else
						echo ""
						$sleep
						echo " Save BulletProof \"One-Shot\" Script to..."
						echo ""
						$sleep
						echo " ...data/V6_SuperCharger/BulletProof_One_Shots?"
						echo ""
						$sleep
						if [ ! "$bpwinfo" ]; then
							echo " If so, with Script Manager..."
							echo ""
							$sleep
							echo "       ...make a \"Quick Widget\" for it..."
							echo ""
							$sleep
							echo "              ...or put it on a timed schedule!"
							echo ""
							$sleep
							bpwinfo=shown
						fi
						echo -n " Enter N for No, any key for Yes: "
						read bpsps
						echo ""
						echo $line
						case $bpsps in
						  n|N)echo " Okay... maybe next time!";;
							*)cat > /data/V6_SuperCharger/BulletProof_One_Shots/1Shot-$bpappname.sh <<EOF
#!/system/bin/sh
#
# BulletProof "One-Shot" Script created by zeppelinrox.
#
line=================================================
clear
echo ""
echo \$line
echo " -=BulletProof=- \"One-Shot\" by -=zeppelinrox=-"
echo \$line
echo ""
sleep 1
if [ ! "\`id | grep =0\`" ]; then
	sleep 2
	echo " You are NOT running this script as root..."
	echo ""
	sleep 3
	echo \$line
	echo "                      ...No SuperUser for you!!"
	echo \$line
	echo ""
	sleep 3
	echo "     ...Please Run as Root and try again..."
	echo ""
	echo \$line
	echo ""
	sleep 3
	exit 69
elif [ "\`pgrep $bpappname\`" ]; then
	for bp in \`pgrep $bpappname\`; do
		echo -17 > /proc/\$bp/oom_adj
		renice -10 \$bp
		echo " BulletProofed $bpappname!"
		echo ""
		echo -n " $bpappname's oom score is "
		cat /proc/\$bp/oom_score
		echo ""
		echo -n " $bpappname's oom priority is "
		cat /proc/\$bp/oom_adj
		echo ""
	done
else
	echo " Can't find $bpappname running..."
	echo ""
	echo "             ...so it can't be BulletProofed :("
	echo ""
fi
sleep 1
echo "    ======================================="
echo "     ) BulletProof \"One-Shot\" Completed! ("
echo "    ======================================="
echo ""
exit 0
EOF
							  echo " Saved 1Shot-$bpappname.sh to..."
							  echo ""
							  $sleep
							  chown 0.0 /data/V6_SuperCharger/BulletProof_One_Shots/*; chmod 777 /data/V6_SuperCharger/BulletProof_One_Shots/*
							  cp -fr /data/V6_SuperCharger/BulletProof_One_Shots /sdcard/V6_SuperCharger
							  echo " ...data/V6_SuperCharger/BulletProof_One_Shots!";;
						esac
					fi
					if [ "$bpapplist" ]; then
						echo $line
						echo ""
						$sleep
						echo " You are already BulletProofing..."
						echo ""
						$sleep
						for bpapp in $bpapplist; do echo "          $bpapp"; done
						echo ""
					fi
					if [ "`echo $bpapplist | grep $bpappname`" ]; then
						$sleep
						echo $line
						echo " $bpappname's on the Hit List! ;-)"
					else
						echo $line
						echo ""
						$sleep
						if [ ! "$bpainfo" ]; then
							echo " The *97BulletProof_Apps Boot Script loads..."
							echo ""
							$sleep
							echo " /data/V6_SuperCharger/BulletProof_Apps_HitList."
							echo ""
							$sleep
							echo $line
							echo " Tip: Manually trim or add apps to this list!"
							echo $line
							echo ""
							$sleep
							echo " If it DOESN'T run automatically..."
							echo "       ======="
							$sleep
							echo "          ...make a \"Quick Widget\" for..."
							echo ""
							$sleep
							echo "      ...The *97BulletProof_Apps Boot Script..."
							echo ""
							$sleep
							echo " OR, for LESS overhead... use Terminal Emulator!"
							echo ""
							$sleep
							echo " Type: \"su\" and enter, \"/data/97*\" and enter!"
							echo ""
							$sleep
							echo "                  THAT'S IT!"
							echo ""
							$sleep
							echo $line
							echo "    NOTE: DO NOT put it on a timed schedule!"
							echo $line
							echo ""
							$sleep
							echo "    It runs every 60 seconds automatically!"
							echo "    ======================================="
							echo ""
							echo $line
							echo ""
							$sleep
							echo " Also, in the /data/V6_SuperCharger folder..."
							echo ""
							$sleep
							echo "         ...find BulletProof_Apps_Fine_Tuner.sh"
							echo ""
							$sleep
							echo $line
							echo " Read its notes and \"Fine Tune\" App Priorities!"
							echo $line
							echo ""
							$sleep
							bpainfo=shown
						fi
						echo " Add to the HitList?"
						echo ""
						$sleep
						echo -n " Enter Y for Yes, any key for No: "
						read bpabs
						echo ""
						echo $line
						case $bpabs in
						y|Y)echo ""
							$sleep
							echo $bpappname >> /data/V6_SuperCharger/BulletProof_Apps_HitList
							bpapplist=`cat /data/V6_SuperCharger/BulletProof_Apps_HitList`
							cat > /data/97BulletProof_Apps.sh <<EOF
#!/system/bin/sh
#
# BulletProof Apps Boot Script created by zeppelinrox.
#
# Usage: 1. Runs automatically via custom rom's init.d folder or BulletProof Apps Service.
#        2. Type in Terminal: "su" and enter, "/data/97*" and enter.
#        3. Script Manager: launch it once like any other script OR with a widget (DO NOT PUT IT ON A SCHEDULE!)
#
# To verify that it's running, type in Terminal:
# 1. "pstree | grep Bullet" - for init.d script or usage option 2
# 2. "pstree | grep sleep" - if using the service or Script Manager
# 3. "cat /proc/*/cmdline | grep Bullet" - Sure-Fire method :)
#    - This output should be 3 items:
#      a. "/path/to/*97BulletProof_Apps*"
#      b. "BulletProof_Apps_is_In_Effect!" (sleep message)
#      c. "Bullet" (created by your query so this doesn't mean anything)
#
# This script loads a list of apps via /data/V6_SuperCharger/BulletProof_Apps_HitList.
# You can easily add to or edit the HitList right from your device!
# Just enter a unique segment of the proces name! However, the more info you give, the better ;-)
#
# This ALSO loads /data/V6_SuperCharger/BulletProof_Apps_Fine_Tuner.sh.
# Read the comments in that file for "Fine Tuning" instrucions!
  ####################################
 #  BulletProof Apps Service Notes  #
####################################
# To leave the BulletProof Apps Service running, insert a # at the beginning of the "stop bullet_service" entry near the middle of this script.
#
# To stop the BulletProof Apps Service, run Terminal Emulator and type...
# "su" and Enter.
# "stop bullet_service" and Enter.
#
# To restart the BulletProof Apps Service, type...
# "su" and Enter.
# "start bullet_service" and Enter.
#
# If the service is running and you type: "cat /proc/*/cmdline | grep Bullet".
# The output would be 3 lines:
# /data/97BulletProof_Apps.sh
# BulletProof_Apps_is_In_Effect!
# Bullet
#
# If it's not running, the output would be the last item ie. "Bullet" which was generated by the typed command.
#
EOF
							if [ ! "$allrcpaths" ]; then
								cat >> /data/97BulletProof_Apps.sh <<EOF
# Ummm... this service won't work on your current ROM. Sorry :P
#
EOF
							fi
							cat >> /data/97BulletProof_Apps.sh <<EOF
clear;
# For debugging, delete the # at the beginning of the following 2 lines, and check the end of /data/Log_BulletProof_Apps.log file to see what may have fubarred.
# set -x;
# exec > /data/Log_BulletProof_Apps.log 2>&1;
#
line=================================================;
echo "";
echo \$line;
echo " -=BulletProof Apps=- script by -=zeppelinrox=-";
echo \$line;
echo "";
if [ ! "\`id | grep =0\`" ]; then
	echo " You are NOT running this script as root...";
	echo "";
	echo \$line;
	echo "                      ...No SuperUser for you!!";
	echo \$line;
	echo "";
	echo "     ...Please Run as Root and try again...";
	echo "";
	echo \$line;
	echo "";
	exit 69;
elif [ "\`pgrep -l android\`" ]; then
	if [ "\`pgrep -l scriptmanager\`" ]; then
		sleep 2;
		echo "         DO NOT PUT THIS ON A SCHEDULE!";
		echo "";
		sleep 2;
		echo "    It runs every 60 seconds automatically!";
		echo "    =======================================";
		echo "";
		sleep 2;
	fi;
	echo " To verify that it's working...";
	echo "";
	echo "       ...check out /data/Ran_BulletProof_Apps!";
	echo "";
	echo \$line;
	echo "";
#	stop bullet_service;
	while :; do
		bpapplist=\`cat /data/V6_SuperCharger/BulletProof_Apps_HitList\`;
		for bpapp in \$bpapplist; do
			if [ "\`pgrep \$bpapp\`" ]; then
				for bp in \`pgrep \$bpapp\`; do
					echo -17 > /proc/\$bp/oom_adj;
					renice -10 \$bp;
					echo " BulletProofed \$bpapp!";
					echo -n " \$bpapp's oom score is ";
					cat /proc/\$bp/oom_score;
					echo -n " \$bpapp's oom priority is ";
					cat /proc/\$bp/oom_adj;
					echo "";
				done;
			else
				echo " Can't find \$bpapp running...";
				echo "             ...so it can't be BulletProofed :(";
				echo "";
			fi;
		done;
		sh /data/V6_SuperCharger/BulletProof_Apps_Fine_Tuner.sh;
		echo " This should update every minute!" > /data/Ran_BulletProof_Apps;
		echo " \$( date +"%m-%d-%Y %H:%M:%S" ): BulletProofed Apps with \$0!" >> /data/Ran_BulletProof_Apps;
		echo "         ==============================";
		echo "          ) BulletProofing Complete! (";
		echo "         ==============================";
		echo "";
		echo " Taking 60 seconds to reload... then...";
		echo "";
		echo "                ...Gonna BulletProof Some More!";
		echo "";
		sleep 60 | grep "BulletProof_Apps_is_In_Effect!";
		echo \$line;
		echo " zOOM... zOOM...";
		echo \$line;
		echo "";
	done;
else \$0 & exit 0;
fi;
EOF
							if [ ! -f "/data/V6_SuperCharger/BulletProof_Apps_Fine_Tuner.sh" ]; then
								cat > /data/V6_SuperCharger/BulletProof_Apps_Fine_Tuner.sh <<EOF
#!/system/bin/sh
#
# BulletProof Apps Fine Tuner created by zeppelinrox.
#
# Use this file to "Fine Tune" App Priorities to your liking!
#
# The possible values of oom_adj range from -17 to 15.
# The higher the score, more likely the associated process is to be killed by OOM-killer.
#
# A niceness (effects CPU Time) of -20 is the highest priority and 19 is the lowest priority.
# A higher priority process will get a larger chunk of the CPU time than a lower priority process.
# 0 is usually the default niceness for processes and is inherited from its parent process.
#
# In this script, by default, all apps have priorities applied as follows:
# echo -17 > /proc/\`pgrep *com.app.name*\`/oom_adj
# renice -10 \`pgrep *com.app.name*\`
# The renice of -10 should be sufficient to keep it snappy but not interfere with other apps that you're using :)
#
# At the opposite end, if you wanted something to get killed off easily, you can change it to:
# echo 15 > /proc/\`pgrep *com.app.name*\`/oom_adj
# renice 19 \`pgrep *com.app.name*\`
#
# Typically, an apps (Secondary Servers) values (when SuperCharged) are:
# echo 6 > /proc/\`pgrep *com.app.name*\`/oom_adj
# renice 0 \`pgrep *com.app.name*\`
#
line=================================================;
echo \$line;
echo "     -=BulletProof Apps Fine Tuner Module=-";
echo \$line;
echo "";
echo " Find this in /data/V6_SuperCharger folder...";
echo "";
echo "                       ...read its comments...";
echo "";
echo "             ...and \"Fine Tune\" App Priorities!";
echo "";
echo \$line;
echo "";
EOF
							fi
							if [ ! "`grep $bpappname /data/V6_SuperCharger/BulletProof_Apps_Fine_Tuner.sh`" ]; then
								cat >> /data/V6_SuperCharger/BulletProof_Apps_Fine_Tuner.sh <<EOF
# Begin $bpappname BulletProofing
if [ "\`pgrep $bpappname\`" ]; then
	for bp in \`pgrep $bpappname\`; do
		echo -17 > /proc/\$bp/oom_adj;
		renice -10 \$bp;
		echo " BulletProofed $bpappname!";
		echo -n " $bpappname's oom score is ";
		cat /proc/\$bp/oom_score;
		echo -n " $bpappname's oom priority is ";
		cat /proc/\$bp/oom_adj;
		echo "";
	done;
else
	echo " Can't find $bpappname running...";
	echo "             ...so it can't be BulletProofed :(";
	echo "";
fi;
# End $bpappname BulletProofing
EOF
							fi
							chown 0.0 /data/97BulletProof_Apps.sh; chmod 777 /data/97BulletProof_Apps.sh
							chown 0.0 /data/V6_SuperCharger/BulletProof_Apps*; chmod 777 /data/V6_SuperCharger/BulletProof_Apps*
							cp -fr /data/97BulletProof_Apps.sh /sdcard/V6_SuperCharger
							cp -fr /data/V6_SuperCharger/BulletProof_Apps* /sdcard/V6_SuperCharger
							echo " Here's Your Updated HitList!"
							echo ""
							$sleep
							for bpapp in $bpapplist; do echo "          $bpapp"; done
							echo ""
							echo $line
							echo ""
							$sleep
							echo " BulletProof_Apps_HitList will be loaded via..."
							echo ""
							$sleep
							if [ -d "/system/etc/init.d" ]; then
								cp -fr /data/97BulletProof_Apps.sh /system/etc/init.d/97BulletProof_Apps
								if [ -f "/system/etc/init.d/97BulletProof_Apps" ]; then
									sed -i '/bullet_service;/s/#//' /system/etc/init.d/97BulletProof_Apps
									for i in /system/etc/init.d/S*; do Sfiles=$i;break;done
									if [ "$Sfiles" = "/system/etc/init.d/S*" ]; then
										echo "      .../system/etc/init.d/97BulletProof_Apps!"
									else
										mv /system/etc/init.d/97BulletProof_Apps /system/etc/init.d/S97BulletProof_Apps
										echo "     .../system/etc/init.d/S97BulletProof_Apps!"
									fi
									cp -fr /system/etc/init.d/*BulletProof_Apps* /sdcard/V6_SuperCharger
								else
									echo " WARNING: ERROR copying file to /system/init.d!"
									echo ""
									$sleep
									echo " Got enough free space?"
									echo ""
									$sleep
									echo " System Partition has `df -h system | awk '{print $4,"Free ("$5}' | sed -n 2p` Used)"
								fi
							else echo "                .../data/97BulletProof_Apps.sh!"
							fi
							echo ""
							if [ ! "$bpsinfo" ]; then
								$sleep
								echo $line
								echo " Is it working? See /data/Ran_BulletProof_Apps!"
								echo $line
								echo ""
								$sleep
								echo " Or... in Terminal Emulator, you can type..."
								echo ""
								$sleep
								echo " \"pstree | grep Bullet\" OR \"pstree | grep sleep\""
								echo ""
								echo " OR... get COMPLETE information with..."
								echo ""
								$sleep
								echo "         ...\"cat /proc/*/cmdline | grep Bullet\"!"
								echo ""
								$sleep
								echo " The output should look like this:"
								echo ""
								$sleep
								echo "      /path/to/*97BulletProof_Apps*"
								echo "      BulletProof_Apps_is_In_Effect!"
								echo "      Bullet"
								echo ""
							fi
							if [ "$allrcpaths" ] && [ ! "$bpsinfo" ]; then
								echo $line
								echo ""
								$sleep
								echo " The BulletProof Apps Service should..."
								echo ""
								$sleep
								echo "       ...automatically run this on a schedule!"
								echo ""
								$sleep
								echo " BUT if it doesn't, just be sure that..."
								echo ""
								$sleep
								echo " ...the *97BulletProof_Apps script runs at boot!"
								echo ""
								$sleep
								echo $line
								echo " BulletProof Apps Service was installed to..."
								echo $line
								echo ""
								$sleep
								for bps in $allrcpaths; do
									echo "   ...$bps!"
									if [ ! "$bpservice" ]; then $sleep; fi
								done
								echo ""
								echo $line
								echo ""
								$sleep
								echo " You can stop the service by either..."
								echo ""
								$sleep
								echo "   ...reading comments in 97BulletProof_Apps.sh"
								echo ""
								$sleep
								echo " Or... Run Terminal Emulator..."
								echo ""
								$sleep
								echo "              ...type \"su\" and Enter..."
								echo "                       =="
								echo ""
								$sleep
								echo "       ...type \"stop bullet_service\" and Enter."
								echo "                ==================="
								echo ""
								$sleep
								echo " To restart the BulletProof Apps Service..."
								echo ""
								$sleep
								echo "       ..type \"start bullet_service\" and Enter."
								echo "               ===================="
								echo ""
								echo $line
								echo ""
								$sleep
								echo " \"cat /proc/*/cmdline | grep Bullet\"..."
								echo ""
								$sleep
								echo "   ...will also show if the service is running!"
								echo ""
							elif [ ! "$allrcpaths" ] && [ ! "$bpsinfo" ]; then
								echo $line
								$sleep
								echo " BulletProof Apps Service Entries Installed!"
								echo $line
								echo ""
								$sleep
								echo " This won't work on this ROM but... "
								echo ""
								$sleep
								echo "       ...it makes for easy cooking into a ROM!"
								echo ""
								$sleep
								echo " Just read the comments in $initrcpath :-)"
								echo ""
								if [ "$smrun" ]; then
									echo $line
									echo ""
									$sleep
									echo "THIS app can load 97BulletProof_Apps.sh on boot!"
									echo ""
								fi
							fi
							bpsinfo=shown;;
						  *)echo " Okay... maybe next time!";;
						esac
					fi
				fi
			fi
			echo $line
		  done
		fi
	fi
	if [ "$opt" -eq 18 ]; then
		echo " Your device may get laggy after a day or two.."
		echo ""
		$sleep
		echo "                    ...if you haven't rebooted."
		echo ""
		$sleep
		echo " It happens when system caches keep growing..."
		echo ""
		$sleep
		echo "            ...and free RAM keeps shrinking..."
		echo ""
		$sleep
		echo "           ...and apps are starved for memory!"
		echo ""
		echo $line
		echo ""
		$sleep
		echo " This Engine Flush will give you a Quick Boost!"
		echo ""
		$sleep
		echo " The system will drop all file system caches..."
		echo ""
		$sleep
		echo "       ...which means more free RAM and no lag!"
		echo ""
		$sleep
		echo "                        ..so no need to reboot!"
		echo ""
		$sleep
		echo $line
		echo "  Note that \"Used RAM\" INCLUDES Cached Apps!!"
		echo $line
		echo ""
		$sleep
		echo " ie. Most tools will report $ramreportedfree MB Free RAM!"
		echo ""
		$sleep
		ramused=$((`free | awk '{ print $3 }' | sed -n 2p`/1024))
		ramkbytesfree=`free | awk '{ print $4 }' | sed -n 2p`
		ramkbytescached=`cat /proc/meminfo | grep Cached | awk '{print $2}' | sed -n 1p`
		ramfree=$(($ramkbytesfree/1024));ramcached=$(($ramkbytescached/1024));ramreportedfree=$(($ramfree + $ramcached))
		echo " Current RAM Stats:"
		echo " ================="
		echo ""
		$sleep
		echo " Total: $ram MB  Used: $ramused MB  True Free: $ramfree MB"
		echo ""
		$sleep
		echo $line
		echo " True Free $ramfree MB = \"Free\" $ramreportedfree - Cached Apps $ramcached"
		echo $line
		echo ""
		$sleep
		echo " ...OR...    True Free RAM   $ramfree"
		echo "               Cached Apps + $ramcached"
		echo "                           ========"
		echo "       Reported \"Free\" RAM = $ramreportedfree MB"
		echo ""
		echo $line
		echo ""
		$sleep
		echo " Continue and drop all file system caches?"
		echo ""
		$sleep
		echo -n " Enter N for No, any key for Yes: "
		read flush
		echo ""
		echo $line
		case $flush in
		  n|N)echo " File system caches were retained...";;
			*)busybox sync; echo 3 > /proc/sys/vm/drop_caches
			  sleep 2
			  echo 0 > /proc/sys/vm/drop_caches
			  echo " Engine Flush Completed!"
			  echo $line
			  echo ""
			  $sleep
			  ramused=$((`free | awk '{ print $3 }' | sed -n 2p`/1024))
			  ramkbytesfree=`free | awk '{ print $4 }' | sed -n 2p`
			  ramkbytescached=`cat /proc/meminfo | grep Cached | awk '{print $2}' | sed -n 1p`
			  ramfree=$(($ramkbytesfree/1024));ramcached=$(($ramkbytescached/1024));ramreportedfree=$(($ramfree + $ramcached))
			  echo " Updated RAM Stats:"
			  echo " ================="
			  echo ""
			  $sleep
			  echo " Total: $ram MB  Used: $ramused MB  True Free: $ramfree MB"
			  echo ""
			  $sleep
			  echo " Reported Free RAM: $ramreportedfree MB!"
			  echo ""
			  $sleep
			  echo $line
			  echo " True Free $ramfree MB = \"Free\" $ramreportedfree - Cached Apps $ramcached"
			  echo $line
			  echo ""
			  $sleep
			  echo " ...OR...    True Free RAM   $ramfree"
			  echo "               Cached Apps + $ramcached"
			  echo "                           ========"
			  echo "       Reported \"Free\" RAM = $ramreportedfree MB"
			  echo ""
			  $sleep
			  echo $line
			  echo "                   ...Enjoy Your Quick Boost :)";;
		esac
		echo $line
		echo ""
		$sleep
		cat > /data/V6_SuperCharger/!Fast_Engine_Flush.sh <<EOF
#!/system/bin/sh
#
# Fast Engine Flush Script created by zeppelinrox.
#
# See http://goo.gl/krtf9 - Linux Memory Consumption - Nice article which also discusses the "drop system cache" function!
# See http://goo.gl/hFdNO - Memory and SuperCharging Overview ...or... "Why \`Free RAM\` Is NOT Wasted RAM!"
#
# Credit imoseyon for making the drop caches command more well known :)
# See http://www.droidforums.net/forum/liberty-rom-d2/122733-tutorial-sysctl-you-guide-better-preformance-battery-life.html
# Credit dorimanx (Cool XDA dev!) for the neat idea to show before and after stats :D
#
line=================================================
clear
echo ""
echo \$line
echo " -=Fast Engine Flush=- script by -=zeppelinrox=-"
echo \$line
echo ""
sleep 1
if [ ! "\`id | grep =0\`" ]; then
	sleep 2
	echo " You are NOT running this script as root..."
	echo ""
	sleep 3
	echo \$line
	echo "                      ...No SuperUser for you!!"
	echo \$line
	echo ""
	sleep 3
	echo "     ...Please Run as Root and try again..."
	echo ""
	echo \$line
	echo ""
	sleep 3
	exit 69
fi
ram=\$((\`free | awk '{ print \$2 }' | sed -n 2p\`/1024))
ramused=\$((\`free | awk '{ print \$3 }' | sed -n 2p\`/1024))
ramkbytesfree=\`free | awk '{ print \$4 }' | sed -n 2p\`
ramkbytescached=\`cat /proc/meminfo | grep Cached | awk '{print \$2}' | sed -n 1p\`
ramfree=\$((\$ramkbytesfree/1024));ramcached=\$((\$ramkbytescached/1024));ramreportedfree=\$((\$ramfree + \$ramcached))
echo "  Note that \"Used RAM\" INCLUDES Cached Apps!!"
echo ""
sleep 1
echo \$line
echo " RAM Stats BEFORE Engine Flush..."
echo \$line
echo ""
sleep 1
echo " Total: \$ram MB  Used: \$ramused MB  True Free: \$ramfree MB"
echo ""
sleep 1
echo " Most tools will report \$ramreportedfree MB Free RAM!"
echo ""
sleep 1
echo \$line
echo " True Free \$ramfree MB = \"Free\" \$ramreportedfree - Cached Apps \$ramcached"
echo \$line
echo ""
sleep 1
echo " ...OR...    True Free RAM   \$ramfree"
echo "               Cached Apps + \$ramcached"
echo "                           ========"
echo "       Reported \"Free\" RAM = \$ramreportedfree MB"
echo ""
busybox sync; echo 3 > /proc/sys/vm/drop_caches
sleep 3
echo 1 > /proc/sys/vm/drop_caches
ramused=\$((\`free | awk '{ print \$3 }' | sed -n 2p\`/1024))
ramkbytesfree=\`free | awk '{ print \$4 }' | sed -n 2p\`
ramkbytescached=\`cat /proc/meminfo | grep Cached | awk '{print \$2}' | sed -n 1p\`
ramfree=\$((\$ramkbytesfree/1024));ramcached=\$((\$ramkbytescached/1024));ramreportedfree=\$((\$ramfree + \$ramcached))
echo \$line
echo "                ...RAM Stats AFTER Engine Flush"
echo \$line
echo ""
sleep 1
echo " Total: \$ram MB  Used: \$ramused MB  True Free: \$ramfree MB"
echo ""
sleep 1
echo " Most tools will report \$ramreportedfree MB Free RAM!"
echo ""
sleep 1
echo \$line
echo " True Free \$ramfree MB = \"Free\" \$ramreportedfree - Cached Apps \$ramcached"
echo \$line
echo ""
sleep 1
echo " ...OR...    True Free RAM   \$ramfree"
echo "               Cached Apps + \$ramcached"
echo "                           ========"
echo "       Reported \"Free\" RAM = \$ramreportedfree MB"
echo ""
sleep 1
echo "       =================================="
echo "        ) Fast Engine Flush Completed! ("
echo "       =================================="
exit 0
EOF
		chown 0.0 /data/V6_SuperCharger/!Fast_Engine_Flush.sh; chmod 777 /data/V6_SuperCharger/!Fast_Engine_Flush.sh
		cp -fr /data/V6_SuperCharger/!Fast_Engine_Flush.sh /sdcard/V6_SuperCharger
		cp -fr /data/V6_SuperCharger/!Fast_Engine_Flush.sh /sdcard/V6_SuperCharger/flush
		cp -fr /data/V6_SuperCharger/!Fast_Engine_Flush.sh /system/xbin/flush
		echo " For Fast Flushing..."
		echo ""
		$sleep
		echo "../data/V6_SuperCharger/!Fast_Engine_Flush.sh.."
		echo ""
		$sleep
		echo "                                ...was created!"
		echo ""
		$sleep
		echo " With Script Manager..."
		echo ""
		$sleep
		echo "       ...make a \"Quick Widget\" for it..."
		echo ""
		$sleep
		echo "              ...or put it on a timed schedule!"
		echo ""
		echo $line
		echo ""
		$sleep
		if [ -f "/system/xbin/flush" ]; then
			echo " There is another copy in /system/xbin..."
			echo ""
			$sleep
			echo " To use this script with Terminal Emulator..."
			echo ""
			$sleep
			echo " Run Terminal Emulator..."
			echo ""
			$sleep
			echo "           ...type \"su\" and Enter..."
			echo "                    =="
			echo ""
			$sleep
			echo "                   ...type \"flush\" and Enter..."
			echo "                            ====="
			echo ""
			$sleep
			echo "                  THAT'S IT!"
		else
			echo " WARNING: ERROR copying file to /system/xbin!"
			echo ""
			$sleep
			echo " Got enough free space?"
			echo ""
			$sleep
			echo " System Partition has `df -h system | awk '{print $4,"Free ("$5}' | sed -n 2p` Used)"
		fi
		echo ""
	fi
	if [ "$opt" -eq 19 ]; then
		echo " You have many SQLite databases that become..."
		echo ""
		$sleep
		echo " ...fragmented and unoptimized over a few days."
		echo ""
		$sleep
		echo " This tool will optimize them with..."
		echo ""
		$sleep
		echo "                  ...SQLite VACUUM and REINDEX!"
		echo ""
		echo $line
		echo ""
		$sleep
		echo " Optimize SQLite databases now?"
		echo ""
		$sleep
		echo -n " Enter N for No, any key for Yes: "
		read detailing
		echo ""
		echo $line
		case $detailing in
		  n|N)echo " SQLite Optimization declined... meh...";;
			*)echo ""
			  echo " Cool beans... just IGNORE errors that say..."
			  echo "                    ======"
			  echo ""
			  $sleep
			  echo " \"malformed database\" OR \"collation sequence\"!"
			  echo "  ==================      =================="
			  echo ""
			  $sleep
			  echo "   ...as they won't effect SQLite Optimization!"
			  echo ""
			  $sleep
			  echo $line
			  echo " This may take awhile... please wait..."
			  echo $line
			  echo ""
			  $sleep
			  for i in `busybox find /d* -iname "*.db"`; do
				sqlite3 $i 'VACUUM;';
				echo "  VACUUMED: $i"
				sqlite3 $i 'REINDEX;';
				echo " REINDEXED: $i"
			  done
			  echo "";;
		esac
		echo $line
		echo ""
		$sleep
		cat > /data/V6_SuperCharger/!Detailing.sh <<EOF
#!/system/bin/sh
#
# Detailing Script (SQLite VACUUM & REINDEX) created by zeppelinrox.
#
# Credit avgjoemomma (from XDA) for the added reindex bit :)
#
line=================================================
clear
echo ""
echo \$line
echo "    -=Detailing=- script by -=zeppelinrox=-"
echo \$line
echo ""
sleep 1
if [ ! "\`id | grep =0\`" ]; then
	sleep 2
	echo " You are NOT running this script as root..."
	echo ""
	sleep 3
	echo \$line
	echo "                      ...No SuperUser for you!!"
	echo \$line
	echo ""
	sleep 3
	echo "     ...Please Run as Root and try again..."
	echo ""
	echo \$line
	echo ""
	sleep 3
	exit 69
fi
echo " Commencing SQLite VACUUM & REINDEX!"
echo ""
sleep 1
echo " Please IGNORE any errors that say..."
echo "        ======"
echo ""
sleep 1
echo " \"malformed database\" OR \"collation sequence\"!"
echo "  ==================      =================="
echo ""
sleep 1
echo "   ...as they won't effect SQLite Optimization!"
echo ""
sleep 1
echo \$line
echo " This may take awhile... please wait..."
echo \$line
echo ""
for i in \`busybox find /d* -iname "*.db"\`; do
	sqlite3 \$i 'VACUUM;';
	echo "  VACUUMED: \$i"
	sqlite3 \$i 'REINDEX;';
	echo " REINDEXED: \$i"
done
echo ""
echo "           =========================="
echo "            ) Detailing Completed! ("
echo "           =========================="
echo ""
exit 0
EOF
		chown 0.0 /data/V6_SuperCharger/!Detailing.sh; chmod 777 /data/V6_SuperCharger/!Detailing.sh
		cp -fr /data/V6_SuperCharger/!Detailing.sh /sdcard/V6_SuperCharger
		cp -fr /data/V6_SuperCharger/!Detailing.sh /sdcard/V6_SuperCharger/vac
		cp -fr /data/V6_SuperCharger/!Detailing.sh /system/xbin/vac
		echo " For Diligent Detailing..."
		echo ""
		$sleep
		echo "   .../data/V6_SuperCharger/!Detailing.sh..."
		echo ""
		$sleep
		echo "                                ...was created!"
		echo ""
		$sleep
		echo " With Script Manager..."
		echo ""
		$sleep
		echo "       ...make a \"Quick Widget\" for it..."
		echo ""
		$sleep
		echo "             ...or put it on a weekly schedule!"
		echo ""
		echo $line
		echo ""
		$sleep
		if [ -f "/system/xbin/vac" ]; then
			echo " There is another copy in /system/xbin..."
			echo ""
			$sleep
			echo " To use this script with Terminal Emulator..."
			echo ""
			$sleep
			echo " Run Terminal Emulator..."
			echo ""
			$sleep
			echo "           ...type \"su\" and Enter..."
			echo "                    =="
			echo ""
			$sleep
			echo "                     ...type \"vac\" and Enter..."
			echo "                              ==="
			echo ""
			$sleep
			echo "                  THAT'S IT!"
		else
			echo " WARNING: ERROR copying file to /system/xbin!"
			echo ""
			$sleep
			echo " Got enough free space?"
			echo ""
			$sleep
			echo " System Partition has `df -h system | awk '{print $4,"Free ("$5}' | sed -n 2p` Used)"
		fi
		echo ""
	fi
	if [ "$opt" -eq 20 ]; then
		echo " This is EXPERIMENTAL..."
		echo ""
		$sleep
		echo "              ...it may improve multitasking..."
		echo ""
		$sleep
		echo "    ...it may make your device even snappier..."
		echo ""
		$sleep
		echo "                 ...it may really do nothing..."
		echo ""
		$sleep
		echo "     But some people swear that it's great!"
		echo ""
		$sleep
		echo " Values are added at the bottom of build.prop!"
		echo ""
		$sleep
		echo $line
		echo " Warning: A few users report Wi-Fi issues!"
		while :; do
			echo $line
			echo ""
			$sleep
			echo -n " Enter (N)ullify, (U)n-Nullify, E(X)it: "
			read nln
			echo ""
			echo $line
			case $nln in
			  n|N)nitro=1;break;;
			  u|U)nitro=2;break;;
			  x|X)nitro=3;break;;
				*)echo "      Invalid entry... Please try again :)";;
			esac
		done
		if [ "$nitro" -ne 3 ]; then
			if [ "$nitro" -eq 1 ]; then
				if [ -f "/system/build.prop.unsuper" ]; then echo " Leaving ORIGINAL build.prop backup intact..."
				else
					echo " Backing up ORIGINAL build.prop..."
					echo ""
					$sleep
					cp -r /system/build.prop /system/build.prop.unsuper
					echo "              ...as /system/build.prop.unsuper!"
				fi
				echo $line
				cp -fr /system/build.prop /sdcard/V6_SuperCharger/build.prop.unsuper
			fi
			echo ""
			$sleep
			if [ -f "/system/bin/build.prop" ] && [ ! -f "/system/bin/build.prop.unsuper" ]; then cp -r /system/bin/build.prop /system/bin/build.prop.unsuper; fi
			sed -i '/Nullifier/,/Nullified/d' /system/build.prop
			if [ "$nitro" -eq 1 ]; then
				cat >> /system/build.prop <<EOF
# Nitro Lag Nullifier created by zeppelinrox.
#
# DO NOT DELETE COMMENTS. DELETING COMMENTS WILL BREAK UNINSTALL ROUTINE!
#
ENFORCE_PROCESS_LIMIT=false
MAX_SERVICE_INACTIVITY=
MIN_HIDDEN_APPS=
MAX_HIDDEN_APPS=
CONTENT_APP_IDLE_OFFSET=
EMPTY_APP_IDLE_OFFSET=
MAX_ACTIVITIES=
ACTIVITY_INACTIVE_RESET_TIME=
# End of Nullified Entries.
EOF
				echo " Nitro Lag Nullifier installed..."
			else echo " Uninstalled Nitro Lag Nullifier..."
			fi
			echo ""
			$sleep
			if [ -f "/system/bin/build.prop" ]; then cp -fr /system/build.prop /system/bin; fi
			echo "                            ...Reboot required!"
		fi
		echo ""
	fi
	if [ "$opt" -eq 21 ]; then
		echo " This will copy V6 SuperCharger to /system/xbin"
		echo ""
		$sleep
		echo " To use this script with Terminal Emulator..."
		echo ""
		$sleep
		echo " Run Terminal Emulator..."
		echo ""
		$sleep
		echo "           ...type \"su\" and Enter..."
		echo "                    =="
		echo ""
		$sleep
		echo "                      ...type \"v6\" and Enter..."
		echo "                               =="
		echo ""
		$sleep
		echo "                  THAT'S IT!"
		echo ""
		$sleep
		echo " Note that su is short for SuperUser..."
		echo ""
		echo $line
		echo ""
		$sleep
		echo " So... continue installation?"
		echo ""
		$sleep
		echo -n " Enter Y for Yes, any key for No: "
		read v6xbin
		echo ""
		echo $line
		case $v6xbin in
		  y|Y)if [ "$0" == "v6" ]; then echo " You are already running it from system/xbin!"
			  else
				dd if=$0 of=/system/xbin/v6 2>/dev/null
				cp -fr $0 /sdcard/V6_SuperCharger/v6
				if [ -f "/system/xbin/v6" ]; then
					chown 0.0 /system/xbin/v6; chmod 777 /system/xbin/v6
					echo "  FABULOUS! Installation Was A Great Success!"
				else
					echo " WARNING: ERROR copying file to /system/xbin!"
					echo ""
					$sleep
					echo " Got enough free space?"
					echo ""
					$sleep
					echo " System Partition has `df -h system | awk '{print $4,"Free ("$5}' | sed -n 2p` Used)"
				fi
			  fi;;
			*)echo " Well... forget it then...";;
		esac
	fi
	if [ "$opt" -eq 29 ]; then
		if [ "$newsupercharger" ] && [ "$ran" -eq 1 ]; then
			echo "              ==================="
			busybox echo "             //// SUPERCLEAN! \\\\\\\\"
			echo $line
			echo ""
			$sleep
			resuperable=; newsupercharger=; firstgear=
		fi
		echo " This tool will wipe your dalvik-cache..."
		echo ""
		$sleep
		echo $line
		echo "     Bootloops are LESS likely to happen :)"
		echo $line
		echo ""
		$sleep
		echo " Initial boot will take a long time but..."
		echo ""
		$sleep
		echo "    ...your system will be clean and efficient!"
		echo ""
		$sleep
		echo $line
		echo " If the screen freezes during boot..."
		echo ""
		$sleep
		echo "         ...pull the battery and retry..."
		echo ""
		$sleep
		echo "    ...sometimes it takes 3 or more tries..."
		echo ""
		$sleep
		echo "           ...as everything gets re-configured!"
		echo $line
		echo ""
		$sleep
		cat > /data/V6_SuperCharger/!SuperClean.sh <<EOF
#!/system/bin/sh
#
# SuperClean & ReStart script (Wipe Dalvik Cache & Reboot) created by zeppelinrox.
#
line=================================================
clear
echo ""
echo \$line
echo "  -=SuperClean & ReStart=- by -=zeppelinrox=-"
echo \$line
echo ""
sleep 1
if [ ! "\`id | grep =0\`" ]; then
	sleep 2
	echo " You are NOT running this script as root..."
	echo ""
	sleep 3
	echo \$line
	echo "                      ...No SuperUser for you!!"
	echo \$line
	echo ""
	sleep 3
	echo "     ...Please Run as Root and try again..."
	echo ""
	echo \$line
	echo ""
	sleep 3
	exit 69
fi
echo " Commencing SuperClean & ReStart!"
echo ""
sleep 2
for dv in /d*/dalvik-cache /c*/dalvik-cache; do rm -r /\$dv/* 2>/dev/null; done
echo " All cleaned up and ready to..."
echo ""
sleep 2
echo \$line
echo "                    !!POOF!!"
echo \$line
echo ""
sleep 2
busybox sync
echo 1 > /proc/sys/kernel/sysrq 2>/dev/null
echo b > /proc/sysrq-trigger 2>/dev/null
echo "  If it don't go poofie, just reboot manually!"
echo ""
busybox reboot
echo "          ==========================="
echo "           ) SuperClean Completed! ("
echo "          ==========================="
echo ""
exit 0
EOF
		chown 0.0 /data/V6_SuperCharger/!SuperClean.sh; chmod 777 /data/V6_SuperCharger/!SuperClean.sh
		cp -fr /data/V6_SuperCharger/!SuperClean.sh /sdcard/V6_SuperCharger
		cp -fr /data/V6_SuperCharger/!SuperClean.sh /sdcard/V6_SuperCharger/sclean
		cp -fr /data/V6_SuperCharger/!SuperClean.sh /system/xbin/sclean
		echo " For Speedy SuperCleaning..."
		echo ""
		$sleep
		echo "  .../data/V6_SuperCharger/!SuperClean.sh..."
		echo ""
		$sleep
		echo "                                ...was created!"
		echo ""
		$sleep
		echo " With Script Manager..."
		echo ""
		$sleep
		echo "       ...make a \"Quick Widget\" for it..."
		echo ""
		$sleep
		echo "             ...or put it on a weekly schedule!"
		echo ""
		echo $line
		echo ""
		$sleep
		if [ -f "/system/xbin/sclean" ]; then
			echo " There is another copy in /system/xbin..."
			echo ""
			$sleep
			echo " To use this script with Terminal Emulator..."
			echo ""
			$sleep
			echo " Run Terminal Emulator..."
			echo ""
			$sleep
			echo "           ...type \"su\" and Enter..."
			echo "                    =="
			echo ""
			$sleep
			echo "                  ...type \"sclean\" and Enter..."
			echo "                           ======"
			echo ""
			$sleep
			echo "                  THAT'S IT!"
		else
			echo " WARNING: ERROR copying file to /system/xbin!"
			echo ""
			$sleep
			echo " Got enough free space?"
			echo ""
			$sleep
			echo " System Partition has `df -h system | awk '{print $4,"Free ("$5}' | sed -n 2p` Used)"
		fi
		echo ""
		echo $line
		echo ""
		$sleep
		echo " Do you want to SuperClean & ReStart now?"
		echo ""
		$sleep
		echo -n " Enter Y for Yes, any key for No: "
		read superclean
		echo ""
		case $superclean in
			y|Y)if [ "$madesqlitefolder" -eq 1 ]; then rm -r /sqlite_stmt_journals; fi
				for dv in /d*/dalvik-cache /c*/dalvik-cache; do rm -r /$dv/* 2>/dev/null; done
				echo ""
				echo " All cleaned up and ready to..."
				echo ""
				$sleep
				echo $line
				echo "                    !!POOF!!"
				echo $line
				echo ""
				sleep 2
				busybox sync
				echo 1 > /proc/sys/kernel/sysrq 2>/dev/null
				echo b > /proc/sysrq-trigger 2>/dev/null
				echo "  If it don't go poofie, just reboot manually!"
				echo ""
				echo $line
				busybox reboot;;
			  *)echo $line
				echo " Okay... maybe next time!";;
		esac
	fi
	if [ "$opt" -ne 15 ] && [ "$opt" -ne 30 ]; then
		echo $line
		echo ""
		$sleep
		echo " To Return to Driver's Console..."
		echo ""
		$sleep
		echo -n "              ...Press the Return or Any Key: "
		read enter
	fi
	if [ "$opt" -ge 2 ] && [ "$opt" -le 14 ] || [ "$opt" -eq 21 ] || [ "$opt" -eq 22 ] || [ "$opt" -eq 30 ]; then
		echo ""
		echo $line
		echo ""
		$sleep
		echo " SuperCharging, OOM Grouping & Priority Fixes.."
		echo ""
		$sleep
		echo "    ...BulletProof, Die-Hard & HTK Launchers..."
		echo ""
		$sleep
		echo "     ...by -=zeppelinrox=- @ XDA & Droid Forums"
		sleep 2
	fi
	if [ "$opt" -eq 30 ]; then
		echo ""
		echo "                                     Buh Bye :)"
		echo ""
		echo $line
		echo ""
		$sleep
		exit 0
	fi
	if [ "$madesqlitefolder" -eq 1 ]; then rm -r /sqlite_stmt_journals; fi
	busybox mount -o remount,ro / 2>/dev/null
	busybox mount -o remount,ro rootfs 2>/dev/null
	busybox mount -o remount,ro /system 2>/dev/null
	busybox mount -o remount,ro `busybox mount | grep system | awk '{print $1,$3}' | sed -n 1p` 2>/dev/null
 fi
done
